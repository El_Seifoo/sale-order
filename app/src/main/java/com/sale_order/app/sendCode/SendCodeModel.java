package com.sale_order.app.sendCode;

import android.content.Context;
import android.view.View;

import com.sale_order.app.classes.SignupResponse;
import com.sale_order.app.utils.MySingleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SendCodeModel {
    private Context context;

    public SendCodeModel(Context context) {
        this.context = context;
    }

    protected void sendCode(String email, final ModelCallback callback) {
        Call<SignupResponse> call = MySingleton.getInstance(context).createService().sendForgetPasswordCode(email);
        call.enqueue(new Callback<SignupResponse>() {
            @Override
            public void onResponse(Call<SignupResponse> call, Response<SignupResponse> response) {
                callback.setProgress(View.GONE);
                callback.setButtonClickable(true);
                if (response.body() != null) {
                    callback.showMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<SignupResponse> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.setButtonClickable(true);
                callback.onFailureHandler(t);
            }
        });


    }

    protected interface ModelCallback {
        void setProgress(int progress);

        void setButtonClickable(boolean clickable);

        void onFailureHandler(Throwable t);

        void showMessage(String message);


    }
}
