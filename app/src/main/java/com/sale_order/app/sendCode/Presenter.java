package com.sale_order.app.sendCode;

public interface Presenter {
    void onSendButtonClicked();
}
