package com.sale_order.app.sendCode;

import android.app.Application;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;

import com.sale_order.app.R;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SendCodeViewModel extends AndroidViewModel implements SendCodeModel.ModelCallback {
    private SendCodeModel model;
    private ViewListener viewListener;
    private ObservableField<Integer> progress;
    private ObservableField<Boolean> buttonClickable;

    public SendCodeViewModel(@NonNull Application application) {
        super(application);
        model = new SendCodeModel(application);
        progress = new ObservableField<>(View.GONE);
        buttonClickable = new ObservableField<>(true);
    }

    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    protected void requestSendCode(String email) {
        if (email.isEmpty()) {
            viewListener.showEditTextError(getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }
        if (!checkMailValidation(email)) {
            viewListener.showToastMessage(getApplication().getString(R.string.mail_not_valid));
            return;
        }
        setProgress(View.VISIBLE);
        setButtonClickable(false);
        model.sendCode(email, this);
    }

    private boolean checkMailValidation(String email) {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";
        CharSequence inputStr = email;
        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if (matcher.matches())
            return true;
        else
            return false;
    }

    @Override
    public void onFailureHandler(Throwable t) {
        viewListener.showToastMessage(t instanceof IOException ? getApplication().getString(R.string.no_internet_connection) :
                getApplication().getString(R.string.something_went_wrong));
    }


    public ObservableField<Boolean> getButtonClickable() {
        return buttonClickable;
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    @Override
    public void showMessage(String message) {
        if (message.equals(getApplication().getString(R.string.send_forget_password_code_response_success))) {
            showMessage(getApplication().getString(R.string.code_send_check_your_inbox));
            viewListener.navigateChangePassword();
            return;
        }
        viewListener.showToastMessage(message.equals(
                getApplication().getString(R.string.send_forget_password_code_response_user_not_exist)) ?
                getApplication().getString(R.string.user_not_exist) : message);
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }

    @Override
    public void setButtonClickable(boolean clickable) {
        this.buttonClickable.set(clickable);
    }

    protected interface ViewListener {

        void showToastMessage(String message);

        void navigateChangePassword();

        void showEditTextError(String errorMessage);
    }
}
