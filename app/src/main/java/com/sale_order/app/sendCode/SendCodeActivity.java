package com.sale_order.app.sendCode;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.sale_order.app.R;
import com.sale_order.app.changePassword.ChangePasswordActivity;
import com.sale_order.app.databinding.ActivitySendCodeBinding;
import com.sale_order.app.utils.Constants;
import com.sale_order.app.utils.Language;
import com.sale_order.app.utils.MySingleton;

public class SendCodeActivity extends AppCompatActivity implements SendCodeViewModel.ViewListener, Presenter {
    private SendCodeViewModel viewModel;
    private ActivitySendCodeBinding dataBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.arabic_key)));
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_send_code);
        viewModel = ViewModelProviders.of(this).get(SendCodeViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void navigateChangePassword() {
        Intent intent = new Intent(this, ChangePasswordActivity.class);
        intent.putExtra("email", dataBinding.emailEditText.getText().toString().trim());
        startActivity(intent);
    }

    @Override
    public void showEditTextError(String errorMessage) {
        dataBinding.emailEditText.setError(errorMessage);
    }

    @Override
    public void onSendButtonClicked() {
        viewModel.requestSendCode(dataBinding.emailEditText.getText().toString());
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
