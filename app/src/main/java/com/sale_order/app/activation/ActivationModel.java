package com.sale_order.app.activation;

import android.content.Context;
import android.util.Log;
import android.view.View;

import androidx.lifecycle.MutableLiveData;

import com.sale_order.app.classes.ActivationObj;
import com.sale_order.app.classes.SignupResponse;
import com.sale_order.app.utils.MySingleton;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivationModel {
    private Context context;
//    private MutableLiveData<String> activationStringMutableLiveData = new MutableLiveData<>();
//    private MutableLiveData<String> resendStringMutableLiveData = new MutableLiveData<>();

    public ActivationModel(Context context) {
        this.context = context;
    }

//    protected MutableLiveData<String> getActivationStringMutableLiveData() {
//        return activationStringMutableLiveData;
//    }
//
//    public MutableLiveData<String> getResendStringMutableLiveData() {
//        return resendStringMutableLiveData;
//    }

    protected void activateUser(String email, String code, final boolean fromLogin, final ModelCallback callback) {
        Call<SignupResponse> call = MySingleton.getInstance(context).createService().activateUser(email, code);
        call.enqueue(new Callback<SignupResponse>() {
            @Override
            public void onResponse(Call<SignupResponse> call, Response<SignupResponse> response) {
                callback.setProgress(View.GONE);
                if (response.body() != null) {
//                    activationStringMutableLiveData.setValue(response.body().getMessage());
                    callback.showMessage(response.body().getMessage(), fromLogin, 0);
                }
            }

            @Override
            public void onFailure(Call<SignupResponse> call, Throwable t) {

                callback.setProgress(View.GONE);
                callback.onFailureHandler(t);
            }
        });
    }

    //"el.seif0o0o1@gmail.com"
    protected void resendCode(String email, final ModelCallback callback) {
        Call<SignupResponse> call = MySingleton.getInstance(context).createService().resendCode(email);
        call.enqueue(new Callback<SignupResponse>() {
            @Override
            public void onResponse(Call<SignupResponse> call, Response<SignupResponse> response) {
                callback.setProgress(View.GONE);
                if (response.body() != null) {
//                    resendStringMutableLiveData.setValue(response.body().getMessage());
                    callback.showMessage(response.body().getMessage(), false, 1);
                }
            }

            @Override
            public void onFailure(Call<SignupResponse> call, Throwable t) {
                Log.e("failed", t.toString());
                callback.setProgress(View.GONE);
                callback.onFailureHandler(t);
            }
        });
    }

    protected interface ModelCallback {
        void setProgress(int progress);

        void showMessage(String message, boolean isFromLogin, int index);

        void onFailureHandler(Throwable t);
    }
}
