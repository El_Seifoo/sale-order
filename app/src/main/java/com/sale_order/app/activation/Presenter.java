package com.sale_order.app.activation;

public interface Presenter {
    void onNextButtonClicked();

    void onResendCodeClicked();
}
