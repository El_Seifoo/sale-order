package com.sale_order.app.activation;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.sale_order.app.R;
import com.sale_order.app.classes.UserObj;
import com.sale_order.app.databinding.ActivityActivationBinding;
import com.sale_order.app.login.LoginActivity;
import com.sale_order.app.utils.Constants;
import com.sale_order.app.utils.Language;
import com.sale_order.app.utils.MySingleton;

public class ActivationActivity extends AppCompatActivity implements Presenter, ActivationViewModel.ViewListener {
    private ActivationViewModel viewModel;
    private ActivityActivationBinding dataBinding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.arabic_key)));
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_activation);
        viewModel = ViewModelProviders.of(this).get(ActivationViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // if coming from LoginActivity sendCode to email first
        if (getIntent().hasExtra("flag"))
            onResendCodeClicked();
    }

    @Override
    public void onNextButtonClicked() {
        viewModel.requestActivation(getIntent().getExtras().getString("email"), dataBinding.activationCodeEditText.getText().toString().trim(), getIntent().hasExtra("flag"));
    }

    @Override
    public void onResendCodeClicked() {
        viewModel.requestResendCode(getIntent().getExtras().getString("email"));
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showCodeEditTextError() {
        dataBinding.activationCodeEditText.setError(getString(R.string.this_field_can_not_be_blank));
    }

    @Override
    public void activationDone() {
        Intent loginIntent = new Intent(this, LoginActivity.class);
        loginIntent.putExtra("email", getIntent().getExtras().getString("email"));
        loginIntent.putExtra("password", getIntent().getExtras().getString("password"));
        startActivity(loginIntent);
    }

    @Override
    public void loginActivationDone() {
        Intent loginIntent = getIntent();
        loginIntent.putExtra("email", getIntent().getExtras().getString("email"));
        loginIntent.putExtra("password", getIntent().getExtras().getString("password"));
        setResult(RESULT_OK, loginIntent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
