package com.sale_order.app.activation;

import android.app.Application;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.sale_order.app.R;

import java.io.IOException;

public class ActivationViewModel extends AndroidViewModel implements ActivationModel.ModelCallback {
    private Application application;
    private ActivationModel model;
    private ViewListener viewListener;
    private ObservableField<Integer> progress;

    public ActivationViewModel(@NonNull Application application) {
        super(application);
        this.application = application;
        model = new ActivationModel(application);
        progress = new ObservableField<>(View.GONE);
    }

    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

//    protected MutableLiveData<String> getActivationMutableLiveData() {
//        return model.getActivationStringMutableLiveData();
//    }

    protected void requestActivation(String email, String code, boolean fromLogin) {
        if (code.isEmpty()) {
            viewListener.showCodeEditTextError();
            return;
        }

        setProgress(View.VISIBLE);
        model.activateUser(email, code, fromLogin, this);
    }

//    protected MutableLiveData<String> getResendCodeMutableLiveData() {
//        return model.getResendStringMutableLiveData();
//    }

    protected void requestResendCode(String email) {
        setProgress(View.VISIBLE);
        model.resendCode(email, this);
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }

    @Override
    public void showMessage(String message, boolean isFromLogin, int index) {
        if (message != null) {
            if (index == 0 && message.equals(application.getString(R.string.activate_user_response_success))) {
                if (isFromLogin)
                    viewListener.loginActivationDone();
                else
                    viewListener.activationDone();
                return;
            }

            viewListener.showToastMessage(message.equals(application.getString(R.string.activate_user_response_user_not_exist))
                    || message.equals(application.getString(R.string.resend_code_response_user_not_exist)) ?
                    application.getString(R.string.user_not_exist) : message.equals(application.getString(R.string.activate_user_response_invalid_code)) ?
                    application.getString(R.string.invalid_code) : message.equals(application.getString(R.string.resend_code_response_success)) ?
                    application.getString(R.string.code_send_check_your_inbox) : application.getString(R.string.something_went_wrong));
        }
    }

    @Override
    public void onFailureHandler(Throwable t) {
        viewListener.showToastMessage(t instanceof IOException ? application.getString(R.string.no_internet_connection) :
                application.getString(R.string.something_went_wrong));
    }

//    public void checkResponseMessage(String message, boolean fromLogin) {
//        if (message.equals(application.getString(R.string.activate_user_response_success))) {
//            if (fromLogin)
//                viewListener.loginActivationDone();
//            else
//                viewListener.activationDone();
//            return;
//        }
//
//        viewListener.showToastMessage(message.equals(application.getString(R.string.activate_user_response_user_not_exist))
//                || message.equals(application.getString(R.string.resend_code_response_user_not_exist)) ?
//                application.getString(R.string.user_not_exist) : message.equals(application.getString(R.string.activate_user_response_invalid_code)) ?
//                application.getString(R.string.invalid_code) : message.equals(application.getString(R.string.resend_code_response_success)) ?
//                application.getString(R.string.code_send_check_your_inbox) : application.getString(R.string.something_went_wrong));
//    }

    protected interface ViewListener {
        void showToastMessage(String message);

        void showCodeEditTextError();

        void activationDone();

        void loginActivationDone();
    }
}
