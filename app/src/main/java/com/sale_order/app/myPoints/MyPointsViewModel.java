package com.sale_order.app.myPoints;

import android.app.Application;
import android.content.Intent;
import android.net.Uri;
import android.util.Base64;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.sale_order.app.R;
import com.sale_order.app.classes.UserInfoData;
import com.sale_order.app.classes.UserInfoObj;
import com.sale_order.app.utils.MySingleton;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import static android.app.Activity.RESULT_OK;

public class MyPointsViewModel extends AndroidViewModel implements MyPointsModel.ModelCallback {
    protected static final int LOGIN_FIRST_PROFILE_REQUEST_CODE = 1;
    private MyPointsModel model;
    private ViewListener viewListener;
    private ObservableField<Integer> progress;
    private ObservableField<String> errorMessage;
    private ObservableField<Integer> errorView;

    public MyPointsViewModel(@NonNull Application application) {
        super(application);
        model = new MyPointsModel(application);
        progress = new ObservableField<>(View.GONE);
        errorView = new ObservableField<>(View.GONE);
        errorMessage = new ObservableField<>("");
    }

    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    protected MutableLiveData<UserInfoData> requestUserInfo() {
        setProgress(View.VISIBLE);
        return model.fetchUserInfo(this);
    }

    public void onRetryClickListener(View view) {
        setProgress(View.VISIBLE);
        setErrorView(View.GONE);
        model.fetchUserInfo(this);
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    public ObservableField<String> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage.set(errorMessage);
    }

    public ObservableField<Integer> getErrorView() {
        return errorView;
    }

    public void setErrorView(int errorView) {
        this.errorView.set(errorView);
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }

    @Override
    public void loginFirst(int requestCode) {
        MySingleton.getInstance(getApplication()).logout();
        viewListener.LoginFirst(requestCode);
    }

    @Override
    public void handleOnFailure(Throwable t) {
        // error fetching user info ...
        setErrorView(View.VISIBLE);
        Log.e("error", t.toString());
        if (t instanceof IOException)
            setErrorMessage(getApplication().getString(R.string.no_internet_connection));
        else
            setErrorMessage(getApplication().getString(R.string.error_fetching_data));

    }

    @Override
    public void showMessage(String message) {
        if (message != null) {
            Log.e("message", message);
            if (message.equals(getApplication().getString(R.string.update_user_info_response_success))) {
                viewListener.showToastMessage(getApplication().getString(R.string.user_updated_successfully));
                return;
            }
            viewListener.showToastMessage(message.equals(getApplication().getString(R.string.update_user_info_response_email_already_exist))
                    ? getApplication().getString(R.string.email_already_exist)
                    : message.equals(getApplication().getString(R.string.update_user_info_response_phone_number_already_exist))
                    ? getApplication().getString(R.string.phone_already_exist)
                    : message.equals(getApplication().getString(R.string.update_user_info_response_username_already_exist))
                    ? getApplication().getString(R.string.username_already_exist)
                    : getApplication().getString(R.string.something_went_wrong));
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) throws FileNotFoundException {
        if (requestCode == LOGIN_FIRST_PROFILE_REQUEST_CODE && resultCode == RESULT_OK)
            this.requestUserInfo();
    }

    protected interface ViewListener {
        void showToastMessage(String message);

        void LoginFirst(int requestCode);
    }
}
