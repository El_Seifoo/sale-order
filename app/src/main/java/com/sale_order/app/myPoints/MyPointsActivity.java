package com.sale_order.app.myPoints;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.sale_order.app.R;
import com.sale_order.app.classes.UserInfoData;
import com.sale_order.app.databinding.ActivityMyPointsBinding;
import com.sale_order.app.login.LoginActivity;
import com.sale_order.app.utils.Constants;
import com.sale_order.app.utils.Language;
import com.sale_order.app.utils.MySingleton;

public class MyPointsActivity extends AppCompatActivity implements MyPointsViewModel.ViewListener {
    private MyPointsViewModel viewModel;
    private ActivityMyPointsBinding dataBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.arabic_key)));
        setContentView(R.layout.activity_my_points);
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_my_points);
        viewModel = ViewModelProviders.of(this).get(MyPointsViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);


        getSupportActionBar().setTitle(getString(R.string.my_points));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        viewModel.requestUserInfo().observe(this, new Observer<UserInfoData>() {
            @Override
            public void onChanged(UserInfoData userInfo) {
                if (userInfo != null)
                    dataBinding.setUser(userInfo.getUserInfo());
            }
        });
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void LoginFirst(int requestCode) {
        Intent loginIntent = new Intent(this, LoginActivity.class);
        loginIntent.putExtra("MyPointsActivity", "");
        startActivityForResult(loginIntent, requestCode);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
