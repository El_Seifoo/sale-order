package com.sale_order.app.myPoints;

import android.content.Context;
import android.util.Log;
import android.view.View;

import androidx.lifecycle.MutableLiveData;

import com.sale_order.app.classes.UserInfoData;
import com.sale_order.app.classes.UserInfoObj;
import com.sale_order.app.classes.UserInfoResponse;
import com.sale_order.app.utils.Constants;
import com.sale_order.app.utils.MySingleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyPointsModel {
    private Context context;

    public MyPointsModel(Context context) {
        this.context = context;
    }

    private MutableLiveData<UserInfoData> userMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<UserInfoData> fetchUserInfo(final ModelCallback callback) {
        if (userMutableLiveData.getValue() == null) {
            Call<UserInfoResponse> call = MySingleton.getInstance(context).createService()
                    .profile(MySingleton.getInstance(context).getStringFromSharedPref(Constants.USER_TOKEN, ""));
            call.enqueue(new Callback<UserInfoResponse>() {
                @Override
                public void onResponse(Call<UserInfoResponse> call, Response<UserInfoResponse> response) {
                    Log.e("response code", response.code() + "))))");//401 not authorized
                    callback.setProgress(View.GONE);
                    if (response.code() == 401) {
                        callback.loginFirst(MyPointsViewModel.LOGIN_FIRST_PROFILE_REQUEST_CODE);
                        return;
                    }
                    if (response.code() == 200) {
                        if (response.body() != null) {
                            UserInfoResponse infoResponse = response.body();
                            callback.showMessage(infoResponse.getMessage());
                            MySingleton.getInstance(context).saveUserData(infoResponse.getUser().getUserInfo());
                            userMutableLiveData.setValue(infoResponse.getUser());
                        }
                        return;
                    }

                    callback.showMessage("");

                }

                @Override
                public void onFailure(Call<UserInfoResponse> call, Throwable t) {
                    callback.setProgress(View.GONE);

                    callback.handleOnFailure(t);
                }
            });
        } else {
            callback.setProgress(View.GONE);
        }
        return userMutableLiveData;
    }

    protected interface ModelCallback {

        void handleOnFailure(Throwable t);

        void showMessage(String message);

        void setProgress(int progress);

        void loginFirst(int requestCode);
    }
}
