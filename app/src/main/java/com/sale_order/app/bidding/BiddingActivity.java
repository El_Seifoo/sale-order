package com.sale_order.app.bidding;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.sale_order.app.R;
import com.sale_order.app.classes.AuctionObj;
import com.sale_order.app.classes.TimerObj;
import com.sale_order.app.databinding.ActivityBiddingBinding;
import com.sale_order.app.login.LoginActivity;
import com.sale_order.app.main.home.SliderAdapter;
import com.sale_order.app.utils.Constants;
import com.sale_order.app.utils.Language;
import com.sale_order.app.utils.MySingleton;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class BiddingActivity extends AppCompatActivity implements Presenter, BiddingViewModel.ViewListener {
    protected static final int LOGIN_FIRST_REQUEST_CODE = 1;
    private BiddingViewModel viewModel;
    private ActivityBiddingBinding dataBinding;
    private BiddingAdapter adapter;
    private CountDownTimer countDownTimer;

    private ViewPager viewPager;
    private SliderAdapter sliderAdapter;

    Timer t;
    private static final int DELAY_TIME = 6000;
    private int currentPage = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.arabic_key)));
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_bidding);
        viewModel = ViewModelProviders.of(this).get(BiddingViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);

        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        dataBinding.biddingList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        adapter = new BiddingAdapter();
        dataBinding.biddingList.setAdapter(adapter);

        viewPager = dataBinding.viewPager;
        sliderAdapter = new SliderAdapter();

        viewModel.requestCertainAuction(getIntent().getExtras().getString("AuctionID")).observe(this, new Observer<AuctionObj>() {
            @Override
            public void onChanged(AuctionObj auctionObj) {
                TimerObj timer = auctionObj.getTimer();
                dataBinding.setAuction(auctionObj);
                startCounter(viewModel.timerConverter(timer.getDays(), timer.getHours(), timer.getMinutes(), timer.getSeconds()));
                adapter.setBidders(auctionObj.getBiddersInAuctions());
                if (!viewModel.getValueEventListener().get()) {
                    viewModel.requestCheckFirebaseNumberOfBids(auctionObj.getId());
                    viewModel.setValueEventListener(true);
                }

                // images slider
                sliderAdapter.setImages(auctionObj.getImages(), new ArrayList<String>());
                viewPager.setAdapter(sliderAdapter);
                if (sliderAdapter.getCount() > 1)
                    startAutoSliding();

            }
        });

    }

    private void startAutoSliding() {
        if (t != null)
            t.cancel();
        t = new Timer();
        t.scheduleAtFixedRate(new TimerTask() {

                                  @Override
                                  public void run() {
                                      currentPage = viewPager.getCurrentItem();
                                      if (currentPage != viewPager.getAdapter().getCount() - 1) {
                                          viewPager.setCurrentItem(++currentPage);
                                      } else {
                                          currentPage = 0;
                                          viewPager.setCurrentItem(currentPage);
                                      }
                                  }
                              },
                //Set how long before to start calling the TimerTask (in milliseconds)
                DELAY_TIME,
                //Set the amount of time between each execution (in milliseconds)
                DELAY_TIME);
    }


    private void startCounter(final long time) {
        cancelCounter();
        final AuctionObj auctionObj = dataBinding.getAuction();
        countDownTimer = new CountDownTimer(time, 1000) {

            public void onTick(long millisUntilFinished) {
                int second = (int) (millisUntilFinished / 1000) % 60;
                int minute = (int) (millisUntilFinished / (1000 * 60)) % 60;
                int hour = (int) (millisUntilFinished / (1000 * 60 * 60)) % 24;
                int days = (int) (millisUntilFinished / (1000 * 60 * 60 * 24));
                if (second == 15 && minute == 0 && hour == 0 && days == 0) {
                    viewModel.requestStartAuction(getIntent().getExtras().getString("AuctionID"));
                }
                auctionObj.setTimer(new TimerObj(days, hour, minute, second, auctionObj.getTimer().getStartIn(), auctionObj.getTimer().getEndIn()));
                dataBinding.setAuction(auctionObj);
            }

            public void onFinish() {
                viewModel.requestCloseAuction(getIntent().getExtras().getString("AuctionID"));
            }

        };
        countDownTimer.start();
    }

    private void cancelCounter() {
        if (countDownTimer != null)
            countDownTimer.cancel();
    }

    @Override
    public void onBidButtonClicked() {
        AuctionObj auction = dataBinding.getAuction();
        viewModel.requestBid(auction.getId(), auction.getCurrentUser(), auction.getMinPointsForBid());
    }


    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void loginFirst() {
        Intent loginIntent = new Intent(this, LoginActivity.class);
        loginIntent.putExtra("BiddingActivity", "");
        startActivityForResult(loginIntent, LOGIN_FIRST_REQUEST_CODE);
    }

    @Override
    public void finishActivity() {
        showToastMessage(getString(R.string.auction_closed));
        setResult(Activity.RESULT_OK);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        viewModel.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cancelCounter();
        viewModel.setValueEventListener(false);
        viewModel.removeEventListener();
        if (t != null)
            t.cancel();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
