package com.sale_order.app.bidding;

import android.content.Context;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sale_order.app.classes.AuctionObj;
import com.sale_order.app.classes.AuctionResponse;
import com.sale_order.app.classes.SignupResponse;
import com.sale_order.app.classes.UserInfoObj;
import com.sale_order.app.utils.ApiServicesClient;
import com.sale_order.app.utils.Constants;
import com.sale_order.app.utils.MySingleton;

import java.util.EventListener;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BiddingModel {
    private Context context;

    public BiddingModel(Context context) {
        this.context = context;
    }

    private MutableLiveData<AuctionObj> auctionObjMutableLiveData = new MutableLiveData<>();

    protected void addCurrentUser(UserInfoObj user) {
        auctionObjMutableLiveData.getValue().setCurrentUser(user);
    }

    protected MutableLiveData<AuctionObj> fetchCertainAuction(final String auctionId, final ModelCallback callback) {

        Call<AuctionResponse> call = MySingleton.getInstance(context).createService().certainAuction(
                MySingleton.getInstance(context).getStringFromSharedPref(Constants.USER_TOKEN, ""), auctionId);
        call.enqueue(new Callback<AuctionResponse>() {
            @Override
            public void onResponse(Call<AuctionResponse> call, Response<AuctionResponse> response) {
                callback.setProgress(View.GONE);
                callback.setButtonClickable(true);

                if (response.code() == 200) {
                    AuctionObj auctionObj = response.body().getAuctionObj();
                    if (auctionObj.getBiddersInAuctions() == null)
                        callback.setEmptyListTextView(View.VISIBLE);
                    else if (auctionObj.getBiddersInAuctions().isEmpty())
                        callback.setEmptyListTextView(View.VISIBLE);
                    else callback.setEmptyListTextView(View.GONE);
                    callback.setHighestBidderPrice(0.0);
                    if (!auctionObj.getBiddersInAuctions().isEmpty()) {
                        int size = auctionObj.getBiddersInAuctions().size();
                        callback.setHighestBidderPrice(auctionObj.getBiddersInAuctions().get(0).getPrice());
                    }
                    MySingleton.getInstance(context).saveUserData(auctionObj.getCurrentUser());
                    auctionObjMutableLiveData.setValue(auctionObj);
                }

            }

            @Override
            public void onFailure(Call<AuctionResponse> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.setButtonClickable(true);
                callback.onFailureHandler(t);
            }
        });

        return auctionObjMutableLiveData;
    }

    private DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_CURRENT_AUCTION_TABLE_NAME);
    private ValueEventListener valueEventListener;

    public void fetchNumberOfBids(final String auctionId, final ModelCallback callback) {
        reference.child(auctionId);
        valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists())
                    fetchCertainAuction(auctionId, callback);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("databseError", databaseError.toString());
            }
        };
        reference.addValueEventListener(valueEventListener);
    }

    public void removeEventListener() {
        if (valueEventListener != null)
            reference.removeEventListener(valueEventListener);
    }

    protected void bid(String auctionId, final ModelCallback callback) {
        Map<String, String> map = new HashMap<>();
        map.put("AuctionId", auctionId);
        Call<SignupResponse> call = MySingleton.getInstance(context).createService().bid(
                MySingleton.getInstance(context).getStringFromSharedPref(Constants.USER_TOKEN, ""), map);
        call.enqueue(new Callback<SignupResponse>() {
            @Override
            public void onResponse(Call<SignupResponse> call, Response<SignupResponse> response) {
                callback.setProgress(View.GONE);
                callback.setButtonClickable(true);
                if (response.code() == 401) {
                    callback.loginFirst();
                    return;
                }
                if (response.code() == 200) {
                    callback.handleBiddingResponse(response.body().getMessage());
                    return;
                }

            }

            @Override
            public void onFailure(Call<SignupResponse> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.setButtonClickable(true);

            }
        });
    }

    public void startAuction(String auctionID) {
        Call<Void> call = MySingleton.getInstance(context).createService().startAuction(
                MySingleton.getInstance(context).getStringFromSharedPref(Constants.USER_TOKEN, ""), auctionID);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }

    public void closeAuction(String auctionID, final ModelCallback callback) {
        Call<Void> call = MySingleton.getInstance(context).createService().closeAuction(
                MySingleton.getInstance(context).getStringFromSharedPref(Constants.USER_TOKEN, ""), auctionID);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                callback.auctionClosed();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                callback.auctionClosed();
            }
        });
    }

    protected interface ModelCallback {
        void setProgress(int progress);

        void setEmptyListTextView(int empty);

        void setButtonClickable(boolean clickable);

        void onFailureHandler(Throwable t);

        void handleBiddingResponse(String message);

        void loginFirst();

        void setHighestBidderPrice(double price);

        void auctionClosed();
    }
}
