package com.sale_order.app.bidding;

public interface Presenter {
    void onBidButtonClicked();
}
