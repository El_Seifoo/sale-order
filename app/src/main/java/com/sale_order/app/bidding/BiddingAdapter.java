package com.sale_order.app.bidding;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.sale_order.app.R;
import com.sale_order.app.classes.BidderObj;
import com.sale_order.app.databinding.BiddingUsersListItemBinding;

import java.util.List;

public class BiddingAdapter extends RecyclerView.Adapter<BiddingAdapter.Holder> {
    private List<BidderObj> bidders;

    public void setBidders(List<BidderObj> bidders) {
        this.bidders = bidders;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder((BiddingUsersListItemBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.bidding_users_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        holder.binding.setBidder(bidders.get(position));
    }

    @Override
    public int getItemCount() {
        return bidders != null ? bidders.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder {
        private BiddingUsersListItemBinding binding;

        public Holder(@NonNull BiddingUsersListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
