package com.sale_order.app.bidding;

import android.app.Application;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.sale_order.app.R;
import com.sale_order.app.classes.AuctionObj;
import com.sale_order.app.classes.UserInfoObj;
import com.sale_order.app.utils.MySingleton;

import java.io.IOException;

import static android.app.Activity.RESULT_OK;

public class BiddingViewModel extends AndroidViewModel implements BiddingModel.ModelCallback {
    private Application application;
    private BiddingModel model;
    private ViewListener viewListener;
    private ObservableField<Integer> progress;
    private ObservableField<Integer> emptyListTextView;
    private ObservableField<String> errorMessage;
    private ObservableField<Integer> errorView;
    private ObservableField<Boolean> valueEventListener;
    private ObservableField<Boolean> buttonClickable;
    private ObservableField<Double> highestBidderPrice;

    public BiddingViewModel(@NonNull Application application) {
        super(application);
        this.application = application;
        model = new BiddingModel(application);
        progress = new ObservableField<>(View.GONE);
        emptyListTextView = new ObservableField<>(View.GONE);
        errorView = new ObservableField<>(View.GONE);
        errorMessage = new ObservableField<>("");
        valueEventListener = new ObservableField<>(false);
        buttonClickable = new ObservableField<>(true);
        highestBidderPrice = new ObservableField<>(0.0);
    }

    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    private String auctionId;

    protected MutableLiveData<AuctionObj> requestCertainAuction(String auctionId) {
        this.auctionId = auctionId;
        setProgress(View.VISIBLE);
        setButtonClickable(false);
        return model.fetchCertainAuction(auctionId, this);
    }

    public void onRetryClickListener(View view) {
        setProgress(View.VISIBLE);
        setErrorView(View.GONE);
        setButtonClickable(false);
        model.fetchCertainAuction(auctionId, this);
    }

    public ObservableField<Boolean> getValueEventListener() {
        return valueEventListener;
    }

    public void setValueEventListener(boolean flag) {
        this.valueEventListener.set(flag);
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    public ObservableField<Integer> getEmptyListTextView() {
        return emptyListTextView;
    }

    public ObservableField<String> getErrorMessage() {
        return errorMessage;
    }

    protected void setErrorMessage(String message) {
        errorMessage.set(message);
    }

    public ObservableField<Integer> getErrorView() {
        return errorView;
    }

    protected void setErrorView(int errorViewStatus) {
        this.errorView.set(errorViewStatus);
    }

    public ObservableField<Boolean> getButtonClickable() {
        return buttonClickable;
    }

    public ObservableField<Double> getHighestBidderPrice() {
        return highestBidderPrice;
    }

    @Override
    public void setHighestBidderPrice(double price) {
        this.highestBidderPrice.set(price);
    }

    @Override
    public void auctionClosed() {
        viewListener.finishActivity();
    }

    @Override
    public void setButtonClickable(boolean clickable) {
        this.buttonClickable.set(clickable);
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }

    @Override
    public void setEmptyListTextView(int empty) {
        this.emptyListTextView.set(empty);
    }

    @Override
    public void onFailureHandler(Throwable t) {
        setErrorView(View.VISIBLE);
        Log.e("error", t.toString());
        if (t instanceof IOException)
            setErrorMessage(application.getString(R.string.no_internet_connection));
        else
            setErrorMessage(application.getString(R.string.error_fetching_data));
    }

    @Override
    public void handleBiddingResponse(String message) {
//        if (!message.equals(application.getString(R.string.bid_response_success)))
        viewListener.showToastMessage(message);
    }

    @Override
    public void loginFirst() {
        MySingleton.getInstance(getApplication()).logout();
        viewListener.loginFirst();
    }

    protected void requestCheckFirebaseNumberOfBids(String auctionId) {
        model.fetchNumberOfBids(auctionId, this);
    }

    protected void requestBid(String auctionId, UserInfoObj currentUser, int minPointsForBid) {
        if (currentUser == null) {
            this.loginFirst();
            return;
        }

        setProgress(View.VISIBLE);
        setButtonClickable(false);
        model.bid(auctionId, this);
    }

    protected void removeEventListener() {
        model.removeEventListener();
    }

    protected long timerConverter(int days, int hours, int minutes, int seconds) {
        long seconds1 = days * (60 * 60 * 24);
        long seconds2 = (hours) * (60 * 60);
        long seconds3 = (minutes) * (60);
        return ((seconds1 + seconds2 + seconds3 + seconds) * 1000);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == BiddingActivity.LOGIN_FIRST_REQUEST_CODE && resultCode == RESULT_OK) {
            model.addCurrentUser(MySingleton.getInstance(getApplication()).userData());
        }
    }

    public void requestStartAuction(String auctionID) {
        model.startAuction(auctionID);
    }

    public void requestCloseAuction(String auctionID) {
        model.closeAuction(auctionID, this);
    }

    protected interface ViewListener {
        void showToastMessage(String message);

        void loginFirst();

        void finishActivity();
    }
}
