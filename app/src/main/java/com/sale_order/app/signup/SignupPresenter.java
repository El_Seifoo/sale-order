package com.sale_order.app.signup;

public interface SignupPresenter {
    void onSignUpClicked();

    void onCountryClicked(int index);
}
