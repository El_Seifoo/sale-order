package com.sale_order.app.signup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.sale_order.app.R;
import com.sale_order.app.activation.ActivationActivity;
import com.sale_order.app.classes.CountryObj;
import com.sale_order.app.classes.UserObj;
import com.sale_order.app.databinding.ActivitySignupBinding;
import com.sale_order.app.login.LoginActivity;
import com.sale_order.app.main.MainActivity;
import com.sale_order.app.signup.countries.CountriesActivity;
import com.sale_order.app.termsConditions.TermsConditionsActivity;
import com.sale_order.app.utils.Constants;
import com.sale_order.app.utils.Language;
import com.sale_order.app.utils.MySingleton;

import java.util.ArrayList;
import java.util.List;

public class SignupActivity extends AppCompatActivity implements SignupViewModel.ViewListener, SignupPresenter {
    protected static final int PICK_COUNTRY_REQUEST = 1;
    private SignupViewModel viewModel;
    private ActivitySignupBinding dataBinding;
    private List<CountryObj> countries;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.arabic_key)));
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_signup);
        viewModel = ViewModelProviders.of(this).get(SignupViewModel.class);
        viewModel.setListener(this);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewModel.requestAllCountries(MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE, getString(R.string.english_key))).observe(this, new Observer<List<CountryObj>>() {
            @Override
            public void onChanged(List<CountryObj> countryObjs) {
                countries = countryObjs;
            }
        });


        dataBinding.termsConditionsCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                viewModel.setTermsConditionsStatus(isChecked);
            }
        });

    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.terms_conditions_text_view:
                Intent intent1 = new Intent(this, TermsConditionsActivity.class);
                startActivity(intent1);
        }
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showEditTextError(EditText editText, String errorMessage) {
        editText.setError(errorMessage);
    }

    @Override
    public void signUpDone() {
        Intent activationIntent = new Intent(this, LoginActivity.class);
        activationIntent.putExtra("email", dataBinding.signupEmailEditText.getText().toString());
        activationIntent.putExtra("username", dataBinding.signupUsernameEditText.getText().toString());
        activationIntent.putExtra("password", dataBinding.signupPasswordEditText.getText().toString());
        startActivity(activationIntent);
    }

    @Override
    public void onSignUpClicked() {
        viewModel.requestSignupMethod(dataBinding.signupFullNameEditText,
                dataBinding.signupUsernameEditText,
                dataBinding.signupEmailEditText,
                dataBinding.signupPhoneNumberCodeTextView.getText().toString(),
                dataBinding.signupPhoneNumberEditText,
                dataBinding.signupPasswordEditText,
                dataBinding.signupConfirmPasswordEditText,
                dataBinding.termsConditionsCheckBox.isChecked());
    }

    @Override
    public void onCountryClicked(int index) {
        Intent intent = new Intent(this, CountriesActivity.class);
        intent.putExtra("countries", new ArrayList<>(countries));
        intent.putExtra("flag", index);
        startActivityForResult(intent, PICK_COUNTRY_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        viewModel.requestOnActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
