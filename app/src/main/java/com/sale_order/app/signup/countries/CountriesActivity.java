package com.sale_order.app.signup.countries;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.sale_order.app.R;
import com.sale_order.app.classes.CountryObj;
import com.sale_order.app.utils.Constants;
import com.sale_order.app.utils.Language;
import com.sale_order.app.utils.MySingleton;

import java.util.ArrayList;

public class CountriesActivity extends AppCompatActivity implements CountriesAdapter.onCountryItemClicked {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.arabic_key)));
        setContentView(R.layout.activity_countries);

        getSupportActionBar().setTitle(getString(R.string.select_country));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        RecyclerView countriesList = findViewById(R.id.countries_list);
        countriesList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        countriesList.setAdapter(new CountriesAdapter((ArrayList<CountryObj>) getIntent().getSerializableExtra("countries"),getIntent().getExtras().getInt("flag"), this));
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(CountryObj country) {
        Intent intent = getIntent();
        intent.putExtra("country", country);
        setResult(RESULT_OK, intent);
        finish();
    }
}
