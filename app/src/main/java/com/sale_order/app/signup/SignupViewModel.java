package com.sale_order.app.signup;

import android.app.Application;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.sale_order.app.R;
import com.sale_order.app.classes.CountryObj;
import com.sale_order.app.classes.UserObj;

import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.app.Activity.RESULT_OK;


public class SignupViewModel extends AndroidViewModel implements SignupModel.ModelCallback {
    private Application application;
    private ViewListener viewListener;
    private SignupModel model;
    private ObservableField<Integer> progress;
    private ObservableField<CountryObj> country;
    private ObservableField<CountryObj> countryName;
    private ObservableField<String> errorMessage;
    private ObservableField<Integer> errorView;
    private ObservableField<Boolean> signButtonClickable;
    private ObservableField<Boolean> termsConditionsStatus;

    public SignupViewModel(@NonNull Application application) {
        super(application);
        this.application = application;
        model = new SignupModel(application);
        progress = new ObservableField<>(View.GONE);
        errorView = new ObservableField<>(View.GONE);
        signButtonClickable = new ObservableField<>(false);
        country = new ObservableField<>();
        countryName = new ObservableField<>();
        errorMessage = new ObservableField<>("");
        termsConditionsStatus = new ObservableField<>(false);
    }

    protected void setListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public void onRetryClickListener(View view) {
        setProgress(View.VISIBLE);
        setErrorView(View.GONE);
        setSignButtonClickable(false);
        model.fetchCountries(view.getContext().getString(R.string.english_key), this);
    }


    protected MutableLiveData<List<CountryObj>> requestAllCountries(String language) {
        setProgress(View.VISIBLE);
        setSignButtonClickable(false);
        return model.fetchCountries(language, this);
    }

    public ObservableField<Boolean> getTermsConditionsStatus() {
        return termsConditionsStatus;
    }

    public void setTermsConditionsStatus(boolean termsConditionsStatus) {
        this.termsConditionsStatus.set(termsConditionsStatus);
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    public ObservableField<CountryObj> getCountry() {
        return country;
    }

    public ObservableField<CountryObj> getCountryName() {
        return countryName;
    }

    public ObservableField<String> getErrorMessage() {
        return errorMessage;
    }

    protected void setErrorMessage(String message) {
        errorMessage.set(message);
    }

    public ObservableField<Integer> getErrorView() {
        return errorView;
    }

    protected void setErrorView(int errorViewStatus) {
        this.errorView.set(errorViewStatus);
    }

    public ObservableField<Boolean> getSignButtonClickable() {
        return signButtonClickable;
    }

    @Override
    public void setSignButtonClickable(boolean clickable) {
        this.signButtonClickable.set(clickable);
    }

    @Override
    public void setMessage(String message) {
        if (message != null) {
            Log.e("message", message);
            if (message.equals(application.getString(R.string.sign_up_response_success))) {
                viewListener.showToastMessage(application.getString(R.string.account_created_successfully));
                viewListener.signUpDone();
                return;
            }
            viewListener.showToastMessage(message.equals(application.getString(R.string.sign_up_response_email_error)) ? application.getString(R.string.email_already_exist)
                    : message.equals(application.getString(R.string.sign_up_response_user_name_error)) ? application.getString(R.string.username_already_exist)
                    : message.trim().equals(application.getString(R.string.sign_up_response_phone_error).trim()) ? application.getString(R.string.phone_already_exist)
                    : application.getString(R.string.something_went_wrong));
        }
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }

    @Override
    public void setCountry(CountryObj county) {
        this.country.set(county);
    }

    @Override
    public void setCountryName(CountryObj county) {
        this.countryName.set(county);
    }


    @Override
    public void onFailureHandler(Throwable t, int index) {
        if (index == 0) {
            // error fetching countries ...
            setErrorView(View.VISIBLE);
            Log.e("error", t.toString());
            if (t instanceof IOException)
                setErrorMessage(application.getString(R.string.no_internet_connection));
            else
                setErrorMessage(application.getString(R.string.error_fetching_data));

        } else if (index == 1) {
            viewListener.showToastMessage(t instanceof IOException ? application.getString(R.string.no_internet_connection) :
                    application.getString(R.string.something_went_wrong));
        }
    }


    protected void requestSignupMethod(EditText fullNameEditText, EditText userNameEditText, EditText emailEditText, String phoneCode, EditText phoneNumberEditText, EditText passwordEditText, EditText confirmPasswordEditText, boolean termsIsChecked) {
        String fullName = userNameEditText.getText().toString().trim();
        if (fullName.isEmpty()) {
            viewListener.showEditTextError(fullNameEditText, application.getString(R.string.this_field_can_not_be_blank));
            return;
        }
        String userName = userNameEditText.getText().toString().trim();
        if (userName.isEmpty()) {
            viewListener.showEditTextError(userNameEditText, application.getString(R.string.this_field_can_not_be_blank));
            return;
        }
        String email = emailEditText.getText().toString().trim();
        if (email.isEmpty()) {
            viewListener.showEditTextError(emailEditText, application.getString(R.string.this_field_can_not_be_blank));
            return;
        }
        if (!checkMailValidation(email)) {
            viewListener.showEditTextError(emailEditText, application.getString(R.string.mail_not_valid));
            return;
        }
        String phone = phoneNumberEditText.getText().toString().trim();
        if (phone.isEmpty()) {
            viewListener.showEditTextError(phoneNumberEditText, application.getString(R.string.this_field_can_not_be_blank));
            return;
        }
        if (phone.length() + phoneCode.length() > 12) {
            viewListener.showEditTextError(phoneNumberEditText, application.getString(R.string.phone_code_length_error));
            return;
        }
        String password = passwordEditText.getText().toString().trim();
        if (password.isEmpty()) {
            viewListener.showEditTextError(passwordEditText, application.getString(R.string.this_field_can_not_be_blank));
            return;
        }

        if (password.length() < 6) {
            viewListener.showEditTextError(passwordEditText, application.getString(R.string.password_length_error));
            return;
        }
        String confirmPassword = confirmPasswordEditText.getText().toString().trim();
        if (confirmPassword.isEmpty()) {
            viewListener.showEditTextError(confirmPasswordEditText, application.getString(R.string.this_field_can_not_be_blank));
            return;
        }
        if (!password.equals(confirmPassword)) {
            viewListener.showEditTextError(confirmPasswordEditText, application.getString(R.string.password_confirm_not_matching));
            return;
        }

        if (!termsIsChecked) {
            viewListener.showToastMessage(application.getString(R.string.terms_conditions_error_message));
            return;
        }

        setProgress(View.VISIBLE);
        model.signUp(new UserObj(fullName, userName, email, phoneCode + phone, password, countryName.get().getId()), this);


    }


    private boolean checkMailValidation(String email) {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";
        CharSequence inputStr = email;
        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if (matcher.matches())
            return true;
        else
            return false;
    }

    protected void requestOnActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SignupActivity.PICK_COUNTRY_REQUEST && resultCode == RESULT_OK && data != null) {
            if (data.getExtras().getInt("flag") == 0)
                setCountry((CountryObj) data.getExtras().getSerializable("country"));
            else setCountryName((CountryObj) data.getExtras().getSerializable("country"));


        }
    }

    protected interface ViewListener {
        void showToastMessage(String message);

        void showEditTextError(EditText editText, String errorMessage);

        void signUpDone();
    }
}
