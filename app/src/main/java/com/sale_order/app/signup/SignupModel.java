package com.sale_order.app.signup;

import android.content.Context;
import android.util.Log;
import android.view.View;

import androidx.lifecycle.MutableLiveData;

import com.sale_order.app.R;
import com.sale_order.app.classes.CountryResponse;
import com.sale_order.app.classes.CountryObj;
import com.sale_order.app.classes.SignupResponse;
import com.sale_order.app.classes.UserObj;
import com.sale_order.app.utils.Constants;
import com.sale_order.app.utils.MySingleton;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignupModel {
    private Context context;

    public SignupModel(Context context) {
        this.context = context;
    }

    private MutableLiveData<List<CountryObj>> countriesMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<List<CountryObj>> fetchCountries(String language, final ModelCallback callback) {
        Call<CountryResponse> call = MySingleton.getInstance(context).createService()
                .fetchCountries(language.equals(context.getString(R.string.arabic_key)) ? null : language);

        call.enqueue(new Callback<CountryResponse>() {
            @Override
            public void onResponse(Call<CountryResponse> call, Response<CountryResponse> response) {
                callback.setProgress(View.GONE);
                callback.setSignButtonClickable(true);
                if (response.body() != null) {
                    List<CountryObj> countries = response.body().getCountryObjList();
                    if (!countries.isEmpty()) {
                        callback.setCountry(countries.get(0));
                        callback.setCountryName(countries.get(0));
                        countriesMutableLiveData.setValue(countries);
                    }
                }

            }

            @Override
            public void onFailure(Call<CountryResponse> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.setSignButtonClickable(true);
                callback.onFailureHandler(t, 0);
            }
        });

        return countriesMutableLiveData;
    }


    protected void signUp(UserObj userObj, final ModelCallback callback) {
        Call<SignupResponse> call = MySingleton.getInstance(context).createService().signUp(userObj);
        call.enqueue(new Callback<SignupResponse>() {
            @Override
            public void onResponse(Call<SignupResponse> call, Response<SignupResponse> response) {
                callback.setProgress(View.GONE);
                if (response.body() != null) {
                    callback.setMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<SignupResponse> call, Throwable t) {
                Log.e("ssssssss", t.toString());
                callback.setProgress(View.GONE);
                callback.onFailureHandler(t, 1);
            }
        });

    }


    protected interface ModelCallback {

        void setProgress(int progress);

        void setCountry(CountryObj country);

        void setCountryName(CountryObj county);

        void onFailureHandler(Throwable t, int index);

        void setSignButtonClickable(boolean clickable);

        void setMessage(String message);
    }
}
