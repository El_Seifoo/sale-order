package com.sale_order.app.signup.countries;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.sale_order.app.R;
import com.sale_order.app.classes.CountryObj;
import com.sale_order.app.databinding.CountriesListItemBinding;

import java.util.ArrayList;

public class CountriesAdapter extends RecyclerView.Adapter<CountriesAdapter.Holder> {
    private ArrayList<CountryObj> countries;
    private final onCountryItemClicked listener;
    private int flag;

    public CountriesAdapter(ArrayList<CountryObj> countries, int flag, onCountryItemClicked listener) {
        this.listener = listener;
        this.flag = flag;
        this.countries = countries;
        notifyDataSetChanged();
    }

    protected interface onCountryItemClicked {
        void onClick(CountryObj country);
    }

    @NonNull
    @Override
    public CountriesAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder((CountriesListItemBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.countries_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CountriesAdapter.Holder holder, int position) {
        holder.binding.setCountry(countries.get(position));
        holder.binding.setFlag(flag);
    }

    @Override
    public int getItemCount() {
        return countries != null ? countries.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CountriesListItemBinding binding;

        public Holder(@NonNull CountriesListItemBinding itemBinding) {
            super(itemBinding.getRoot());
            binding = itemBinding;
            binding.getRoot().setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onClick(countries.get(getAdapterPosition()));
        }
    }


}
