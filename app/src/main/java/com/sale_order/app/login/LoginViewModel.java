package com.sale_order.app.login;

import android.app.Application;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.sale_order.app.R;
import com.sale_order.app.classes.UserInfoResponse;
import com.sale_order.app.classes.UserLoginObj;
import com.sale_order.app.main.MainActivity;
import com.sale_order.app.utils.Constants;
import com.sale_order.app.utils.MySingleton;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.app.Activity.RESULT_OK;
import static androidx.constraintlayout.widget.Constraints.TAG;

public class LoginViewModel extends AndroidViewModel implements LoginModel.ModelCallback {
    private Application application;
    private LoginModel model;
    private ViewListener viewListener;
    private ObservableField<Integer> progress;

    public LoginViewModel(@NonNull Application application) {
        super(application);
        this.application = application;
        model = new LoginModel(application);
        progress = new ObservableField<>(View.GONE);
        generateFirebaseToken();
    }

    private void generateFirebaseToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.e("LoginActivity", "getInstanceId failed", task.getException());
                            return;
                        }
                        // Get new Instance ID token
                        String token = task.getResult().getToken();


                        MySingleton.getInstance(application).saveStringToSharedPref(Constants.FIREBASE_TOKEN, token);
                    }
                });
    }

    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public void requestLogin(EditText usernameEditText, EditText passwordEditText) {
        String username = usernameEditText.getText().toString().trim();
        if (username.isEmpty()) {
            viewListener.showEditTextError(usernameEditText, application.getString(R.string.this_field_can_not_be_blank));
            return;
        }

        String password = passwordEditText.getText().toString().trim();
        if (password.isEmpty()) {
            viewListener.showEditTextError(passwordEditText, application.getString(R.string.this_field_can_not_be_blank));
            return;
        }

        setProgress(View.VISIBLE);
        String firebaseToken = MySingleton.getInstance(application).getStringFromSharedPref(Constants.FIREBASE_TOKEN, "");
        if (firebaseToken.isEmpty())
            generateFirebaseToken();
        firebaseToken = MySingleton.getInstance(application).getStringFromSharedPref(Constants.FIREBASE_TOKEN, "");
        model.logIn(new UserLoginObj(username, password, firebaseToken), this);
    }


    public ObservableField<Integer> getProgress() {
        return progress;
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }

    @Override
    public void onFailureHandler(Throwable t) {
        viewListener.showToastMessage(t instanceof IOException ? application.getString(R.string.no_internet_connection) :
                application.getString(R.string.something_went_wrong));
    }

    @Override
    public void handleLoginResponse(UserInfoResponse userInfoResponse) {
        if (userInfoResponse.getUser() != null) {
            //login ...
            MySingleton.getInstance(application).saveUserData(userInfoResponse.getUser().getUserInfo());
            MySingleton.getInstance(application).saveStringToSharedPref(Constants.USER_TOKEN, userInfoResponse.getUser().getTokenObj().getToken());
            MySingleton.getInstance(application).loginUser();
            viewListener.loginDone();
            return;
        }
        String message = userInfoResponse.getMessage();
        if (message.equals(application.getString(R.string.login_response_activation_error))) {
            // ask user to activate ...
            viewListener.navigateActivation();
            return;
        }
        viewListener.showToastMessage(message.equals(application.getString(R.string.login_response_invalid_data)) ? application.getString(R.string.invalid_email_password)
                : message.equals(application.getString(R.string.login_response_blocked_user))? application.getString(R.string.ur_account_is_blocked)
                :application.getString(R.string.something_went_wrong));
    }

    public void checkIntent(Intent intent) {
        if (intent.hasExtra("email") && intent.hasExtra("username")&& intent.hasExtra("password"))
            viewListener.setEmailPassword(intent.getExtras().getString("email"), intent.getExtras().getString("username"), intent.getExtras().getString("password"));
    }

    private static final int ACTIVATE_USER_REQUEST_CODE = 1;

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ACTIVATE_USER_REQUEST_CODE && resultCode == RESULT_OK && data != null) {
            viewListener.setEmailPassword(data.getExtras().getString("email"), data.getExtras().getString("username"), data.getExtras().getString("password"));
        }
    }

    public void onChangePasswordClicked() {
        viewListener.navigateSendCode();
    }

    protected interface ViewListener {
        void showToastMessage(String message);

        void showEditTextError(EditText editText, String errorMessage);

        void loginDone();

        void setEmailPassword(String email,String username, String password);

        void navigateActivation();

        void navigateSendCode();
    }
}
