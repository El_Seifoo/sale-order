package com.sale_order.app.login;

public interface Presenter {
    void onLoginButtonClicked();

    void onForgetPasswordClicked();
}
