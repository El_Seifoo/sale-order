package com.sale_order.app.login;

import android.content.Context;
import android.util.Log;
import android.view.View;

import androidx.lifecycle.MutableLiveData;

import com.sale_order.app.classes.LoginResponse;
import com.sale_order.app.classes.SignupResponse;
import com.sale_order.app.classes.UserInfoData;
import com.sale_order.app.classes.UserInfoResponse;
import com.sale_order.app.classes.UserLoginObj;

import com.sale_order.app.utils.MySingleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginModel {
    private Context context;

    protected LoginModel(Context context) {
        this.context = context;
    }


    protected void logIn(UserLoginObj userObj, final ModelCallback callback) {
        Log.e("userObj", userObj.toString());
        Call<UserInfoResponse> call = MySingleton.getInstance(context).createService().logIn(userObj);
        call.enqueue(new Callback<UserInfoResponse>() {
            @Override
            public void onResponse(Call<UserInfoResponse> call, Response<UserInfoResponse> response) {
                callback.setProgress(View.GONE);
                if (response.code() == 200)
                    if (response.body() != null) {
                        callback.handleLoginResponse(response.body());
                    }
            }

            @Override
            public void onFailure(Call<UserInfoResponse> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.onFailureHandler(t);
            }
        });
    }

    protected interface ModelCallback {
        void setProgress(int progress);

        void onFailureHandler(Throwable t);

        void handleLoginResponse(UserInfoResponse response);
    }
}
