package com.sale_order.app.login;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sale_order.app.R;
import com.sale_order.app.activation.ActivationActivity;
import com.sale_order.app.changePassword.ChangePasswordActivity;
import com.sale_order.app.classes.UserInfoResponse;
import com.sale_order.app.databinding.ActivityLoginBinding;
import com.sale_order.app.main.MainActivity;
import com.sale_order.app.sendCode.SendCodeActivity;
import com.sale_order.app.utils.Constants;
import com.sale_order.app.utils.Language;
import com.sale_order.app.utils.MySingleton;

public class LoginActivity extends AppCompatActivity implements LoginViewModel.ViewListener, Presenter {
    private static final int ACTIVATE_USER_REQUEST_CODE = 1;
    private TextView forgetPassTextView;
    private LoginViewModel viewModel;
    private ActivityLoginBinding dataBinding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.arabic_key)));
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        viewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewModel.checkIntent(getIntent());
        forgetPassTextView = findViewById(R.id.login_forget_password_text);
        forgetPassTextView.setText(Html.fromHtml(getString(R.string.forget_password) +
                        "<font><strong>" + getString(R.string.get_new) + "</strong></font>"),
                TextView.BufferType.SPANNABLE);

    }


    @Override
    public void onLoginButtonClicked() {
        viewModel.requestLogin(dataBinding.loginUsernameEditText, dataBinding.loginPasswordEditText);
    }

    @Override
    public void onForgetPasswordClicked() {
        viewModel.onChangePasswordClicked();
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showEditTextError(EditText editText, String error) {
        editText.setError(error);
    }

    @Override
    public void loginDone() {
        if (getIntent().hasExtra("BiddingActivity") || getIntent().hasExtra("PrevAuctionDetailActivity")
                || getIntent().hasExtra("CreditFragment") || getIntent().hasExtra("MyAccountActivity")
                || getIntent().hasExtra("ProductActivity") || getIntent().hasExtra("NotificationsActivity")
                || getIntent().hasExtra("MyPointsActivity") || getIntent().hasExtra("CustomPointsActivity")) {
            setResult(RESULT_OK, getIntent());
            finish();
            return;
        }
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void setEmailPassword(String email, String username, String password) {
        dataBinding.loginUsernameEditText.setText(username);
        dataBinding.loginPasswordEditText.setText(password);
//        viewModel.requestLogin(dataBinding.loginUsernameEditText, dataBinding.loginPasswordEditText);
    }

    @Override
    public void navigateActivation() {
        showToastMessage(getString(R.string.account_not_activated));
        Intent activateIntent = new Intent(this, ActivationActivity.class);
        activateIntent.putExtra("username", dataBinding.loginUsernameEditText.getText().toString());
        activateIntent.putExtra("password", dataBinding.loginPasswordEditText.getText().toString());
        activateIntent.putExtra("flag", true);
        startActivityForResult(activateIntent, ACTIVATE_USER_REQUEST_CODE);
    }

    @Override
    public void navigateSendCode() {
        startActivity(new Intent(this, SendCodeActivity.class));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        viewModel.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
