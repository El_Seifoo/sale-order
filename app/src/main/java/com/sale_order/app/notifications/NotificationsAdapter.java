package com.sale_order.app.notifications;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.sale_order.app.R;
import com.sale_order.app.classes.NotificationObj;
import com.sale_order.app.databinding.NotificationsListItemBinding;

import java.util.List;

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.Holder> {
    private List<NotificationObj> notifications;

    public void setNotifications(List<NotificationObj> notifications) {
        this.notifications = notifications;
        notifyDataSetChanged();
    }

    public void addNotifications(List<NotificationObj> notifications) {
        this.notifications.addAll(notifications);
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder((NotificationsListItemBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.notifications_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        holder.dataBinding.setNotification(notifications.get(position));
    }

    @Override
    public int getItemCount() {
        return notifications != null ? notifications.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder {
        private NotificationsListItemBinding dataBinding;

        public Holder(@NonNull NotificationsListItemBinding dataBinding) {
            super(dataBinding.getRoot());
            this.dataBinding = dataBinding;
        }
    }
}
