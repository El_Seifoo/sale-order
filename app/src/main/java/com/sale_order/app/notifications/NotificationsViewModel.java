package com.sale_order.app.notifications;

import android.app.Application;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.sale_order.app.R;
import com.sale_order.app.classes.NotificationObj;
import com.sale_order.app.utils.MySingleton;

import java.io.IOException;
import java.util.List;

import static android.app.Activity.RESULT_OK;

public class NotificationsViewModel extends AndroidViewModel implements NotificationsModel.ModelCallback {
    private Application application;
    private NotificationsModel model;
    private ViewListener viewListener;
    private ObservableField<Integer> progress;
    private ObservableField<Integer> emptyListTextView;
    private ObservableField<String> errorMessage;
    private ObservableField<Integer> errorView;

    public NotificationsViewModel(@NonNull Application application) {
        super(application);
        this.application = application;
        model = new NotificationsModel(application);
        progress = new ObservableField<>(View.GONE);
        emptyListTextView = new ObservableField<>(View.GONE);
        errorView = new ObservableField<>(View.GONE);
        errorMessage = new ObservableField<>("");
    }

    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public void onRetryClickListener(View view) {
        setProgress(View.VISIBLE);
        setErrorView(View.GONE);
        model.fetchNotifications(skip, take, language, this);
    }

    private int skip;
    private int take;
    private String language;

    protected MutableLiveData<List<NotificationObj>> requestNotifications(int skip, int take, String language) {
        this.skip = skip;
        this.take = take;
        this.language = language;
        setProgress(View.VISIBLE);
        return model.fetchNotifications(skip, take, language, this);
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    public ObservableField<Integer> getEmptyListTextView() {
        return emptyListTextView;
    }

    public ObservableField<String> getErrorMessage() {
        return errorMessage;
    }

    protected void setErrorMessage(String message) {
        errorMessage.set(message);
    }

    public ObservableField<Integer> getErrorView() {
        return errorView;
    }

    protected void setErrorView(int errorViewStatus) {
        this.errorView.set(errorViewStatus);
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }

    @Override
    public void setEmptyListTextView(int empty) {
        this.emptyListTextView.set(empty);
    }

    @Override
    public void onFailureHandler(Throwable t) {
        setErrorView(View.VISIBLE);
        Log.e("error", t.toString());
        if (t instanceof IOException)
            setErrorMessage(application.getString(R.string.no_internet_connection));
        else
            setErrorMessage(application.getString(R.string.error_fetching_data));
    }

    @Override
    public void loginFirst() {
        MySingleton.getInstance(getApplication()).logout();
        viewListener.LoginFirst();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == NotificationsActivity.LOGIN_FIRST_REQUEST_CODE && resultCode == RESULT_OK)
            this.requestNotifications(skip, take, language);
    }

    protected interface ViewListener {
        void LoginFirst();
    }

}
