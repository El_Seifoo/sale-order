package com.sale_order.app.notifications;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.sale_order.app.R;
import com.sale_order.app.classes.NotificationObj;
import com.sale_order.app.databinding.ActivityNotificationsBinding;
import com.sale_order.app.login.LoginActivity;
import com.sale_order.app.utils.Constants;
import com.sale_order.app.utils.Language;
import com.sale_order.app.utils.MySingleton;

import java.util.List;

public class NotificationsActivity extends AppCompatActivity implements NotificationsViewModel.ViewListener {
    protected static final int LOGIN_FIRST_REQUEST_CODE = 1;
    private NotificationsViewModel viewModel;
    private ActivityNotificationsBinding dataBinding;
    private NotificationsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.arabic_key)));
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_notifications);
        viewModel = ViewModelProviders.of(this).get(NotificationsViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);

        getSupportActionBar().setTitle(getString(R.string.notifications));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        dataBinding.notificationsList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        adapter = new NotificationsAdapter();
        dataBinding.notificationsList.setAdapter(adapter);

        viewModel.requestNotifications(0, 100,
                MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE, getString(R.string.english_key))).observe(this, new Observer<List<NotificationObj>>() {
            @Override
            public void onChanged(List<NotificationObj> notificationObjs) {
                adapter.setNotifications(notificationObjs);
            }
        });
    }

    @Override
    public void LoginFirst() {
        Intent loginIntent = new Intent(this, LoginActivity.class);
        loginIntent.putExtra("NotificationsActivity", "");
        startActivityForResult(loginIntent, LOGIN_FIRST_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        viewModel.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
