package com.sale_order.app.notifications;

import android.content.Context;
import android.view.View;

import androidx.lifecycle.MutableLiveData;

import com.sale_order.app.R;
import com.sale_order.app.classes.NotificationObj;
import com.sale_order.app.classes.NotificationsResponse;
import com.sale_order.app.utils.Constants;
import com.sale_order.app.utils.MySingleton;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationsModel {
    private Context context;

    public NotificationsModel(Context context) {
        this.context = context;
    }

    private MutableLiveData<List<NotificationObj>> notificationsMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<List<NotificationObj>> fetchNotifications(final int skip, int take, String language, final ModelCallback callback) {

        Map<String, Object> map = new HashMap<>();
        map.put("skip", skip);
        map.put("take", take);
        if (language.equals(context.getString(R.string.english_key)))
            map.put("Ln", language);

        Call<NotificationsResponse> call = MySingleton.getInstance(context).createService()
                .notifications(MySingleton.getInstance(context).getStringFromSharedPref(Constants.USER_TOKEN, ""), map);
        call.enqueue(new Callback<NotificationsResponse>() {
            @Override
            public void onResponse(Call<NotificationsResponse> call, Response<NotificationsResponse> response) {
                callback.setProgress(View.GONE);
                if (response.code() == 401) {
                    callback.loginFirst();
                    return;
                }
                if (response.code() == 200) {
                    if (response.body() != null) {
                        List<NotificationObj> notifications = response.body().getNotifications();
                        if (skip == 0 && notifications == null) {
                            callback.setEmptyListTextView(View.VISIBLE);
                            return;
                        }
                        if (skip == 0 && notifications.isEmpty())
                            callback.setEmptyListTextView(View.VISIBLE);
                        notificationsMutableLiveData.setValue(notifications);
                    }
                }
            }

            @Override
            public void onFailure(Call<NotificationsResponse> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.onFailureHandler(t);
            }
        });
        return notificationsMutableLiveData;
    }

    protected interface ModelCallback {
        void setProgress(int progress);

        void setEmptyListTextView(int empty);

        void onFailureHandler(Throwable t);

        void loginFirst();
    }
}
