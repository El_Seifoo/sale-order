package com.sale_order.app.classes;

import com.google.gson.annotations.SerializedName;
import com.sale_order.app.main.shopping.product.SliderAdapter;

import java.util.List;

public class AuctionsResponse {
    @SerializedName("Data")
    private List<AuctionObj> auctions;
    @SerializedName("SliderData")
    private SliderObj slider;

    public List<AuctionObj> getAuctions() {
        return auctions;
    }

    public SliderObj getSlider() {
        return slider;
    }
}
