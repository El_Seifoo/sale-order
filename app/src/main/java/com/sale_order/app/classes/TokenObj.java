package com.sale_order.app.classes;

import com.google.gson.annotations.SerializedName;

public class TokenObj {
    @SerializedName("Token")
    private String token;

    public TokenObj(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }
}
