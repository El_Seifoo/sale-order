package com.sale_order.app.classes;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OffersResponse {
    @SerializedName("Message")
    private String message;
    @SerializedName("Data")
    private OfferObj offers;

    public String getMessage() {
        return message;
    }

    public OfferObj getOffers() {
        return offers;
    }
}
