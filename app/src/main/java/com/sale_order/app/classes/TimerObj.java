package com.sale_order.app.classes;

import com.google.gson.annotations.SerializedName;

public class TimerObj {
    @SerializedName("Days")
    private int days;
    @SerializedName("Hours")
    private int hours;
    @SerializedName("Minutes")
    private int minutes;
    @SerializedName("Seconds")
    private int seconds;
    @SerializedName("StartIn")
    private String startIn;
    @SerializedName("EndIn")
    private String endIn;

    @Override
    public String toString() {
        return "TimerObj{" +
                "days=" + days +
                ", hours=" + hours +
                ", minutes=" + minutes +
                ", seconds=" + seconds +
                ", startIn='" + startIn + '\'' +
                ", endIn='" + endIn + '\'' +
                '}';
    }

    public TimerObj(int days, int hours, int minutes, int seconds, String startIn, String endIn) {
        this.days = days;
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
        this.startIn = startIn;
        this.endIn = endIn;
    }


    public int getDays() {
        return days;
    }

    public int getHours() {
        return hours;
    }

    public int getMinutes() {
        return minutes;
    }

    public int getSeconds() {
        return seconds;
    }

    public String getStartIn() {
        return startIn;
    }

    public String getEndIn() {
        return endIn;
    }
}
