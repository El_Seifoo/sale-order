package com.sale_order.app.classes;

import com.google.gson.annotations.SerializedName;

public class UserLoginObj extends UserObj {
    @SerializedName("FireBaseAccesToken")
    private String firebaseToken;

    @Override
    public String toString() {
        return "UserLoginObj{" +
                "Super='" + super.toString() + "\'" +
                "firebaseToken='" + firebaseToken + '\'' +
                '}';
    }

    public UserLoginObj(String username, String password, String firebaseToken) {
        super(username, password);
        this.firebaseToken = firebaseToken;
    }

    public String getFirebaseToken() {
        return firebaseToken;
    }
}
