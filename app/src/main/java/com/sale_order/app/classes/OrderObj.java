package com.sale_order.app.classes;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OrderObj implements Serializable {
    @SerializedName("url")
    private String url;
    @SerializedName("OrderNo")
    private String orderNumber;
    @SerializedName("RedirectUrl")
    private String redirectUrl;
    @SerializedName(value = "ErrorMessage", alternate = "Message")
    private String errorMessage;

    public String getUrl() {
        return url;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
