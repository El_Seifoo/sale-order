package com.sale_order.app.classes;

import com.google.gson.annotations.SerializedName;

public class AuctionResponse {
    @SerializedName("Data")
    private AuctionObj auctionObj;

    public AuctionObj getAuctionObj() {
        return auctionObj;
    }
}
