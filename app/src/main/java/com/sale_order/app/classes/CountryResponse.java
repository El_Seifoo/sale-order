package com.sale_order.app.classes;

import com.google.gson.annotations.SerializedName;
import com.sale_order.app.classes.CountryObj;

import java.util.List;

public class CountryResponse {
    private String DevMessage, InnerException, Message, RequestType;
    @SerializedName("Data")
    private List<CountryObj> countryObjList;
    private String IsData, SliderData;

    public String getDevMessage() {
        return DevMessage;
    }

    public String getInnerException() {
        return InnerException;
    }

    public String getMessage() {
        return Message;
    }

    public String getRequestType() {
        return RequestType;
    }

    public String getIsData() {
        return IsData;
    }

    public String getSliderData() {
        return SliderData;
    }

    public List<CountryObj> getCountryObjList() {
        return countryObjList;
    }

    public CountryResponse(String devMessage, String innerException, String message, String requestType, String isData, String sliderData, List<CountryObj> countryObjList) {
        DevMessage = devMessage;
        InnerException = innerException;
        Message = message;
        RequestType = requestType;
        IsData = isData;
        SliderData = sliderData;
        this.countryObjList = countryObjList;
    }
}
