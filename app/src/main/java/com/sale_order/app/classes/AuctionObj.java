package com.sale_order.app.classes;

import android.widget.ImageView;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.databinding.library.baseAdapters.BR;

import com.bumptech.glide.Glide;
import com.google.gson.annotations.SerializedName;
import com.sale_order.app.R;
import com.sale_order.app.bidding.BiddingAdapter;

import java.util.List;

public class AuctionObj extends BaseObservable {
    @SerializedName("Id")
    private String id;
    @SerializedName("Title")
    private String title;
    @SerializedName("TitleAr")
    private String titleAr;
    @SerializedName("TitleEn")
    private String titleEn;
    @SerializedName("Description")
    private String description;
    @SerializedName("DescriptionAr")
    private String descriptionAr;
    @SerializedName("DescriptionEn")
    private String descriptionEn;
    @SerializedName("MinimumPointsForBid")
    private int minPointsForBid;
    @SerializedName("IsStarted")
    private boolean isStarted;
    @SerializedName("MinimumPrice")
    private double MinPrice;
    @SerializedName("IsFinshed")
    private boolean isFinished;
    @SerializedName("IsWin")
    private boolean isWin;
    @SerializedName("UserWin")
    private UserInfoObj userWin;
    @SerializedName("Timer")
    private TimerObj timer;
    @SerializedName("Images")
    private List<String> images;
    @SerializedName("BaseImage")
    private String baseImage;
    @SerializedName("PointPrice")
    private int pointPrice;
    @SerializedName("OriginalPrice")
    private double originalPrice;
    @SerializedName("TotalPointsPrice")
    private int totalPointsPrice;
    @SerializedName("IsPaid")
    private boolean isPaid;
    @SerializedName("IsCurrentUserWinner")
    private boolean isCurrentUserWinner;
    @SerializedName("BiddersInAuctions")
    private List<BidderObj> biddersInAuctions;
    @SerializedName("CurrentUser")
    private UserInfoObj currentUser;

    @Override
    public String toString() {
        return "AuctionObj{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", titleAr='" + titleAr + '\'' +
                ", titleEn='" + titleEn + '\'' +
                ", description='" + description + '\'' +
                ", descriptionAr='" + descriptionAr + '\'' +
                ", descriptionEn='" + descriptionEn + '\'' +
                ", minPointsForBid=" + minPointsForBid +
                ", isStarted=" + isStarted +
                ", MinPrice=" + MinPrice +
                ", isFinished=" + isFinished +
                ", isWin=" + isWin +
                ", userWin='" + userWin + '\'' +
                ", timer=" + timer +
                ", images=" + images +
                ", baseImage='" + baseImage + '\'' +
                ", pointPrice=" + pointPrice +
                ", originalPrice=" + originalPrice +
                ", totalPointsPrice=" + totalPointsPrice +
                ", isPaid=" + isPaid +
                ", isCurrentUserWinner=" + isCurrentUserWinner +
                ", biddersInAuctions=" + biddersInAuctions +
                ", currentUser=" + currentUser.toString() +
                '}';
    }

    public AuctionObj(String id, String title, String titleAr, String titleEn, String description, String descriptionAr,
                      String descriptionEn, int minPointsForBid, boolean isStarted, double minPrice, boolean isFinished,
                      boolean isWin, UserInfoObj userWin, TimerObj timer, List<String> images, String baseImage, int pointPrice,
                      double originalPrice, int totalPointsPrice, boolean isPaid, boolean isCurrentUserWinner, List<BidderObj> biddersInAuctions, UserInfoObj currentUser) {
        this.id = id;
        this.title = title;
        this.titleAr = titleAr;
        this.titleEn = titleEn;
        this.description = description;
        this.descriptionAr = descriptionAr;
        this.descriptionEn = descriptionEn;
        this.minPointsForBid = minPointsForBid;
        this.isStarted = isStarted;
        MinPrice = minPrice;
        this.isFinished = isFinished;
        this.isWin = isWin;
        this.userWin = userWin;
        this.timer = timer;
        this.images = images;
        this.baseImage = baseImage;
        this.pointPrice = pointPrice;
        this.originalPrice = originalPrice;
        this.totalPointsPrice = totalPointsPrice;
        this.isPaid = isPaid;
        this.isCurrentUserWinner = isCurrentUserWinner;
        this.biddersInAuctions = biddersInAuctions;
        this.currentUser = currentUser;
    }

    @BindingAdapter("baseImage")
    public static void loadImage(ImageView view, String baseImage) {
        Glide.with(view.getContext())
                .load(baseImage)
                .error(R.mipmap.logo)
                .into(view);
    }


    public void setTimer(TimerObj timer) {
        this.timer = timer;
        notifyPropertyChanged(BR.timer);
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getTitleAr() {
        return titleAr;
    }

    public String getTitleEn() {
        return titleEn;
    }

    public String getDescription() {
        return description;
    }

    public String getDescriptionAr() {
        return descriptionAr;
    }

    public String getDescriptionEn() {
        return descriptionEn;
    }

    public int getMinPointsForBid() {
        return minPointsForBid;
    }

    public boolean isStarted() {
        return isStarted;
    }

    public double getMinPrice() {
        return MinPrice;
    }

    public boolean isFinished() {
        return isFinished;
    }

    public boolean isWin() {
        return isWin;
    }

    public UserInfoObj getUserWin() {
        return userWin;
    }

    @Bindable
    public TimerObj getTimer() {
        return timer;
    }

    public List<String> getImages() {
        return images;
    }

    public String getBaseImage() {
        return baseImage;
    }

    public int getPointPrice() {
        return pointPrice;
    }

    public double getOriginalPrice() {
        return originalPrice;
    }

    public int getTotalPointsPrice() {
        return totalPointsPrice;
    }

    public boolean isPaid() {
        return isPaid;
    }

    public boolean isCurrentUserWinner() {
        return isCurrentUserWinner;
    }

    public List<BidderObj> getBiddersInAuctions() {
        return biddersInAuctions;
    }

    public UserInfoObj getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(UserInfoObj currentUser) {
        this.currentUser = currentUser;
    }
}
