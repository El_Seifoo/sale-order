package com.sale_order.app.classes;

import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.BaseObservable;
import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.google.gson.annotations.SerializedName;
import com.sale_order.app.R;

import java.io.Serializable;


public class CountryObj implements Serializable {
    @SerializedName("Id")
    private String id;
    @SerializedName("Name")
    private String name;
    @SerializedName("CountryKey")
    private String key;
    @SerializedName("CountryFlag")
    private String flag;

    @NonNull
    @Override
    public String toString() {
        return "ID= " + id
                + "\nName= " + name
                + "\nKey= " + key
                + "\nflag= " + flag;
    }

    public CountryObj(String id, String name, String key, String flag) {
        this.id = id;
        this.name = name;
        this.key = key;
        this.flag = flag;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getKey() {
        return key;
    }

    public String getFlag() {
        return flag;
    }

    @BindingAdapter("flag")
    public static void loadImage(ImageView view, String flag) {
        Glide.with(view.getContext())
                .load(flag)
                .error(R.mipmap.logo)
                .into(view);
    }

}
