package com.sale_order.app.classes;

import com.google.gson.annotations.SerializedName;

public class SignupResponse {
    @SerializedName("Message")
    private String message;

    public SignupResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
