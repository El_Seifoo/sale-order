package com.sale_order.app.classes;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CreditResponse {
    @SerializedName("Data")
    private List<CreditObj> credits;

    public CreditResponse(List<CreditObj> credits) {
        this.credits = credits;
    }

    public List<CreditObj> getCredits() {
        return credits;
    }
}
