package com.sale_order.app.classes;

import com.google.gson.annotations.SerializedName;

public class TermsConditionsResponse {
    @SerializedName("Data")
    private String data;

    public TermsConditionsResponse(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }
}
