package com.sale_order.app.classes;

import com.google.gson.annotations.SerializedName;

public class NotificationObj {
    @SerializedName("Date")
    private String date;
    @SerializedName("Id")
    private String id;
    @SerializedName("ObjectDiscription")
    private String body;
    @SerializedName("ObjectId")
    private String objectId;
    @SerializedName("ObjectNme")
    private String objectName;
    @SerializedName("AuctionUserNameWinner")
    private String winner;

    public NotificationObj(String date, String id, String body, String objectId, String objectName, String winner) {
        this.date = date;
        this.id = id;
        this.body = body;
        this.objectId = objectId;
        this.objectName = objectName;
        this.winner = winner;
    }

    public String getDate() {
        return date;
    }

    public String getId() {
        return id;
    }

    public String getBody() {
        return body;
    }

    public String getObjectId() {
        return objectId;
    }

    public String getObjectName() {
        return objectName;
    }

    public String getWinner() {
        return winner;
    }


}
