package com.sale_order.app.classes;

import com.google.gson.annotations.SerializedName;

public class UserInfoData {
    @SerializedName("UserInformation")
    private UserInfoObj userInfo;
    @SerializedName("AccessToken")
    private TokenObj tokenObj;



    public UserInfoData(UserInfoObj userInfo, TokenObj tokenObj) {
        this.userInfo = userInfo;
        this.tokenObj = tokenObj;
    }

    public UserInfoObj getUserInfo() {
        return userInfo;
    }

    public TokenObj getTokenObj() {
        return tokenObj;
    }
}
