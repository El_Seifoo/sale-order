package com.sale_order.app.classes;

import com.google.gson.annotations.SerializedName;

public class OrderPayment {
    @SerializedName("Rresult")
    private String result;
    @SerializedName("Status")
    private String status;

    public String getResult() {
        return result;
    }

    public String getStatus() {
        return status;
    }
}
