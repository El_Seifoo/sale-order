package com.sale_order.app.classes;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.google.gson.annotations.SerializedName;
import com.sale_order.app.R;

public class BidderObj {
    @SerializedName("Id")
    private String id;
    @SerializedName("UserName")
    private String name;
    @SerializedName("PointsPrice")
    private double price;
    @SerializedName("CountryFlagUrl")
    private String country;


    public BidderObj(String id, String name, double price, String country) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.country = country;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public String getCountry() {
        return country;
    }

    @BindingAdapter("country")
    public static void loadImage(ImageView view, String country) {
        Glide.with(view.getContext())
                .load(country)
                .error(R.mipmap.logo)
                .into(view);
    }

}
