package com.sale_order.app.classes;

import com.google.gson.annotations.SerializedName;

public class UserInfoResponse {
    @SerializedName("Data")
    private UserInfoData user;
    @SerializedName("Message")
    private String message;

    public UserInfoResponse(UserInfoData user, String message) {
        this.user = user;
        this.message = message;
    }

    public UserInfoData getUser() {
        return user;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
