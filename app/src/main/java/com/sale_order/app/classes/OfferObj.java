package com.sale_order.app.classes;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OfferObj {
    @SerializedName("Title")
    private String title;
    @SerializedName("Description")
    private String description;
    @SerializedName("EndDate")
    private String endDate;
    @SerializedName("IsFinshed")
    private boolean finished;
    @SerializedName("IsCanBeLoop")
    private boolean canBeLoop;
    @SerializedName("OfferSubscription")
    private List<SubscriberObj> subscribers;


    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getEndDate() {
        return endDate;
    }

    public boolean isFinished() {
        return finished;
    }

    public boolean isCanBeLoop() {
        return canBeLoop;
    }

    public List<SubscriberObj> getSubscribers() {
        return subscribers;
    }

    public void setSubscribers(List<SubscriberObj> subscribers) {
        this.subscribers = subscribers;
    }
}
