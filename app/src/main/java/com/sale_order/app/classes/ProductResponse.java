package com.sale_order.app.classes;

import com.google.gson.annotations.SerializedName;

public class ProductResponse {
    @SerializedName("Data")
    private ShoppingObj product;

    public ShoppingObj getProduct() {
        return product;
    }
}
