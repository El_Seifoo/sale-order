package com.sale_order.app.classes;

public class ActivationObj {
    private String email,password;

    public ActivationObj(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
