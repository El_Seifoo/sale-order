package com.sale_order.app.classes;

import com.google.gson.annotations.SerializedName;

public class SliderObj {

    @Override
    public String toString() {
        return "SliderObj{" +
                "image='" + image + '\'' +
                ", imageVideo='" + imageVideo + '\'' +
                ", instaImage='" + instaImage + '\'' +
                ", instaVideo='" + instaVideo + '\'' +
                '}';
    }

    @SerializedName("VideoImage")
    private String image;
    @SerializedName("Video")
    private String imageVideo;
    @SerializedName("InstagramImage")
    private String instaImage;
    @SerializedName("Instagram")
    private String instaVideo;


    public SliderObj(String image, String imageVideo, String instaImage, String instaVideo) {
        this.image = image;
        this.imageVideo = imageVideo;
        this.instaImage = instaImage;
        this.instaVideo = instaVideo;
    }

    public String getImage() {
        return image;
    }

    public String getImageVideo() {
        return imageVideo;
    }

    public String getInstaImage() {
        return instaImage;
    }

    public String getInstaVideo() {
        return instaVideo;
    }
}
