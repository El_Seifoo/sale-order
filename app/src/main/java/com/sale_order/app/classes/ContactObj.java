package com.sale_order.app.classes;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;

import com.google.gson.annotations.SerializedName;

public class ContactObj extends BaseObservable {

    @SerializedName("Message")
    private String message;
    @SerializedName("Phone")
    private String phone;
    @SerializedName("UserName")
    private String userName;
    @SerializedName("Email")
    private String email;

    @Override
    public String toString() {
        return "ContactObj{" +
                "message='" + message + '\'' +
                ", phone='" + phone + '\'' +
                ", userName='" + userName + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    public ContactObj(String message, String phone, String userName, String email) {
        this.message = message;
        this.phone = phone;
        this.userName = userName;
        this.email = email;
    }

    @Bindable
    public String getMessage() {
        return message;
    }

    @Bindable
    public String getPhone() {
        return phone;
    }

    @Bindable
    public String getUserName() {
        return userName;
    }

    @Bindable
    public String getEmail() {
        return email;
    }

    public void setMessage(String message) {
        this.message = message;
        notifyPropertyChanged(BR.message);
    }

    public void setPhone(String phone) {
        this.phone = phone;
        notifyPropertyChanged(BR.phone);
    }

    public void setUserName(String userName) {
        this.userName = userName;
        notifyPropertyChanged(BR.userName);
    }

    public void setEmail(String email) {
        this.email = email;
        notifyPropertyChanged(BR.email);
    }
}
