package com.sale_order.app.classes;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotificationsResponse {
    @SerializedName("Data")
    private List<NotificationObj> notifications;

    public List<NotificationObj> getNotifications() {
        return notifications;
    }
}
