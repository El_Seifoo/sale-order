package com.sale_order.app.classes;

import com.google.gson.annotations.SerializedName;

public class OrderResponse {
    @SerializedName("Data")
    private OrderObj order;
    @SerializedName("Message")
    private String message;

    public OrderObj getOrder() {
        return order;
    }

    public String getMessage() {
        return message;
    }
}
