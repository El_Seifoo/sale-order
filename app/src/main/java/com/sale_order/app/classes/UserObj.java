package com.sale_order.app.classes;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserObj implements Serializable {
    @SerializedName("FullName")
    private String fullName;
    @SerializedName("UserName")
    private String userName;
    @SerializedName("Email")
    private String email;
    @SerializedName(value = "PhoneNumber", alternate = "Phone")
    private String phoneNumber;
    @SerializedName("Password")
    private String password;
    @SerializedName("CountryId")
    private String countryId;

    @Override
    public String toString() {
        return "UserObj{" +
                "userName='" + userName + '\'' +
                ", email='" + email + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public UserObj() {
    }

    // signUp
    public UserObj(String fullName, String userName, String email, String phoneNumber, String password, String countryId) {
        this.fullName = fullName;
        this.userName = userName;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.countryId = countryId;
    }

    // update user info
    public UserObj(String userName, String email, String phoneNumber) {
        this.userName = userName;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }

    // login
    public UserObj(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getUserName() {
        return userName;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getPassword() {
        return password;
    }
}
