package com.sale_order.app.classes;

import com.google.gson.annotations.SerializedName;

public class CreditObj {

    @SerializedName("Id")
    private String id;
    @SerializedName("PointPackageName")
    private String name;
    @SerializedName("Points")
    private int points;
    @SerializedName("Price")
    private double price;

    public CreditObj(String id, String name, int points, double price) {
        this.id = id;
        this.name = name;
        this.points = points;
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPoints() {
        return points;
    }

    public double getPrice() {
        return price;
    }
}
