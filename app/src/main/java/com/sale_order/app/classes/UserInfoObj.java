package com.sale_order.app.classes;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.google.gson.annotations.SerializedName;
import com.sale_order.app.R;

public class UserInfoObj extends UserObj {
    @SerializedName("Id")
    private String id;
    @SerializedName(value = "Image", alternate = "Base64StringFile")
    private String profilePic;
    @SerializedName("Points")
    private double points;
    @SerializedName("CountryFlagUrl")
    private String countryFlag;

    @Override
    public String toString() {
        return "UserInfoObj{" +
                "id='" + id + '\'' +
                ", profilePic='" + profilePic + '\'' +
                ", points=" + points +
                ", countryFlag='" + countryFlag + '\'' +
                ", super='" + super.toString() + '\'' +
                '}';
    }

    public UserInfoObj() {
    }


//    public UserInfoObj(String userName, String email, String phoneNumber, String password,
//                       String id, String profilePic, double points, String countryFlag) {
//        super(userName, email, phoneNumber, password);
//        this.id = id;
//        this.profilePic = profilePic;
//        this.points = points;
//        this.countryFlag = countryFlag;
//    }

    public UserInfoObj(String userName, String email, String phoneNumber) {
        super(userName, email, phoneNumber);
    }

    @BindingAdapter("profilePic")
    public static void loadImage(ImageView view, String profilePic) {
        Glide.with(view.getContext())
                .load(profilePic)
                .error(R.mipmap.logo)
                .into(view);
    }

    public String getId() {
        return id;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public double getPoints() {
        return points;
    }

    public String getCountryFlag() {
        return countryFlag;
    }
}
