package com.sale_order.app.classes;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ShoppingResponse {
    @SerializedName("Data")
    private List<ShoppingObj> shoppingList;

    public List<ShoppingObj> getShoppingList() {
        return shoppingList;
    }
}
