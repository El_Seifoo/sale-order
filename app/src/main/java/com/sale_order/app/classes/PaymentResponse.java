package com.sale_order.app.classes;

import com.google.gson.annotations.SerializedName;

public class PaymentResponse {
    @SerializedName("Data")
    private OrderPayment orderPayment;
    @SerializedName("Message")
    private String message;

    public String getMessage() {
        return message;
    }

    public OrderPayment getOrderPayment() {
        return orderPayment;
    }
}
