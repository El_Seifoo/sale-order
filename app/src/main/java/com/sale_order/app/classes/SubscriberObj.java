package com.sale_order.app.classes;

import com.google.gson.annotations.SerializedName;

public class SubscriberObj {
    @SerializedName("UserName")
    private String name;
    @SerializedName("IsWin")
    private boolean winner;

    public String getName() {
        return name;
    }

    public boolean isWinner() {
        return winner;
    }
}
