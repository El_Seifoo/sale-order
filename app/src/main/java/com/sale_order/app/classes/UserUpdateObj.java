package com.sale_order.app.classes;

import com.google.gson.annotations.SerializedName;

public class UserUpdateObj extends UserInfoObj {
    @SerializedName("IsUpdateUserName")
    private String updateUsername;
    @SerializedName("IsUpdateEmail")
    private String updateEmail;
    @SerializedName("IsUpdatePhone")
    private String updatePhone;

    @Override
    public String toString() {
        return "UserUpdateObj{" +
                "updateUsername=" + updateUsername +
                ", updateEmail=" + updateEmail +
                ", updatePhone=" + updatePhone +
                ", UserInfo=" + super.toString() +
                '}';
    }

    public UserUpdateObj(String userName, String email, String phoneNumber, String updateUsername, String updateEmail, String updatePhone) {
        super(userName, email, phoneNumber);
        this.updateUsername = updateUsername;
        this.updateEmail = updateEmail;
        this.updatePhone = updatePhone;
    }

}
