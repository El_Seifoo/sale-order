package com.sale_order.app.classes;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.google.gson.annotations.SerializedName;
import com.sale_order.app.R;

import java.util.List;

public class ShoppingObj {
    @SerializedName("Id")
    private String id;
    //    @SerializedName("Name")
//    private String name;
    @SerializedName(value = "Description", alternate = "ProductDescription")
    private String description;
    @SerializedName("ProductName")
    private String productName;
    @SerializedName("Price")
    private double price;
    @SerializedName("Image")
    private String image;
//    @SerializedName("Album")
//    private List<String> album;
    @SerializedName("Images")
    private List<ImageObj> images;
    //    @SerializedName("CurrentUser")
//    private UserInfoObj currentUser;
    @SerializedName("Qty")
    private int quantity;


    @BindingAdapter("image")
    public static void loadImage(ImageView view, String image) {
        Glide.with(view.getContext())
                .load(image)
                .error(R.mipmap.logo)
                .into(view);
    }

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public String getProductName() {
        return productName;
    }

    public double getPrice() {
        return price;
    }

    public String getImage() {
        return image;
    }

    public List<ImageObj> getImages() {
        return images;
    }

    public static class ImageObj {
        @SerializedName("FileId")
        private String id;
        @SerializedName("Image")
        private String imageUrl;

        public ImageObj(String id, String imageUrl) {
            this.id = id;
            this.imageUrl = imageUrl;
        }

        public String getId() {
            return id;
        }

        public String getImageUrl() {
            return imageUrl;
        }
    }

    public int getQuantity() {
        return quantity;
    }
}
