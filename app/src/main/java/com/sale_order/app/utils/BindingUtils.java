package com.sale_order.app.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.widget.TextView;


import androidx.databinding.BindingAdapter;

import com.sale_order.app.R;
import com.sale_order.app.classes.TimerObj;
import com.sale_order.app.classes.UserInfoData;
import com.sale_order.app.classes.UserInfoObj;

public class BindingUtils {

    public static String totalPoints(String requiredPoints, String freePoints) {
        int required = 0;
        int free = 0;
        if (!requiredPoints.isEmpty())
            required = Integer.valueOf(requiredPoints);
        if (!freePoints.isEmpty()) Integer.valueOf(freePoints);

        return String.valueOf(required + free);
    }

    public static String points(String points) {
        if (!points.isEmpty())
            return points;
        return "0";
    }

    public static int pickVisibility(boolean isWinner, boolean isPaid) {
        return isWinner && !isPaid ? View.VISIBLE : View.GONE;
    }

    public static int pickVisibility(UserInfoObj currentUser) {
        return currentUser != null ? View.VISIBLE : View.GONE;
    }

    public static int picColor(boolean winner, int yellow, int blue) {
        return winner ? yellow : blue;
    }

    public static int picColor(String data, int yellow, int red) {
        return data == null ? red : data.isEmpty() ? red : yellow;
    }

    public static String appendSymbol(String symbol, int number) {
        return symbol + number;
    }

    public static String pickWinner(String defaultMessage, String winnerName) {
        return winnerName == null ? defaultMessage : winnerName.isEmpty() ? defaultMessage : winnerName;
    }

    public static String getCountryNameCode(String name, String code, int flag) {
        if (flag == 0)
            return name.concat("(+").concat(code).concat(")");
        return name;
    }

    public static String appendTextToIntWithNewLine(String customString, String string, int number) {
        return number == 0 ? customString : number + "\n" + string;
    }

    public static String appendTextToIntWithNewLine(String string, double number) {
        String pointString = String.valueOf(number);
        return (number % 1 == 0 ? pointString.substring(0, pointString.indexOf(".")) : number) + "\n" + string;
    }

    public static String handleUserPoints(UserInfoObj currentUser) {
        if (currentUser == null) return "00";
        double number = currentUser.getPoints();
        String pointString = String.valueOf(number);
        return (number % 1 == 0 ? pointString.substring(0, pointString.indexOf(".")) : number) + "";
    }

    public static String appendCurrency(String currency, double price) {
        String priceString = String.valueOf(price);
        return (price % 1 == 0 ? priceString.substring(0, priceString.indexOf(".")) : price) + " " + currency;
    }

    public static String setTimer(String string, String strings, boolean listItem, TimerObj timerObj) {
        if (timerObj == null)
            return "00:00:00";
        if (timerObj.getDays() <= 0 && timerObj.getHours() <= 0 && timerObj.getMinutes() <= 0 && timerObj.getSeconds() <= 0)
            return "00:00:00";
        String days = timerObj.getDays() <= 0 ? "" : (timerObj.getDays() < 10 ? "0" + timerObj.getDays() + string : timerObj.getDays() + strings) + "  ";


        if ((listItem && days.trim().isEmpty()) || !listItem && days.trim().isEmpty()) {
            days = days.trim();
        } else if (!listItem && !days.trim().isEmpty()) {
            days = days.trim().concat("\n");
        }

        return days + (timerObj.getHours() < 10 ? "0" + timerObj.getHours() : timerObj.getHours()) + " : "
                + (timerObj.getMinutes() < 10 ? "0" + timerObj.getMinutes() : timerObj.getMinutes()) + " : "
                + (timerObj.getSeconds() < 10 ? "0" + timerObj.getSeconds() : timerObj.getSeconds());
    }

    public static boolean setTimerCardClickable(TimerObj timerObj) {
        if (timerObj.getDays() <= 0 && timerObj.getHours() <= 0 && timerObj.getMinutes() <= 0 && timerObj.getSeconds() <= 0)
            return false;
        return true;
    }


    @BindingAdapter({"android:text", "android:price"})
    public static void setPriceWithTitleAndCurrency(TextView view, String string, double price) {
        String priceString = String.valueOf(price);
        priceString = price % 1 == 0 ? priceString.substring(0, priceString.indexOf(".")) : price + "";
        if (MySingleton.getInstance(view.getContext()).getStringFromSharedPref(Constants.APP_LANGUAGE, view.getContext().getString(R.string.english_key)).equals(view.getContext().getString(R.string.english_key)))
            string = string + "\n" + priceString + " " + view.getContext().getString(R.string.currency);
        else
            string = string + "\n" + view.getContext().getString(R.string.currency) + " " + priceString;
        view.setText(string);
    }

    @BindingAdapter("android:product_name")
    public static void setProductNameWithTitle(TextView view, String name) {
        String text = view.getContext().getString(R.string.product) + "\n";
        if (name == null) view.setText(text);
        else
            view.setText(text.concat(name));
    }

    @BindingAdapter({"android:text", "android:points", "android:price"})
    public static void setText(TextView view, String string, double minimumPointsForBid, double currentPrice) {
        if (currentPrice != 0) {
            String priceString = String.valueOf(currentPrice);
            priceString = minimumPointsForBid % 1 == 0 ? priceString.substring(0, priceString.indexOf(".")) : minimumPointsForBid + "";

            if (MySingleton.getInstance(view.getContext()).getStringFromSharedPref(Constants.APP_LANGUAGE, view.getContext().getString(R.string.english_key)).equals(view.getContext().getString(R.string.english_key)))
                string = string + "\n" + priceString + " " + view.getContext().getString(R.string.currency);
            else
                string = string + "\n" + view.getContext().getString(R.string.currency) + " " + priceString;
            view.setText(string);
            return;
        }
        String priceString = String.valueOf(minimumPointsForBid);
        priceString = minimumPointsForBid % 1 == 0 ? priceString.substring(0, priceString.indexOf(".")) : minimumPointsForBid + "";
        if (MySingleton.getInstance(view.getContext()).getStringFromSharedPref(Constants.APP_LANGUAGE, view.getContext().getString(R.string.english_key)).equals(view.getContext().getString(R.string.english_key)))
            string = string + "\n" + priceString + " " + view.getContext().getString(R.string.currency);
        else
            string = string + "\n" + view.getContext().getString(R.string.currency) + " " + priceString;
        view.setText(string);
    }

    // description of auction...
    public static String decideStringByLang(Context context, String english, String arabic) {
        return MySingleton.getInstance(context).
                getStringFromSharedPref(
                        Constants.APP_LANGUAGE,
                        context.getString(R.string.english_key)).equals(context.getString(R.string.english_key))
                ? english : arabic;
    }

    public static String convertFromIntToString(int number) {
        return String.valueOf(number);
    }


}
