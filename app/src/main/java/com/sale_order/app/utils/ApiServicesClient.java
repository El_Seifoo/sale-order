package com.sale_order.app.utils;

import com.sale_order.app.classes.AuctionResponse;
import com.sale_order.app.classes.AuctionsResponse;
import com.sale_order.app.classes.ContactObj;
import com.sale_order.app.classes.CountryResponse;
import com.sale_order.app.classes.CreditResponse;
import com.sale_order.app.classes.NotificationsResponse;
import com.sale_order.app.classes.OffersResponse;
import com.sale_order.app.classes.OrderResponse;
import com.sale_order.app.classes.PaymentResponse;
import com.sale_order.app.classes.ProductResponse;
import com.sale_order.app.classes.ShoppingResponse;
import com.sale_order.app.classes.SignupResponse;
import com.sale_order.app.classes.TermsConditionsResponse;
import com.sale_order.app.classes.UserInfoResponse;
import com.sale_order.app.classes.UserLoginObj;
import com.sale_order.app.classes.UserObj;
import com.sale_order.app.classes.UserUpdateObj;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

public interface ApiServicesClient {
    @GET(URLs.COUNTRIES)
    Call<CountryResponse> fetchCountries(@Query("Ln") String language);

    @POST(URLs.REGISTER)
    Call<SignupResponse> signUp(@Body UserObj userObj);

    @POST(URLs.ACTIVATE)
    Call<SignupResponse> activateUser(@Query("email") String email, @Query("code") String code);

    @POST(URLs.RESEND_CODE)
    Call<SignupResponse> resendCode(@Query("email") String email);

    @GET(URLs.SEND_FORGET_PASSWORD_CODE)
    Call<SignupResponse> sendForgetPasswordCode(@Query("email") String email);

    @POST(URLs.CHANGE_PASSWORD)
    Call<SignupResponse> changePassword(@Query("email") String email, @Query("newPassword") String newPassword, @Query("code") String code);

    @POST(URLs.LOGIN)
    Call<UserInfoResponse> logIn(@Body UserLoginObj userObj);

    @GET(URLs.TERMS_CONDITIONS)
    Call<TermsConditionsResponse> termsConditions(@Query("Ln") String language);

    @GET(URLs.OFFERS)
    Call<OffersResponse> offers(@Query("Ln") String language);

    @GET(URLs.ABOUT_US)
    Call<TermsConditionsResponse> aboutUs(@Query("Ln") String language);

    @GET(URLs.CREDITS)
    Call<CreditResponse> credits(@QueryMap Map<String, Object> map);

    @GET(URLs.PROFILE)
    Call<UserInfoResponse> profile(@Header("Authorization") String token);

    @POST(URLs.UPDATE_PROFILE)
    Call<UserInfoResponse> updateProfile(@Header("Authorization") String token, @Body UserUpdateObj userUpdateObj);

    @POST(URLs.UPDATE_PROFILE)
    Call<UserInfoResponse> updateProfile(@Header("Authorization") String token, @Body Map<String, Object> userUpdateObj);

    @GET(URLs.CURRENT_AUCTIONS)
    Call<AuctionsResponse> currentAuctions(@QueryMap Map<String, Object> map);

    @GET(URLs.PREVIOUS_AUCTIONS)
    Call<AuctionsResponse> prevAuctions(@QueryMap Map<String, Object> map);

    //skip={skip}&take={take}&Ln={Ln}
    @GET(URLs.SHOPPING)
    Call<ShoppingResponse> shoppingList(@QueryMap Map<String, Object> map);

    //?productId={productId}
    @GET(URLs.PRODUCT)
    Call<ProductResponse> certainProduct(@Query("productId") String id);

    //{productId}/{quantity}
    @POST(URLs.MAKE_ORDER + "{productId}/{quantity}")
    Call<SignupResponse> makeOrder(@Header("Authorization") String token, @Path("productId") String id, @Path("quantity") int quantity);

    //auctionId={auctionId}&isCredit={isCredit}
    @POST(URLs.PAY_AUCTION)
    Call<OrderResponse> payAuction(@Header("Authorization") String token, @Query("auctionId") String id, @Query("isCredit") boolean isCredit);

//    //points=150&isCredit=true
//    @POST(URLs.BUY_POINTS)
//    Call<OrderResponse> buyPoints(@Header("Authorization") String token, @Query("points") int points, @Query("isCredit") boolean isCredit);

    //points=150
    @POST(URLs.BUY_POINTS)
    Call<OrderResponse> buyPoints(@Header("Authorization") String token, @Query("points") int points);

    //?pointPackageId=5
    @POST(URLs.BUY_POINTS_BY_PACKAGE)
    Call<OrderResponse> buyPointsByPackage(@Header("Authorization") String token, @Query("PointPackageId") String packageId);

    //{productId}/{orderId}
    @GET(URLs.CHECK_PAYMENT + "{productId}/{orderId}")
    Call<PaymentResponse> checkPayment(@Header("Authorization") String token, @Path("productId") String id, @Path("orderId") String orderId);

    //{{orderId}
    @GET(URLs.CHECK_AUCTION_PAYMENT + "{orderId}")
    Call<PaymentResponse> checkAuctionPayment(@Header("Authorization") String token, @Path("orderId") String id);

    //{orderId}
    @GET(URLs.CHECK_POINTS_PAYMENT + "{orderId}")
    Call<PaymentResponse> checkPayment(@Header("Authorization") String token, @Path("orderId") String orderId);

    @GET(URLs.NOTIFICATIONS)
    Call<NotificationsResponse> notifications(@Header("Authorization") String token, @QueryMap Map<String, Object> map);

    @GET(URLs.CERTAIN_AUCTION)
    Call<AuctionResponse> certainAuction(@Header("Authorization") String token, @Query("auctionId") String auctionId);

    @POST(URLs.CONTACT_US)
    Call<SignupResponse> contactUs(@Body ContactObj contactObj);

    @POST(URLs.BID)
    Call<SignupResponse> bid(@Header("Authorization") String token, @Body Map<String, String> map);


    @GET(URLs.START_AUCTION + "{auctionId}")
    Call<Void> startAuction(@Header("Authorization") String token, @Path("auctionId") String id);

    @GET(URLs.CLOSE_AUCTION + "{auctionId}")
    Call<Void> closeAuction(@Header("Authorization") String token, @Path("auctionId") String id);

//    //?paymentId={paymentId}&orderNumber={orderNumber}
//    @POST(URLs.DISCLOSE_POINTS)
//    Call<Void> disclosePoints(@Header("Authorization") String token, @Query("paymentId") String paymentId, @Query("orderNumber") String orderNumber);

    //?paymentId={paymentId}&orderNumber={orderNumber}
    @POST()
    Call<Void> discloseAuction(@Url String url, @Header("Authorization") String token, @Query("paymentId") String paymentId, @Query("orderNumber") String orderNumber);
}
