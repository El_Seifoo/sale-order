package com.sale_order.app.utils;

public class Constants {
    public static final String SHARED_PREF_NAME = "sale_order";
    public static final String USER_TOKEN = "token";
    public static final String IS_LOGGED_IN = "is_logged_in";
    public static final String APP_LANGUAGE = "app_language";
    public static final String USER_DATA = "user_data";
    public static final String NOTIFICATION = "notification";
    public static final String FIREBASE_CURRENT_AUCTION_TABLE_NAME = "CurrentAuction";
    public static final String FIREBASE_TOKEN = "firebase_token";
}
