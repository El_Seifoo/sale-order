package com.sale_order.app.utils;

public class URLs {
    public static final String ROOT = "http://api.sale-order.com/";

    public static final String REGISTER = "SaleOrder/api/Account/Register";
    public static final String ACTIVATE = "SaleOrder/api/Account/ActiveUser";
    public static final String RESEND_CODE = "SaleOrder/api/Account/ResendUserActiveCode";
    public static final String SEND_FORGET_PASSWORD_CODE = "SaleOrder/api/Account/ForgetPasswrod";
    public static final String CHANGE_PASSWORD = "SaleOrder/api/Account/ResetPassword";
    public static final String LOGIN = "SaleOrder/api/Login/GetAccessToken";
    public static final String PROFILE = "SaleOrder/api/Users/GetUserInfromation";
    public static final String UPDATE_PROFILE = "SaleOrder/api/Users/UpdateUserData";
    public static final String COUNTRIES = "SaleOrder/api/Service/GetCountries";
    public static final String ABOUT_US = "SaleOrder/api/Service/GetAboutUs";
    public static final String TERMS_CONDITIONS = "SaleOrder/api/Service/GetTerms";
    public static final String OFFERS = "SaleOrder/api/Offers/GetCurrentOffer";
    public static final String CREDITS = "SaleOrder/api/PointPackages/GetPointPackages";
    public static final String PREVIOUS_AUCTIONS = "SaleOrder/api/Auctions/GetPreviousAuctions";
    public static final String CURRENT_AUCTIONS = "SaleOrder/api/Auctions/GetStartTodayAuctions";
    public static final String CERTAIN_AUCTION = "SaleOrder/api/Auctions/GetAuctionById";
    public static final String BID = "SaleOrder/api/BiddersInAuction/PostBidderInAuction";
    public static final String SHOPPING = "SaleOrder/api/Products/GetProducts";
    public static final String PRODUCT = "SaleOrder/api/Products/GetProductById";
    public static final String MAKE_ORDER = "SaleOrder/api/ProductsSold/ProductBuyByUserPoints/";
    public static final String PAY_AUCTION = "SaleOrder/api/Auctions/PayAuctionPrice";
    public static final String BUY_POINTS = "SaleOrder/api/V2/UserPointPackages/CustomPointPackageBuyPoints";
    public static final String BUY_POINTS_BY_PACKAGE = "SaleOrder/api/V2/UserPointPackages/PointPackageBuy";
    public static final String CHECK_PAYMENT = "SaleOrder/api/v2/ProductsSold/CheckPay/";
    public static final String CHECK_AUCTION_PAYMENT = "SaleOrder/api/Auctions/CheckPay/";
    public static final String CHECK_POINTS_PAYMENT = "SaleOrder/api/V2/UserPointPackages/CheckPay/";
    public static final String NOTIFICATIONS = "SaleOrder/api/Notifications/GetNotifications";
    public static final String CONTACT_US = "SaleOrder/api/TechnicalSupport/PostTechnicalSupport";
    public static final String START_AUCTION = "SaleOrder/api/Secret/SrartAuction/";
    public static final String CLOSE_AUCTION = "SaleOrder/api/Secret/CloseAuction/";
    public static final String DISCLOSE_POINTS = "SaleOrder/api/V2/PaymentGateway/DiscloseBuyPoints";
    public static final String DISCLOSE_AUCTIONS = "SaleOrder/api/V2/PaymentGateway/DisclosePayAuctionPrice";
}
