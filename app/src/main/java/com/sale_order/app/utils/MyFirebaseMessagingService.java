package com.sale_order.app.utils;


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.sale_order.app.R;

import java.util.Map;
import java.util.Random;


public class MyFirebaseMessagingService extends FirebaseMessagingService {


    @Override
    public void onNewToken(String token) {
//        Log.e("FirebaseMService", "Refreshed token: " + token);
        MySingleton.getInstance(getApplicationContext()).saveStringToSharedPref(Constants.FIREBASE_TOKEN, token);
    }

    private final String CHANNEL_ID = "SaleOrder";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        if (remoteMessage.getData() != null) {
            Log.e("notificationData", remoteMessage.getData().toString());
        }

        if (MySingleton.getInstance(getApplication()).getBooleanFromSharedPref(Constants.NOTIFICATION, true)) {
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            Uri notificationSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                CharSequence notificationChannelName = "Notification";

                AudioAttributes audioAttributes = new AudioAttributes.Builder()
                        .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                        .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                        .build();

                NotificationChannel notificationChannel;
                notificationChannel = new NotificationChannel(CHANNEL_ID, notificationChannelName, NotificationManager.IMPORTANCE_HIGH);
                notificationChannel.enableVibration(false);
                notificationChannel.setVibrationPattern(new long[]{0, 0, 0, 0});
                notificationChannel.enableLights(false);
                notificationChannel.setSound(notificationSoundUri, audioAttributes);
                if (notificationManager != null) {
                    notificationManager.createNotificationChannel(notificationChannel);
                }
            }

            String title = "", message = "";
            Map<String, String> data = remoteMessage.getData();
            int index = Integer.parseInt(data.get("MessageType") == null ? "0" : data.get("MessageType"));
            String language = MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getString(R.string.english_key));
            String enKey = getString(R.string.english_key);
            String arKey = getString(R.string.arabic_key);
            switch (index) {
                case 1:
                case 2:
                case 3:
                    if (language.equals(enKey)) {
                        title = data.get("NameActionEn");
                        message = data.get("BodyEn");
                    } else {
                        title = data.get("NameActionAr");
                        message = data.get("BodyAr");
                    }
                    break;
                case 4:
                    title = getString(R.string.new_bid);
                    message = getString(R.string.new_bidder_called) + " " + data.get("UserName") + " " +
                            getString(R.string.bid_with) + " " + data.get("Points");
                    break;
                case 5:
                case 6:
                case 7:
                    if (language.equals(enKey)) {
                        title = data.get("NameActionEn");
                        message = data.get("DescriptionEn");
                    } else {
                        title = data.get("NameActionAr");
                        message = data.get("DescriptionAr");
                    }
                    break;
            }

            if (index != 0) {
                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                        .setSmallIcon(R.drawable.nav_notifications_icon)
                        .setContentTitle(remoteMessage.getData().get("title"))
                        .setContentText(remoteMessage.getData().get("body"))
                        .setAutoCancel(true)
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setSound(notificationSoundUri);


                int notificationId = new Random().nextInt(60000);
                notificationManager.notify(notificationId, notificationBuilder.build());
            }

        }

    }
}
