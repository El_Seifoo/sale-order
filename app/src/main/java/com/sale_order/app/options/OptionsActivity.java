package com.sale_order.app.options;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sale_order.app.R;
import com.sale_order.app.login.LoginActivity;
import com.sale_order.app.main.MainActivity;
import com.sale_order.app.signup.SignupActivity;
import com.sale_order.app.utils.Constants;
import com.sale_order.app.utils.Language;
import com.sale_order.app.utils.MySingleton;

import java.util.ArrayList;

public class OptionsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.arabic_key)));
        setContentView(R.layout.activity_options);

//        if (MySingleton.getInstance(this).isLoggedIn()) {
//            Intent intent = new Intent(this, MainActivity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            startActivity(intent);
//        }
    }


    public void onClick(View view) {
        if (view.getId() == R.id.signup_button) {
            startActivity(new Intent(this, SignupActivity.class));
        } else if (view.getId() == R.id.login_button) {
            startActivity(new Intent(this, LoginActivity.class));
        } else if (view.getId() == R.id.skip_text_view) {
            startActivity(new Intent(this, MainActivity.class));
        }
    }


}
