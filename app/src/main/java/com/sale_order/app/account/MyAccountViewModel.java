package com.sale_order.app.account;

import android.app.Application;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.sale_order.app.R;
import com.sale_order.app.classes.UserInfoData;
import com.sale_order.app.classes.UserInfoObj;
import com.sale_order.app.classes.UserUpdateObj;
import com.sale_order.app.utils.MySingleton;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.app.Activity.RESULT_OK;

public class MyAccountViewModel extends AndroidViewModel implements MyAccountModel.ModelCallback {
    protected static final int LOGIN_FIRST_PROFILE_REQUEST_CODE = 1;
    protected static final int LOGIN_FIRST_EDIT_PROFILE_REQUEST_CODE = 2;
    private Application application;
    private MyAccountModel model;
    private ViewListener viewListener;
    private ObservableField<Integer> progress;
    private ObservableField<String> errorMessage;
    private ObservableField<Integer> errorView;
    private ObservableField<Boolean> editButtonClickable;
    private ObservableField<UserInfoObj> userObjObservableField;

    public MyAccountViewModel(@NonNull Application application) {
        super(application);
        this.application = application;
        model = new MyAccountModel(application);
        progress = new ObservableField<>(View.GONE);
        errorView = new ObservableField<>(View.GONE);
        editButtonClickable = new ObservableField<>(true);
        errorMessage = new ObservableField<>("");
        userObjObservableField = new ObservableField<>(new UserInfoObj("", "", ""));
    }

    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    protected MutableLiveData<UserInfoData> requestUserInfo() {
        setProgress(View.VISIBLE);
        setEditButtonClickable(false);
        return model.fetchUserInfo(this);
    }

    public void onRetryClickListener(View view) {
        setProgress(View.VISIBLE);
        setErrorView(View.GONE);
        setEditButtonClickable(false);
        model.fetchUserInfo(this);
    }

    protected void requestUpdateUser(EditText userNameEditText, EditText emailEditText, EditText phoneNumberEditText,
                                     String originUserName, String originEmail, String originPhone) {

        String username = userNameEditText.getText().toString().trim();
        if (username.isEmpty()) {
            viewListener.showEditTextError(userNameEditText, application.getString(R.string.this_field_can_not_be_blank));
            return;
        }

        String email = emailEditText.getText().toString().trim();
        if (email.isEmpty()) {
            viewListener.showEditTextError(emailEditText, application.getString(R.string.this_field_can_not_be_blank));
            return;
        }
        if (!checkMailValidation(email)) {
            viewListener.showEditTextError(emailEditText, application.getString(R.string.mail_not_valid));
            return;
        }

        String phone = phoneNumberEditText.getText().toString().trim();
        if (phone.isEmpty()) {
            viewListener.showEditTextError(phoneNumberEditText, application.getString(R.string.this_field_can_not_be_blank));
            return;
        }
        if (phone.length() > 12) {
            viewListener.showEditTextError(phoneNumberEditText, application.getString(R.string.phone_code_length_error));
            return;
        }

        if (originUserName.equals(username) && originEmail.equals(email) && originPhone.equals(phone)) {
            viewListener.showToastMessage(application.getString(R.string.no_changes_to_be_updated));
            return;
        }

        boolean isUserNameChanged = false, isEmailChanged = false, isPhoneChanged = false;
        if (!originUserName.equals(username)) isUserNameChanged = true;
        if (!originEmail.equals(email)) isEmailChanged = true;
        if (!originPhone.equals(phone)) isPhoneChanged = true;

        setProgress(View.VISIBLE);
        setEditButtonClickable(false);
        model.updateUserInfo(new UserUpdateObj(username, email, phone, isUserNameChanged + "", isEmailChanged + "", isPhoneChanged + ""), this);
    }

    private boolean checkMailValidation(String email) {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";
        CharSequence inputStr = email;
        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if (matcher.matches())
            return true;
        else
            return false;
    }


    public ObservableField<UserInfoObj> getUserObjObservableField() {
        return userObjObservableField;
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    public ObservableField<String> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage.set(errorMessage);
    }

    public ObservableField<Integer> getErrorView() {
        return errorView;
    }

    public void setErrorView(int errorView) {
        this.errorView.set(errorView);
    }

    public ObservableField<Boolean> getEditButtonClickable() {
        return editButtonClickable;
    }

    @Override
    public void setUserObjObservableField(UserInfoObj user) {
        this.userObjObservableField.set(user);
    }

    @Override
    public void setEditButtonClickable(boolean editButtonClickable) {
        this.editButtonClickable.set(editButtonClickable);
    }

    @Override
    public void showMessage(String message) {
        if (message != null) {
            if (message.equals(application.getString(R.string.update_user_info_response_success))) {
                viewListener.showToastMessage(application.getString(R.string.user_updated_successfully));
                return;
            }
            viewListener.showToastMessage(message.equals(application.getString(R.string.update_user_info_response_email_already_exist))
                    ? application.getString(R.string.email_already_exist)
                    : message.equals(application.getString(R.string.update_user_info_response_phone_number_already_exist))
                    ? application.getString(R.string.phone_already_exist)
                    : message.equals(application.getString(R.string.update_user_info_response_username_already_exist))
                    ? application.getString(R.string.username_already_exist)
                    : application.getString(R.string.something_went_wrong));
        }
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }

    @Override
    public void loginFirst(int requestCode) {
        MySingleton.getInstance(getApplication()).logout();
        viewListener.LoginFirst(requestCode);
    }

    @Override
    public void handleOnFailure(Throwable t, int index) {
        if (index == 0) {
            // error fetching user info ...
            setErrorView(View.VISIBLE);
            Log.e("error", t.toString());
            if (t instanceof IOException)
                setErrorMessage(application.getString(R.string.no_internet_connection));
            else
                setErrorMessage(application.getString(R.string.error_fetching_data));
        } else {
            //update user error
            if (t instanceof IOException)
                setErrorMessage(application.getString(R.string.no_internet_connection));
            else
                setErrorMessage(application.getString(R.string.error_fetching_data));
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        /*
        if (requestCode == MyAccountActivity.PICK_PICTURE_REQUEST_CODE && resultCode == RESULT_OK && data != null) {
            viewListener.startCroppingAct(data.getData());
            return;
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                InputStream inputStream = getApplication().getContentResolver().openInputStream(result.getUri());
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                Bitmap bitmap = Bitmap.createScaledBitmap(BitmapFactory.decodeStream(inputStream), 100, 100, false);
                bitmap = Bitmap.createScaledBitmap(bitmap, 100, 100, false);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] imageBytes = baos.toByteArray();
                String encoded = Base64.encodeToString(imageBytes, Base64.NO_WRAP);

//                setProgress(View.VISIBLE);
//                setEditButtonClickable(false);
                Log.e("encoded", encoded);
                model.updateProfilePic(encoded, this);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                viewListener.showToastMessage(error.toString());
            }
            return;
        }
         */
        if (requestCode == LOGIN_FIRST_PROFILE_REQUEST_CODE && resultCode == RESULT_OK)
            this.requestUserInfo();
    }

    /*
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == MyAccountActivity.PICK_PICTURE_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                viewListener.startPicking(Intent.createChooser(intent, getApplication().getString(R.string.profile_picture)), MyAccountActivity.PICK_PICTURE_REQUEST_CODE);

            }
        }
    }*/


    protected interface ViewListener {
        void showToastMessage(String message);

        void showEditTextError(EditText editText, String errorMessage);

        void LoginFirst(int requestCode);
        /*
            void startPicking(Intent intent, int requestCode);

            void startCroppingAct(Uri uri);

            void setProfilePicture(Uri uri);
         */
    }
}
