package com.sale_order.app.account;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.sale_order.app.R;
import com.sale_order.app.classes.UserInfoData;
import com.sale_order.app.classes.UserInfoObj;
import com.sale_order.app.classes.UserInfoResponse;
import com.sale_order.app.databinding.ActivityMyAccountBinding;
import com.sale_order.app.login.LoginActivity;
import com.sale_order.app.utils.Constants;
import com.sale_order.app.utils.Language;
import com.sale_order.app.utils.MySingleton;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.FileNotFoundException;

public class MyAccountActivity extends AppCompatActivity implements MyAccountViewModel.ViewListener, Presenter {
    //    protected static final int PICK_PICTURE_REQUEST_CODE = 3;
    private MyAccountViewModel viewModel;
    private ActivityMyAccountBinding dataBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.arabic_key)));

        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_my_account);
        viewModel = ViewModelProviders.of(this).get(MyAccountViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);

        getSupportActionBar().setTitle(getString(R.string.my_account));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewModel.requestUserInfo().observe(this, new Observer<UserInfoData>() {
            @Override
            public void onChanged(UserInfoData userInfo) {
                if (userInfo != null)
                    dataBinding.setUser(userInfo.getUserInfo());
            }
        });

        handleEditTextsChanges();
    }

    private void handleEditTextsChanges() {
        dataBinding.accountUsernameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                viewModel.setUserObjObservableField(new UserInfoObj(s.toString(), dataBinding.accountEmailEditText.getText().toString(), dataBinding.accountPhoneNumberEditText.getText().toString()));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        dataBinding.accountEmailEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                viewModel.setUserObjObservableField(new UserInfoObj(dataBinding.accountUsernameEditText.getText().toString(), s.toString(), dataBinding.accountPhoneNumberEditText.getText().toString()));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        dataBinding.accountPhoneNumberEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                viewModel.setUserObjObservableField(new UserInfoObj(dataBinding.accountUsernameEditText.getText().toString(), dataBinding.accountEmailEditText.getText().toString(), s.toString()));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    @Override
    public void onEditAccountClicked() {

        viewModel.requestUpdateUser(dataBinding.accountUsernameEditText, dataBinding.accountEmailEditText, dataBinding.accountPhoneNumberEditText,
                dataBinding.getUser().getUserName(), dataBinding.getUser().getEmail(), dataBinding.getUser().getPhoneNumber());
    }

//    @Override
//    public void onChangePicClicked() {
//        ActivityCompat.requestPermissions(this,
//                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
//                PICK_PICTURE_REQUEST_CODE
//        );
//    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showEditTextError(EditText editText, String errorMessage) {
        editText.setError(errorMessage);
    }

    @Override
    public void LoginFirst(int requestCode) {
        Intent loginIntent = new Intent(this, LoginActivity.class);
        loginIntent.putExtra("MyAccountActivity", "");
        startActivityForResult(loginIntent, requestCode);
    }

    /*
        @Override
        public void startPicking(Intent intent, int requestCode) {
            startActivityForResult(intent, requestCode);
        }

        @Override
        public void startCroppingAct(Uri uri) {
            CropImage.activity(uri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setCropShape(CropImageView.CropShape.OVAL)
                    .setAspectRatio(1, 1)
                    .start(this);
        }

        @Override
        public void setProfilePicture(Uri uri) {
            Glide.with(this)
                    .load(uri)
                    .error(R.mipmap.logo)
                    .into(dataBinding.profilePic);
        }
    */


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        viewModel.onRequestPermissionsResult(requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        viewModel.onActivityResult(requestCode, resultCode, data);
    }
}
