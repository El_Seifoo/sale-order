package com.sale_order.app.account;

import android.content.Context;
import android.util.Log;
import android.view.View;

import androidx.lifecycle.MutableLiveData;

import com.sale_order.app.classes.UserInfoData;
import com.sale_order.app.classes.UserInfoObj;
import com.sale_order.app.classes.UserInfoResponse;
import com.sale_order.app.classes.UserObj;
import com.sale_order.app.classes.UserUpdateObj;
import com.sale_order.app.utils.Constants;
import com.sale_order.app.utils.MySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyAccountModel {
    private Context context;

    public MyAccountModel(Context context) {
        this.context = context;
    }

    private MutableLiveData<UserInfoData> userMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<UserInfoData> fetchUserInfo(final ModelCallback callback) {
        if (userMutableLiveData.getValue() == null) {
            Call<UserInfoResponse> call = MySingleton.getInstance(context).createService()
                    .profile(MySingleton.getInstance(context).getStringFromSharedPref(Constants.USER_TOKEN, ""));
            call.enqueue(new Callback<UserInfoResponse>() {
                @Override
                public void onResponse(Call<UserInfoResponse> call, Response<UserInfoResponse> response) {
                    Log.e("response code", response.code() + "))))");//401 not authorized
                    callback.setProgress(View.GONE);
                    callback.setEditButtonClickable(true);
                    if (response.code() == 401) {
                        callback.loginFirst(MyAccountViewModel.LOGIN_FIRST_PROFILE_REQUEST_CODE);
                        return;
                    }
                    if (response.code() == 200) {
                        if (response.body() != null) {
                            UserInfoResponse infoResponse = response.body();
                            if (userMutableLiveData.getValue() == null) {
                                callback.setUserObjObservableField(infoResponse.getUser().getUserInfo());
                            }
                            callback.showMessage(infoResponse.getMessage());
                            MySingleton.getInstance(context).saveUserData(infoResponse.getUser().getUserInfo());
                            userMutableLiveData.setValue(infoResponse.getUser());
                        }
                        return;
                    }

                    callback.showMessage("");

                }

                @Override
                public void onFailure(Call<UserInfoResponse> call, Throwable t) {
                    callback.setProgress(View.GONE);
                    callback.setEditButtonClickable(true);
                    callback.handleOnFailure(t, 0);
                }
            });
        } else {
            callback.setProgress(View.GONE);
            callback.setEditButtonClickable(true);
        }
        return userMutableLiveData;
    }

    protected void updateUserInfo(final UserUpdateObj updatedUserInfo, final ModelCallback callback) {
        Call<UserInfoResponse> call = MySingleton.getInstance(context).createService()
                .updateProfile(MySingleton.getInstance(context).getStringFromSharedPref(Constants.USER_TOKEN, ""), updatedUserInfo);
        call.enqueue(new Callback<UserInfoResponse>() {
            @Override
            public void onResponse(Call<UserInfoResponse> call, Response<UserInfoResponse> response) {
                Log.e("response code", response.code() + "))))");//401 not authorized
                callback.setProgress(View.GONE);
                callback.setEditButtonClickable(true);
                if (response.code() == 401) {
                    callback.loginFirst(MyAccountViewModel.LOGIN_FIRST_EDIT_PROFILE_REQUEST_CODE);
                    return;
                }
                if (response.code() == 200) {
                    if (response.body() != null) {
                        UserInfoResponse infoResponse = response.body();
                        callback.showMessage(infoResponse.getMessage());
                        MySingleton.getInstance(context).saveUserData(infoResponse.getUser().getUserInfo());
                        userMutableLiveData.setValue(infoResponse.getUser());
                        callback.setUserObjObservableField(infoResponse.getUser().getUserInfo());
                    }

                    return;
                }

                callback.showMessage("");
            }

            @Override
            public void onFailure(Call<UserInfoResponse> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.setEditButtonClickable(true);
                callback.handleOnFailure(t, 1);
            }
        });
    }

    /*
        protected void updateProfilePic(String profilePic, final ModelCallback callback) {
            Map<String, Object> map = new HashMap<>();
            map.put("Base64StringFile", profilePic);
            map.put("IsUpdateImageProfile", true);
            Call<UserInfoResponse> call = MySingleton.getInstance(context).createService()
                    .updateProfile(MySingleton.getInstance(context).getStringFromSharedPref(Constants.USER_TOKEN, ""), map);
            call.enqueue(new Callback<UserInfoResponse>() {
                @Override
                public void onResponse(Call<UserInfoResponse> call, Response<UserInfoResponse> response) {
                    callback.setProgress(View.GONE);
                    callback.setEditButtonClickable(true);
                    if (response.code() == 401) {
                        callback.loginFirst(MyAccountViewModel.LOGIN_FIRST_EDIT_PROFILE_REQUEST_CODE);
                        return;
                    }
                    if (response.code() == 200) {
                        callback.showMessage(response.body().getMessage());
                        if (response.body().getUser() != null) {
                            UserInfoResponse infoResponse = response.body();
                            MySingleton.getInstance(context).saveUserData(infoResponse.getUser().getUserInfo());
                            userMutableLiveData.setValue(infoResponse.getUser());
                            callback.setUserObjObservableField(infoResponse.getUser().getUserInfo());
                        }
                        return;
                    }

                    callback.showMessage("");
                }

                @Override
                public void onFailure(Call<UserInfoResponse> call, Throwable t) {
                    callback.setProgress(View.GONE);
                    callback.setEditButtonClickable(true);
                    callback.handleOnFailure(t, 1);
                }
            });
        }

     */

    protected interface ModelCallback {

        void handleOnFailure(Throwable t, int index);

        void setUserObjObservableField(UserInfoObj user);

        void setEditButtonClickable(boolean clickable);

        void showMessage(String message);

        void setProgress(int progress);

        void loginFirst(int requestCode);
    }
}
