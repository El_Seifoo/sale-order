package com.sale_order.app.changePassword;

import android.content.Context;
import android.util.Log;
import android.view.View;

import androidx.lifecycle.MutableLiveData;

import com.sale_order.app.classes.SignupResponse;
import com.sale_order.app.utils.MySingleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordModel {
    private Context context;

    public ChangePasswordModel(Context context) {
        this.context = context;
    }

//    protected void sendCode(String email, final ModelCallback callback) {
//        Call<SignupResponse> call = MySingleton.getInstance(context).createService().sendForgetPasswordCode(email);
//        call.enqueue(new Callback<SignupResponse>() {
//            @Override
//            public void onResponse(Call<SignupResponse> call, Response<SignupResponse> response) {
//                callback.setProgress(View.GONE);
//                if (response.body() != null) {
//                    callback.showMessage(response.body().getMessage());
//                }
//            }
//
//            @Override
//            public void onFailure(Call<SignupResponse> call, Throwable t) {
//                callback.setProgress(View.GONE);
//                callback.onFailureHandler(t);
//            }
//        });
//
//
//    }


    protected void changePassword(String email, String code, String newPassword, final ModelCallback callback) {
        Call<SignupResponse> call = MySingleton.getInstance(context).createService().changePassword(email, newPassword, code);
        call.enqueue(new Callback<SignupResponse>() {
            @Override
            public void onResponse(Call<SignupResponse> call, Response<SignupResponse> response) {
                callback.setProgress(View.GONE);
                if (response.body() != null) {
                    callback.showMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<SignupResponse> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.onFailureHandler(t);
            }
        });
    }

    protected interface ModelCallback {
        void setProgress(int progress);

        void onFailureHandler(Throwable t);

        void showMessage(String message);
    }

}
