package com.sale_order.app.changePassword;

import android.app.Application;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.sale_order.app.R;

import java.io.IOException;

public class ChangePasswordViewModel extends AndroidViewModel implements ChangePasswordModel.ModelCallback {
    private Application application;
    private ChangePasswordModel model;
    private ViewListener viewListener;
    private ObservableField<Integer> progress;

    public ChangePasswordViewModel(@NonNull Application application) {
        super(application);
        this.application = application;
        this.model = new ChangePasswordModel(application);
        progress = new ObservableField<>(View.GONE);
    }

    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }


//    protected void requestSendCode(String email) {
//        setProgress(View.VISIBLE);
//        model.sendCode(email, this);
//    }


    protected void requestChangePassword(EditText passwordEditText, EditText confirmPasswordEditText, String email, String code) {
        String password = passwordEditText.getText().toString().trim();
        if (password.isEmpty()) {
            viewListener.showEditTextError(passwordEditText, application.getString(R.string.this_field_can_not_be_blank));
            return;
        }

        if (password.length() < 6) {
            viewListener.showEditTextError(passwordEditText, application.getString(R.string.password_length_error));
            return;
        }
        String confirmPassword = confirmPasswordEditText.getText().toString().trim();
        if (confirmPassword.isEmpty()) {
            viewListener.showEditTextError(confirmPasswordEditText, application.getString(R.string.this_field_can_not_be_blank));
            return;
        }
        if (!password.equals(confirmPassword)) {
            viewListener.showEditTextError(confirmPasswordEditText, application.getString(R.string.password_confirm_not_matching));
            return;
        }

        setProgress(View.VISIBLE);
        model.changePassword(email, code, password, this);
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }

    @Override
    public void onFailureHandler(Throwable t) {
        viewListener.showToastMessage(t instanceof IOException ? application.getString(R.string.no_internet_connection) :
                application.getString(R.string.something_went_wrong));
    }

    @Override
    public void showMessage(String message) {
        if (message == null) {
            //password changed
            viewListener.passwordChanged();
            return;
        }

        if (message.equals(application.getString(R.string.change_password_response_invalid_code))) {
            viewListener.showToastMessage(application.getString(R.string.invalid_code));
            return;
        }

//        if (message.equals(application.getString(R.string.send_forget_password_code_response_success))) {
//            // code is sent...
//            viewListener.showToastMessage(application.getString(R.string.code_send_check_your_inbox));
//            return;
//        }
//        if (message.equals(application.getString(R.string.send_forget_password_code_response_user_not_exist))) {
//            viewListener.showToastMessage(application.getString(R.string.user_not_exist));
//            viewListener.finishActivity();
//        }
    }


    protected interface ViewListener {
        void showToastMessage(String message);

        void showEditTextError(EditText editText, String errorMessage);

        void passwordChanged();
    }
}
