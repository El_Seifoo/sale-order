package com.sale_order.app.changePassword;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.sale_order.app.R;
import com.sale_order.app.databinding.ActivityChangePasswordBinding;
import com.sale_order.app.options.OptionsActivity;
import com.sale_order.app.utils.Constants;
import com.sale_order.app.utils.Language;
import com.sale_order.app.utils.MySingleton;

public class ChangePasswordActivity extends AppCompatActivity implements ChangePasswordViewModel.ViewListener, Presenter {
    private ActivityChangePasswordBinding dataBinding;
    private ChangePasswordViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.arabic_key)));
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_change_password);
        viewModel = ViewModelProviders.of(this).get(ChangePasswordViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//        viewModel.requestSendCode(getIntent().getExtras().getString("email"));
    }


    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showEditTextError(EditText editText, String errorMessage) {
        editText.setError(errorMessage);
    }

    @Override
    public void passwordChanged() {
        showToastMessage(getString(R.string.password_changed_successfully));
        Intent intent = new Intent(this, OptionsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onChangeButtonClicked() {
        viewModel.requestChangePassword(dataBinding.changePasswordNewPasswordEditText,
                dataBinding.changePasswordConfirmPasswordEditText,
                getIntent().getExtras().getString("email"),
                dataBinding.codeEditText.getText().toString());
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
