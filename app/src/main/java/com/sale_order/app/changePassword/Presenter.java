package com.sale_order.app.changePassword;

public interface Presenter {
    void onChangeButtonClicked();
}
