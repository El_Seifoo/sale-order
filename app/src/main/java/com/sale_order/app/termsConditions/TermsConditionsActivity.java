package com.sale_order.app.termsConditions;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.sale_order.app.R;
import com.sale_order.app.databinding.ActivityTermsConditionsBinding;
import com.sale_order.app.utils.Constants;
import com.sale_order.app.utils.Language;
import com.sale_order.app.utils.MySingleton;

public class TermsConditionsActivity extends AppCompatActivity implements TermsConditionsViewModel.ViewListener {
    private TermsConditionsViewModel viewModel;
    private ActivityTermsConditionsBinding dataBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.arabic_key)));
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_terms_conditions);
        viewModel = ViewModelProviders.of(this).get(TermsConditionsViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);

        getSupportActionBar().setTitle(getString(R.string.terms_conditions));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewModel.requestTermsConditions(MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE, getString(R.string.english_key))).observe(this, new Observer<String>() {
            @Override
            public void onChanged(String data) {
                dataBinding.termsConditionsTextView.setText(data);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
