package com.sale_order.app.withdraw;

import android.app.Application;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.sale_order.app.R;
import com.sale_order.app.classes.OfferObj;

import java.io.IOException;

public class WithdrawViewModel extends AndroidViewModel implements WithdrawModel.ModelCallback {
    private WithdrawModel model;
    private ViewListener viewListener;
    private ObservableField<Integer> emptyListTextView;
    private ObservableField<Integer> progress;
    private ObservableField<String> errorMessage;
    private ObservableField<Integer> errorView;

    private ObservableField<Integer> firstDiceNumber, secondDiceNumber, thirdDiceNumber;
    private ObservableField<Drawable> firstDice, secondDice, thirdDice;
    private ObservableField<Float> firstDiceX, firstDiceY, secondDiceX, secondDiceY, thirdDiceX, thirdDiceY;


    public WithdrawViewModel(@NonNull Application application) {
        super(application);
        model = new WithdrawModel(application);
        progress = new ObservableField<>(View.GONE);
        emptyListTextView = new ObservableField<>(View.GONE);
        errorView = new ObservableField<>(View.GONE);
        errorMessage = new ObservableField<>("");

        firstDiceNumber = new ObservableField<>(1);
        secondDiceNumber = new ObservableField<>(1);
        thirdDiceNumber = new ObservableField<>(1);

        firstDice = new ObservableField<>(application.getResources().getDrawable(R.drawable.ic_dice_one));
        secondDice = new ObservableField<>(application.getResources().getDrawable(R.drawable.ic_dice_one));
        thirdDice = new ObservableField<>(application.getResources().getDrawable(R.drawable.ic_dice_one));

        firstDiceX = new ObservableField<>(0.5f);
        firstDiceY = new ObservableField<>(0.5f);
        secondDiceX = new ObservableField<>(0.5f);
        secondDiceY = new ObservableField<>(0.5f);
        thirdDiceX = new ObservableField<>(0.5f);
        thirdDiceY = new ObservableField<>(0.5f);

    }

    protected MutableLiveData<OfferObj> requestOffers() {
        setProgress(View.VISIBLE);
        return model.fetchOffers(this);
    }

    public void onRetryClickListener(View view) {
        setProgress(View.VISIBLE);
        setErrorView(View.GONE);
        model.fetchOffers(this);
    }


    @Override
    public void onFailureHandler(Throwable t) {
        // error fetching Terms & conditions ...
        setErrorView(View.VISIBLE);
        Log.e("error", t.toString());
        if (t instanceof IOException)
            setErrorMessage(getApplication().getString(R.string.no_internet_connection));
        else
            setErrorMessage(getApplication().getString(R.string.error_fetching_data));

    }


    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public ObservableField<Integer> getEmptyListTextView() {
        return emptyListTextView;
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    public ObservableField<String> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String message) {
        errorMessage.set(message);
    }

    public ObservableField<Integer> getErrorView() {
        return errorView;
    }

    public void setErrorView(int errorViewStatus) {
        this.errorView.set(errorViewStatus);
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }

    @Override
    public void setEmptyListTextView(int empty) {
        this.emptyListTextView.set(empty);
    }


    public void setFirstDiceNumber(int firstDiceNumber) {
        firstDice.set(pickDiceSide(firstDiceNumber));
        this.firstDiceNumber.set(firstDiceNumber);
    }

    public void setSecondDiceNumber(int secondDiceNumber) {
        secondDice.set(pickDiceSide(secondDiceNumber));
        this.secondDiceNumber.set(secondDiceNumber);
    }

    public void setThirdDiceNumber(int thirdDiceNumber) {
        thirdDice.set(pickDiceSide(thirdDiceNumber));
        this.thirdDiceNumber.set(thirdDiceNumber);
    }

    public ObservableField<Integer> getFirstDiceNumber() {
        return firstDiceNumber;
    }

    public ObservableField<Integer> getSecondDiceNumber() {
        return secondDiceNumber;
    }

    public ObservableField<Integer> getThirdDiceNumber() {
        return thirdDiceNumber;
    }

    public ObservableField<Drawable> getFirstDice() {
        return firstDice;
    }

    public ObservableField<Drawable> getSecondDice() {
        return secondDice;
    }

    public ObservableField<Drawable> getThirdDice() {
        return thirdDice;
    }

    public void setFirstDiceX(float firstDiceX) {
        this.firstDiceX.set(firstDiceX);
    }

    public void setFirstDiceY(float firstDiceY) {
        this.firstDiceY.set(firstDiceY);
    }

    public void setSecondDiceX(float secondDiceX) {
        this.secondDiceX.set(secondDiceX);
    }

    public void setSecondDiceY(float secondDiceY) {
        this.secondDiceY.set(secondDiceY);
    }

    public void setThirdDiceX(float thirdDiceX) {
        this.thirdDiceX.set(thirdDiceX);
    }

    public void setThirdDiceY(float thirdDiceY) {
        this.thirdDiceY.set(thirdDiceY);
    }

    public ObservableField<Float> getFirstDiceX() {
        return firstDiceX;
    }

    public ObservableField<Float> getFirstDiceY() {
        return firstDiceY;
    }

    public ObservableField<Float> getSecondDiceX() {
        return secondDiceX;
    }

    public ObservableField<Float> getSecondDiceY() {
        return secondDiceY;
    }

    public ObservableField<Float> getThirdDiceX() {
        return thirdDiceX;
    }

    public ObservableField<Float> getThirdDiceY() {
        return thirdDiceY;
    }

    private Drawable pickDiceSide(int number) {
        switch (number) {
            case 1:
                return getApplication().getResources().getDrawable(R.drawable.ic_dice_one);
            case 2:
                return getApplication().getResources().getDrawable(R.drawable.ic_dice_two);
            case 3:
                return getApplication().getResources().getDrawable(R.drawable.ic_dice_three);
            case 4:
                return getApplication().getResources().getDrawable(R.drawable.ic_dice_four);
            case 5:
                return getApplication().getResources().getDrawable(R.drawable.ic_dice_five);
            default:
                return getApplication().getResources().getDrawable(R.drawable.ic_dice_six);
        }
    }

    protected interface ViewListener {

    }
}
