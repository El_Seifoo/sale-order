package com.sale_order.app.withdraw;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;

import com.sale_order.app.R;
import com.sale_order.app.classes.OfferObj;
import com.sale_order.app.databinding.ActivityWithdrawBinding;
import com.sale_order.app.utils.Constants;
import com.sale_order.app.utils.Language;
import com.sale_order.app.utils.MySingleton;

import java.util.Random;

public class WithdrawActivity extends AppCompatActivity {
    private WithdrawViewModel viewModel;
    private ActivityWithdrawBinding dataBinding;
    private OffersAdapter adapter;
    private RecyclerView subscribersList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.arabic_key)));
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_withdraw);
        viewModel = ViewModelProviders.of(this).get(WithdrawViewModel.class);
        dataBinding.setViewModel(viewModel);


        getSupportActionBar().setTitle(getString(R.string.withdraw));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        viewModel.setFirstDiceNumber(generateRandomNumber());
        viewModel.setSecondDiceNumber(generateRandomNumber());
        viewModel.setThirdDiceNumber(generateRandomNumber());

        viewModel.setFirstDiceX(generateFloatRandomNumber());
        viewModel.setFirstDiceY(generateFloatRandomNumber());
        viewModel.setSecondDiceX(generateFloatRandomNumber());
        viewModel.setSecondDiceY(generateFloatRandomNumber());
        viewModel.setThirdDiceX(generateFloatRandomNumber());
        viewModel.setThirdDiceY(generateFloatRandomNumber());


        subscribersList = dataBinding.subscribersList;
        subscribersList.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));
        adapter = new OffersAdapter();

        viewModel.requestOffers().observe(this, new Observer<OfferObj>() {
            @Override
            public void onChanged(OfferObj offerObj) {
                adapter.setList(offerObj.getSubscribers());
                subscribersList.setAdapter(adapter);
            }
        });
    }

    private int generateRandomNumber() {
        Random r = new Random();
        return r.nextInt((6 - 1) + 1) + 1;
    }

    private float generateFloatRandomNumber() {
        Random r = new Random();
        return r.nextFloat();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
