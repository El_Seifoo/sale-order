package com.sale_order.app.withdraw;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.sale_order.app.R;
import com.sale_order.app.classes.SubscriberObj;
import com.sale_order.app.databinding.SubscribersListItemBinding;

import java.util.List;

public class OffersAdapter extends RecyclerView.Adapter<OffersAdapter.Holder> {
    private List<SubscriberObj> list;

    public void setList(List<SubscriberObj> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public OffersAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder((SubscribersListItemBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.subscribers_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull OffersAdapter.Holder holder, int position) {
        holder.dataBinding.setSubscriber(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list != null ? list.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder {
        private SubscribersListItemBinding dataBinding;

        public Holder(@NonNull SubscribersListItemBinding dataBinding) {
            super(dataBinding.getRoot());
            this.dataBinding = dataBinding;
        }
    }
}
