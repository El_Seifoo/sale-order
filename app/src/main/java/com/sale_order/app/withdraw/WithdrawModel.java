package com.sale_order.app.withdraw;

import android.content.Context;
import android.view.View;

import androidx.lifecycle.MutableLiveData;

import com.sale_order.app.R;
import com.sale_order.app.classes.OfferObj;
import com.sale_order.app.classes.OffersResponse;
import com.sale_order.app.classes.SubscriberObj;
import com.sale_order.app.utils.Constants;
import com.sale_order.app.utils.MySingleton;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WithdrawModel {
    private Context context;

    public WithdrawModel(Context context) {
        this.context = context;
    }

    private MutableLiveData<OfferObj> offerObjMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<OfferObj> fetchOffers(final ModelCallback callback) {
        Call<OffersResponse> call = MySingleton.getInstance(context).createService().offers(
                MySingleton.getInstance(context).getStringFromSharedPref(Constants.APP_LANGUAGE, context.getString(R.string.english_key)).equals(context.getString(R.string.arabic_key)) ? null :
                        context.getString(R.string.english_key));
        call.enqueue(new Callback<OffersResponse>() {
            @Override
            public void onResponse(Call<OffersResponse> call, Response<OffersResponse> response) {
                callback.setProgress(View.GONE);
                if (response.code() == 200) {
                    if (response.body() != null) {
                        OfferObj offer = response.body().getOffers();
                        List<SubscriberObj> subscribers = offer.getSubscribers();
                        if (subscribers == null) {
                            callback.setEmptyListTextView(View.VISIBLE);
                            offer.setSubscribers(new ArrayList<SubscriberObj>());
                        } else if (subscribers.isEmpty())
                            callback.setEmptyListTextView(View.VISIBLE);
                        else callback.setEmptyListTextView(View.GONE);
                        offerObjMutableLiveData.setValue(offer);
                    }

                }
            }

            @Override
            public void onFailure(Call<OffersResponse> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.onFailureHandler(t);
            }
        });

        return offerObjMutableLiveData;
    }


    protected interface ModelCallback {
        void setProgress(int progress);

        void setEmptyListTextView(int empty);

        void onFailureHandler(Throwable t);
    }
}
