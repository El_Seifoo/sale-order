package com.sale_order.app.splash;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.sale_order.app.R;
import com.sale_order.app.classes.UserInfoObj;
import com.sale_order.app.login.LoginActivity;
import com.sale_order.app.main.MainActivity;
import com.sale_order.app.options.OptionsActivity;
import com.sale_order.app.utils.MySingleton;

public class SplashActivity extends AppCompatActivity {
    private static final int DELAY_TIME = 3000;
    private Handler handler;
    private Runnable runnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                if (MySingleton.getInstance(SplashActivity.this).isLoggedIn()) {
//                    UserInfoObj user = MySingleton.getInstance(SplashActivity.this).userData();
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
//                    intent.putExtra("email", user.getEmail());
//                    intent.putExtra("username", user.getUserName());
//                    intent.putExtra("password", user.getPassword());
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);

                } else {
                    Intent intent = new Intent(getApplicationContext(), OptionsActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);


                }
            }
        };
        handler.postDelayed(runnable, DELAY_TIME);
    }

    @Override
    protected void onStart() {
        super.onStart();
        handler.postDelayed(runnable, DELAY_TIME);
    }

    @Override
    protected void onStop() {
        super.onStop();
        handler.removeCallbacks(runnable);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runnable);
    }

}
