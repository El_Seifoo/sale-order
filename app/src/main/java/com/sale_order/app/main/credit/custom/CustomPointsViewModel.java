package com.sale_order.app.main.credit.custom;

import android.app.Application;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;

import com.sale_order.app.R;
import com.sale_order.app.classes.OrderObj;
import com.sale_order.app.classes.OrderResponse;
import com.sale_order.app.classes.PaymentResponse;
import com.sale_order.app.utils.MySingleton;

import java.io.IOException;

import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class CustomPointsViewModel extends AndroidViewModel implements CustomPointsModel.ModelCallback {
    private ViewListener viewListener;
    private CustomPointsModel model;
    private ObservableField<Integer> progress;
    private ObservableField<Boolean> buttonsClickable;
    //    private ObservableField<Boolean> dialogStatus;
//    private ObservableField<Integer> paymentMethod;
    private ObservableField<String> requiredPoints, freePoints;

    public CustomPointsViewModel(@NonNull Application application) {
        super(application);
        model = new CustomPointsModel(application);
        progress = new ObservableField<>(View.GONE);
        buttonsClickable = new ObservableField<>(true);
//        dialogStatus = new ObservableField<>(false);
//        paymentMethod = new ObservableField<>(R.id.k_net_payment);
        requiredPoints = new ObservableField<>("");
        freePoints = new ObservableField<>("");
    }

    public void checkPoints() {
        if (getRequiredPoints().get().isEmpty()) {
            viewListener.showToastMessage(getApplication().getString(R.string.required_points_can_not_be_blank));
            return;
        }
        if (Integer.valueOf(getRequiredPoints().get()) < 25) {
            viewListener.showToastMessage(getApplication().getString(R.string.required_points_must_be_more_than_25));
            return;
        }

//        viewListener.showDialog();
        this.requestBuyPoints();
    }

    public void requestBuyPoints() {
        if (!MySingleton.getInstance(getApplication()).isLoggedIn()) {
            this.loginFirst();
            return;
        }

        setProgress(View.VISIBLE);
        setButtonsClickable(false);

        int required = 0;
        if (!getRequiredPoints().get().isEmpty())
            required = Integer.valueOf(getRequiredPoints().get());
        int free = 0;
        if (!getFreePoints().get().isEmpty())
            free = Integer.valueOf(getFreePoints().get());

        int totalPoints = required + free;
        model.buyPoints(totalPoints, this);
    }


    @Override
    public void loginFirst() {
        MySingleton.getInstance(getApplication()).logout();
        viewListener.loginFirst();
    }

    @Override
    public void handleBuyPointsResponse(Response<OrderResponse> response) {
        if (response == null) {
            viewListener.showToastMessage(getApplication().getString(R.string.something_went_wrong));
            return;
        }
        OrderResponse orderResponse = response.body();
        OrderObj order = orderResponse.getOrder();
        String message = orderResponse.getMessage();
        if (order != null) {
            if (message.equals(getApplication().getString(R.string.start_buying_points_response_success)))
                viewListener.startWebView(order);
            else
                viewListener.showToastMessage(order.getErrorMessage() != null ? order.getErrorMessage() : message);
        } else {
            if (message != null)
                viewListener.showToastMessage(getApplication().getString(R.string.something_went_wrong));
            else if (message.isEmpty())
                viewListener.showToastMessage(getApplication().getString(R.string.something_went_wrong));
            else viewListener.showToastMessage(message);
        }
    }

    @Override
    public void onFailureHandler(Throwable t) {
        viewListener.showToastMessage(t instanceof IOException
                ? getApplication().getString(R.string.no_internet_connection)
                : getApplication().getString(R.string.error_fetching_data));
    }


    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }


    public ObservableField<String> getRequiredPoints() {
        return requiredPoints;
    }

    public void setRequiredPoints(String points) {
        this.requiredPoints.set(points);
    }

    public ObservableField<String> getFreePoints() {
        return freePoints;
    }

    public void setFreePoints(String freePoints) {
        this.freePoints.set(freePoints);
    }


    public ObservableField<Boolean> getButtonsClickable() {
        return buttonsClickable;
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }

    @Override
    public void setButtonsClickable(boolean clickable) {
        this.buttonsClickable.set(clickable);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CustomPointsActivity.WEB_VIEW_REQUEST_CODE) {
            if (!data.getExtras().getBoolean("done"))
                viewListener.showToastMessage(getApplication().getString(R.string.failed_to_buy));
            return;
        }
        if (requestCode == CustomPointsActivity.LOGIN_FIRST_REQUEST_CODE && resultCode == RESULT_OK) {
            requestBuyPoints();
        }
    }


    protected interface ViewListener {
//        void showDialog();

        void showToastMessage(String message);

        void startWebView(OrderObj response);

        void loginFirst();

        void paymentDone();
    }
}

    /*
        private void requestCheckPayment(String orderId) {
            setProgress(View.VISIBLE);
            setButtonsClickable(false);
            model.checkPaymentStatus(orderId, this);
        }

        @Override
        public void handleCheckingPaymentResponse(PaymentResponse response) {
            if (response.getMessage() != null) {
                if (response.getMessage().equals(getApplication().getString(R.string.check_payment_response_status_failed_1)))
                    viewListener.showToastMessage(getApplication().getString(R.string.failed_to_buy));
                else
                    viewListener.showToastMessage(getApplication().getString(R.string.payment_done_check_your_points));
                return;
            }
            if (response.getOrderPayment().getStatus().equals(getApplication().getString(R.string.check_payment_response_status_failed))) {
                viewListener.showToastMessage(getApplication().getString(R.string.failed_to_buy));
                return;
            }

            viewListener.showToastMessage(getApplication().getString(R.string.payment_done_check_your_points));
            viewListener.paymentDone();
        }

        public ObservableField<Integer> getPaymentMethod() {
            return paymentMethod;
        }

        public void setPaymentMethod(int paymentMethod) {
            this.paymentMethod.set(paymentMethod);
        }

        public ObservableField<Boolean> getDialogStatus() {
        return dialogStatus;
        }

        protected void checkDialogStatus() {
            if (getDialogStatus().get())
                viewListener.showDialog();
        }

        public void setDialogStatus(boolean dialogStatus) {
            this.dialogStatus.set(dialogStatus);
        }

     */