package com.sale_order.app.main.shopping;

import android.content.Context;
import android.view.View;

import androidx.lifecycle.MutableLiveData;

import com.sale_order.app.R;
import com.sale_order.app.classes.ShoppingObj;
import com.sale_order.app.classes.ShoppingResponse;
import com.sale_order.app.utils.MySingleton;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShoppingModel {
    private Context context;

    public ShoppingModel(Context context) {
        this.context = context;
    }

    private MutableLiveData<List<ShoppingObj>> shoppingListMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<List<ShoppingObj>> fetchShoppingList(final int skip, int take, String language, final ModelCallback callback) {
        Map<String, Object> map = new HashMap<>();
        map.put("skip", skip);
        map.put("take", take);
        if (language.equals(context.getString(R.string.english_key)))
            map.put("Ln", language);

        Call<ShoppingResponse> call = MySingleton.getInstance(context).createService().shoppingList(map);
        call.enqueue(new Callback<ShoppingResponse>() {
            @Override
            public void onResponse(Call<ShoppingResponse> call, Response<ShoppingResponse> response) {
                callback.setProgress(View.GONE);
                List<ShoppingObj> list = response.body().getShoppingList();
                if (list == null) {
                    callback.setEmptyListTextView(View.VISIBLE);
                    return;
                }
                if (list.isEmpty()) {
                    callback.setEmptyListTextView(View.VISIBLE);
                    return;
                }

                callback.setEmptyListTextView(View.GONE);
                shoppingListMutableLiveData.setValue(list);
            }

            @Override
            public void onFailure(Call<ShoppingResponse> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.onFailureHandler(t);
            }
        });

        return shoppingListMutableLiveData;
    }

    protected interface ModelCallback {

        void setProgress(int progress);

        void onFailureHandler(Throwable t);

        void setEmptyListTextView(int visibility);
    }
}
