package com.sale_order.app.main.credit.custom;

public interface Presenter {
    void onNextButtonClicked();
}
