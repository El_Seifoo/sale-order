package com.sale_order.app.main.shopping.product;

import android.content.Context;
import android.util.Log;
import android.view.View;

import androidx.lifecycle.MutableLiveData;

import com.sale_order.app.classes.OrderObj;
import com.sale_order.app.classes.OrderPayment;
import com.sale_order.app.classes.OrderResponse;
import com.sale_order.app.classes.PaymentResponse;
import com.sale_order.app.classes.ProductResponse;
import com.sale_order.app.classes.ShoppingObj;
import com.sale_order.app.classes.SignupResponse;
import com.sale_order.app.classes.UserInfoObj;
import com.sale_order.app.utils.Constants;
import com.sale_order.app.utils.MySingleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductModel {
    private Context context;

    public ProductModel(Context context) {
        this.context = context;
    }

    private MutableLiveData<ShoppingObj> shoppingObjMutableLiveData = new MutableLiveData<>();

//    protected void addCurrentUser(UserInfoObj user) {
//        shoppingObjMutableLiveData.getValue().setCurrentUser(user);
//    }

    protected MutableLiveData<ShoppingObj> fetchProduct(String productId, final ModelCallback callback) {
        Call<ProductResponse> call = MySingleton.getInstance(context).createService().certainProduct(productId);
        call.enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                if (response.code() == 200)
                    if (response.body() != null) {
                        ShoppingObj shopping = response.body().getProduct();
                        if (shopping != null)
                            shoppingObjMutableLiveData.setValue(shopping);
                        else
                            callback.handleEmptyProduct();


                    }
            }

            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.onFailureHandler(t, 0);
            }
        });

        return shoppingObjMutableLiveData;
    }

    protected void makeOrder(String productId, int quantity, final ModelCallback callback) {
        Call<SignupResponse> call = MySingleton.getInstance(context).createService().makeOrder(
                MySingleton.getInstance(context).getStringFromSharedPref(Constants.USER_TOKEN, ""),
                productId, quantity);

        call.enqueue(new Callback<SignupResponse>() {
            @Override
            public void onResponse(Call<SignupResponse> call, Response<SignupResponse> response) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                if (response.code() == 401) {
                    callback.loginFirst();
                    return;
                }
                if (response.code() == 200)
                    callback.handleMakeOrderResponse(response.body().getMessage());
            }

            @Override
            public void onFailure(Call<SignupResponse> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.onFailureHandler(t, 1);
            }
        });
    }


    protected interface ModelCallback {
        void setProgress(int progress);

        void setButtonsClickable(boolean clickable);

        void onFailureHandler(Throwable t, int index);

        void handleMakeOrderResponse(String message);

        void loginFirst();

        void handleEmptyProduct();
    }
}
