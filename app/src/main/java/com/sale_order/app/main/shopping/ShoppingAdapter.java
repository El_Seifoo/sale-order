package com.sale_order.app.main.shopping;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.sale_order.app.R;
import com.sale_order.app.classes.ShoppingObj;
import com.sale_order.app.databinding.ShoppingListItemBinding;

import java.util.List;

public class ShoppingAdapter extends RecyclerView.Adapter<ShoppingAdapter.Holder> {
    private List<ShoppingObj> shoppingList;
    private final OnProductClicked listener;

    public ShoppingAdapter(OnProductClicked listener) {
        this.listener = listener;
    }

    public void setShoppingList(List<ShoppingObj> shoppingList) {
        this.shoppingList = shoppingList;
        notifyDataSetChanged();
    }

    public void addShoppingList(List<ShoppingObj> shoppingList) {
        this.shoppingList.addAll(shoppingList);
        notifyDataSetChanged();
    }

    protected interface OnProductClicked {
        void onProductClickListener(String productId, int position);
    }

    protected void removeItemByPosition(Context context, int position) {
        shoppingList.remove(position);
        notifyItemRemoved(position);
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder((ShoppingListItemBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.shopping_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        holder.dataBinding.setShopping(shoppingList.get(position));
    }

    @Override
    public int getItemCount() {
        return shoppingList != null ? shoppingList.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ShoppingListItemBinding dataBinding;

        public Holder(@NonNull ShoppingListItemBinding dataBinding) {
            super(dataBinding.getRoot());
            this.dataBinding = dataBinding;
            this.dataBinding.getRoot().setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onProductClickListener(dataBinding.getShopping().getId(), getAdapterPosition());
        }
    }
}