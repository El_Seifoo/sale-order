package com.sale_order.app.main.credit.custom;

import android.content.Context;
import android.view.View;

import com.sale_order.app.classes.OrderResponse;
import com.sale_order.app.classes.PaymentResponse;
import com.sale_order.app.utils.Constants;
import com.sale_order.app.utils.MySingleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomPointsModel {
    private Context context;

    public CustomPointsModel(Context context) {
        this.context = context;
    }

    public void buyPoints(int points, final ModelCallback callback) {
        Call<OrderResponse> call = MySingleton.getInstance(context).createService().buyPoints(
                "Bearer " + MySingleton.getInstance(context).getStringFromSharedPref(Constants.USER_TOKEN, ""),
                points);

        call.enqueue(new Callback<OrderResponse>() {
            @Override
            public void onResponse(Call<OrderResponse> call, Response<OrderResponse> response) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                if (response.code() == 401) {
                    callback.loginFirst();
                    return;
                }
                if (response.code() == 200) {
                    callback.handleBuyPointsResponse(response);
                    return;
                }

                callback.handleBuyPointsResponse(null);
            }

            @Override
            public void onFailure(Call<OrderResponse> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.onFailureHandler(t);
            }
        });
    }

    /*
        protected void checkPaymentStatus(String orderId, final ModelCallback callback) {
        Call<PaymentResponse> call = MySingleton.getInstance(context).createService().checkPayment(
                "Bearer " + MySingleton.getInstance(context).getStringFromSharedPref(Constants.USER_TOKEN, ""),
                orderId);
        call.enqueue(new Callback<PaymentResponse>() {
            @Override
            public void onResponse(Call<PaymentResponse> call, Response<PaymentResponse> response) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.handleCheckingPaymentResponse(response.body());
            }

            @Override
            public void onFailure(Call<PaymentResponse> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.onFailureHandler(t);

            }
        });
    }
    */

    protected interface ModelCallback {

        void setProgress(int progress);

        void loginFirst();

        void handleBuyPointsResponse(Response<OrderResponse> response);

        void onFailureHandler(Throwable t);

//        void handleCheckingPaymentResponse(PaymentResponse response);

        void setButtonsClickable(boolean clickable);
    }
}
