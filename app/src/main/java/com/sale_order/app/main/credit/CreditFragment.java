package com.sale_order.app.main.credit;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sale_order.app.R;
import com.sale_order.app.classes.CreditObj;
import com.sale_order.app.classes.OrderObj;
import com.sale_order.app.databinding.FragmentCreditBinding;
import com.sale_order.app.login.LoginActivity;
import com.sale_order.app.main.credit.custom.CustomPointsActivity;
import com.sale_order.app.main.shopping.product.webview.PaymentActivity;
import com.sale_order.app.utils.Constants;
import com.sale_order.app.utils.Language;
import com.sale_order.app.utils.MySingleton;

import java.util.List;

public class CreditFragment extends Fragment implements CreditAdapter.ListItemClickListener, CreditViewModel.ViewListener {
    protected final static int WEB_VIEW_REQUEST_CODE = 1;
    protected static final int LOGIN_FIRST_REQUEST_CODE = 2;
    private CreditViewModel viewModel;
    private FragmentCreditBinding dataBinding;
    private RecyclerView creditList;
    private CreditAdapter adapter;


    public static CreditFragment newInstance() {
        return new CreditFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Language.setLanguage(getContext(), MySingleton.getInstance(getContext()).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.arabic_key)));
        dataBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_credit, container, false);
        View view = dataBinding.getRoot();
        viewModel = ViewModelProviders.of(this).get(CreditViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);

        creditList = dataBinding.creditList;
        creditList.setLayoutManager(new GridLayoutManager(getContext(), 2));
        adapter = new CreditAdapter(this);
        creditList.setAdapter(adapter);

        viewModel.requestAllCredits(0, 100,
                MySingleton.getInstance(getContext()).getStringFromSharedPref(Constants.APP_LANGUAGE, getString(R.string.english_key)))
                .observe(this, new Observer<List<CreditObj>>() {
                    @Override
                    public void onChanged(List<CreditObj> creditObjs) {
                        creditObjs.add(new CreditObj("0", getString(R.string.custom), 0, 0.0));
                        adapter.setCredits(creditObjs);
                    }
                });

//        createDialog();

        return view;
    }

    /*
        Dialog dialog;

        private void createDialog() {
            dialog = new Dialog(getContext());

            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialog.setContentView(R.layout.payment_method_dialog_view);
            dialog.getWindow().setLayout((int) (getResources().getDisplayMetrics().widthPixels * 0.70), WindowManager.LayoutParams.WRAP_CONTENT);

            TextView pay = dialog.findViewById(R.id.pay_button);
            pay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    viewModel.requestBuyPoints(viewModel.getPoints().get());
                    dialog.dismiss();
                }
            });

            RadioGroup paymentGroup = dialog.findViewById(R.id.radio_group);
            paymentGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    viewModel.setPaymentMethod(checkedId);
                }
            });

            paymentGroup.check(viewModel.getPaymentMethod().get());


            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    viewModel.setDialogStatus(false);
                }
            });

            dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    viewModel.setDialogStatus(true);
                }
            });
            viewModel.checkDialogStatus();
        }

        @Override
        public void showDialog() {
            dialog.show();
        }
    */

    @Override
    public void onCreditItemClicked(String packageId, int points, boolean isCustom) {
        if (isCustom)
            startActivity(new Intent(getContext(), CustomPointsActivity.class));
        else {
//            viewModel.setPoints(points);
            viewModel.requestBuyPoints(packageId);
//            showDialog();
        }
    }


    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void startWebView(OrderObj response) {
        Intent intent = new Intent(getContext(), PaymentActivity.class);
        intent.putExtra("order", response);
        intent.putExtra("isPoints", true);
        startActivityForResult(intent, WEB_VIEW_REQUEST_CODE);
    }

    @Override
    public void loginFirst() {
        Intent loginIntent = new Intent(getContext(), LoginActivity.class);
        loginIntent.putExtra("CreditFragment", "");
        startActivityForResult(loginIntent, LOGIN_FIRST_REQUEST_CODE);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        viewModel.onActivityResult(requestCode, resultCode, data);

    }

//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        dialog.dismiss();
//    }
}
