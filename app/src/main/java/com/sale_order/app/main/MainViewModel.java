package com.sale_order.app.main;

import android.app.Application;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.AndroidViewModel;

import com.google.android.material.navigation.NavigationView;
import com.sale_order.app.R;
import com.sale_order.app.about.AboutUsActivity;
import com.sale_order.app.account.MyAccountActivity;
import com.sale_order.app.login.LoginActivity;
import com.sale_order.app.main.contact.ContactUsFragment;
import com.sale_order.app.main.credit.CreditFragment;
import com.sale_order.app.main.home.HomeFragment;
import com.sale_order.app.main.prevAuction.PrevAuctionFragment;
import com.sale_order.app.main.shopping.ShoppingFragment;
import com.sale_order.app.myPoints.MyPointsActivity;
import com.sale_order.app.notifications.NotificationsActivity;
import com.sale_order.app.options.OptionsActivity;
import com.sale_order.app.settings.SettingsActivity;
import com.sale_order.app.termsConditions.TermsConditionsActivity;
import com.sale_order.app.utils.MySingleton;
import com.sale_order.app.withdraw.WithdrawActivity;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainViewModel extends AndroidViewModel {
    private ViewListener viewListener;

    public MainViewModel(@NonNull Application application) {
        super(application);
    }

    protected void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    protected void checkSavedInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            viewListener.handleNavFragments(0, MainActivity.TAG_HOME, false);
            viewListener.loadHomeFragment();
        }
    }

    //nav_home0, nav_my_account1, nav_withdraw2, nav_my_points3, nav_notification4,
    // nav_settings5, nav_terms_conditions6, nav_about_us7, nav_login8, nav_logout9
    protected void loadHeader(NavigationView navigationView, View navHeader) {
        if (MySingleton.getInstance(navigationView.getContext()).isLoggedIn()) {
            navigationView.getMenu().getItem(7).setVisible(false);
            navigationView.getMenu().getItem(8).setVisible(true);
        } else {
            navigationView.getMenu().getItem(7).setVisible(true);
            navigationView.getMenu().getItem(8).setVisible(false);

        }
    }

    protected boolean handleOnNavigationItemSelected(MenuItem menuItem) {

        switch (menuItem.getItemId()) {
            case R.id.bottom_nav_shopping:
                viewListener.handleNavFragments(1, MainActivity.TAG_SHOPPING, true);
                break;
            case R.id.bottom_nav_credit:
                viewListener.handleNavFragments(2, MainActivity.TAG_CREDIT, true);
                break;
            case R.id.bottom_nav_prev_auction:
                viewListener.handleNavFragments(3, MainActivity.TAG_PREVIOUS_AUCTION, true);
                break;
            case R.id.bottom_nav_contact_us:
                viewListener.handleNavFragments(4, MainActivity.TAG_CONTACT_US, true);
                break;
            case R.id.nav_my_account:
                if (MySingleton.getInstance(getApplication()).isLoggedIn())
                    return viewListener.navigateToActivity(MyAccountActivity.class);
                viewListener.closeDrawer();
                break;
//            case R.id.nav_withdraw:
//                return viewListener.navigateToActivity(WithdrawActivity.class);
            case R.id.nav_my_points:
                if (MySingleton.getInstance(getApplication()).isLoggedIn())
                    return viewListener.navigateToActivity(MyPointsActivity.class);
                viewListener.closeDrawer();
                break;
            case R.id.nav_notification:
                if (MySingleton.getInstance(getApplication()).isLoggedIn())
                    return viewListener.navigateToActivity(NotificationsActivity.class);
                viewListener.closeDrawer();
                break;
            case R.id.nav_settings:
                return viewListener.navigateToActivity1(SettingsActivity.class);
            case R.id.nav_terms_conditions:
                return viewListener.navigateToActivity(TermsConditionsActivity.class);
            case R.id.nav_about_us:
                return viewListener.navigateToActivity(AboutUsActivity.class);
            case R.id.nav_login:
                return viewListener.navigateToActivity(LoginActivity.class);
            case R.id.nav_logout:
                return viewListener.handleLogoutAction(OptionsActivity.class);
            case R.id.bottom_nav_home:
            case R.id.nav_home:
            default:
                // navItemIndex , CURRENT_TAG
                viewListener.handleNavFragments(0, MainActivity.TAG_HOME, false);
        }

        if (!MySingleton.getInstance(getApplication()).isLoggedIn() &&
                (menuItem.getItemId() == R.id.nav_my_account
                        || menuItem.getItemId() == R.id.nav_my_points
                        || menuItem.getItemId() == R.id.nav_notification)) {
            viewListener.showToastMessage(getApplication().getString(R.string.you_should_login_first));
            return true;
        }
        viewListener.loadHomeFragment();
        return true;

    }

    protected Fragment getSelectedFragment(int naveItemIndex) {
        switch (naveItemIndex) {
            case 1:
                return ShoppingFragment.newInstance();
            case 2:
                return CreditFragment.newInstance();
            case 3:
                return PrevAuctionFragment.newInstance();
            case 4:
                return ContactUsFragment.newInstance();
            case 0:
            default:
                return HomeFragment.newInstance();
        }
    }

    public boolean onBackBtnPressed(boolean drawerOpen, boolean shouldLoadHomeFragOnBackPress) {
        if (drawerOpen) {
            viewListener.closeDrawer();
            return true;
        }

        // return to home fragment if user pressed back in other fragment
        if (shouldLoadHomeFragOnBackPress) {
            viewListener.selectHome();
            return true;
        }

        return false;
    }


    protected interface ViewListener {
        void loadHomeFragment();

        boolean navigateToActivity(Class destination);

        boolean handleLogoutAction(Class destination);

        void closeDrawer();

        void handleNavFragments(int naveItemIndex, String currentTag, boolean shouldLoadHomeFragOnBackPress);

        boolean navigateToActivity1(Class destination);

        void selectHome();

        void showToastMessage(String message);
    }
}
