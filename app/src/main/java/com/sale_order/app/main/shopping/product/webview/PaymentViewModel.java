package com.sale_order.app.main.shopping.product.webview;

import android.app.Application;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;

import com.sale_order.app.R;

import java.io.IOException;

public class PaymentViewModel extends AndroidViewModel implements PaymentModel.ModelCallback {
    private PaymentModel model;
    private ViewListener viewListener;
    private ObservableField<Integer> progress;
    private ObservableField<String> errorMessage;
    private ObservableField<Integer> errorView;
    private ObservableField<Boolean> paymentStatus;

    public PaymentViewModel(@NonNull Application application) {
        super(application);
        model = new PaymentModel(application);
        progress = new ObservableField<>(View.GONE);
        errorView = new ObservableField<>(View.GONE);
        errorMessage = new ObservableField<>("");
        paymentStatus = new ObservableField<>(false);
    }

//    private String paymentId;
//    private String orderNumber;
//    private boolean isPoints;

    protected void disclose(String paymentId, String orderNumber, boolean isPoints) {
//        this.paymentId = paymentId;
//        this.orderNumber = orderNumber;
//        this.isPoints = isPoints;
        setProgress(View.VISIBLE);
        model.disclosePayment(paymentId, orderNumber, isPoints, this);
    }

//    public void onRetryClickListener(View view) {
//        setProgress(View.VISIBLE);
//        setErrorView(View.GONE);
//        model.disclosePayment(paymentId, orderNumber, isPoints, this);
//    }

    @Override
    public void onFailure(Throwable t) {
        setPaymentStatus(false);
        setErrorView(View.VISIBLE);
        setErrorMessage(t instanceof IOException
                ? getApplication().getString(R.string.no_internet_connection)
                : getApplication().getString(R.string.error_sending_data));
    }

    @Override
    public void handleResponse(int code) {
        if (code == 200) {
            setPaymentStatus(true);
            viewListener.paymentDone(getApplication().getString(R.string.payment_done_check_your_points));
        } else {
            setPaymentStatus(false);
            setErrorView(View.VISIBLE);
            setErrorMessage(getApplication().getString(R.string.error_sending_data));
        }
    }


    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public ObservableField<Boolean> getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(boolean status) {
        this.paymentStatus.set(status);
    }

    public ObservableField<String> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String message) {
        errorMessage.set(message);
    }

    public ObservableField<Integer> getErrorView() {
        return errorView;
    }

    public void setErrorView(int errorViewStatus) {
        this.errorView.set(errorViewStatus);
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }


    protected interface ViewListener {
        void showToastMessage(String message);

        void paymentDone(String string);
    }
}
