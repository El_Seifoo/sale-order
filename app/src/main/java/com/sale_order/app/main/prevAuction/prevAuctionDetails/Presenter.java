package com.sale_order.app.main.prevAuction.prevAuctionDetails;

public interface Presenter {
    void onPayButtonClicked();
}
