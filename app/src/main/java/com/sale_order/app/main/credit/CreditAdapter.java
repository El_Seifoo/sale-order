package com.sale_order.app.main.credit;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.sale_order.app.R;
import com.sale_order.app.classes.CreditObj;
import com.sale_order.app.databinding.CreditListItemBinding;

import java.util.List;

public class CreditAdapter extends RecyclerView.Adapter<CreditAdapter.Holder> {
    private List<CreditObj> credits;
    private final ListItemClickListener listener;

    public CreditAdapter(ListItemClickListener listener) {
        this.listener = listener;
    }

    public void setCredits(List<CreditObj> credits) {
        this.credits = credits;
        notifyDataSetChanged();
    }

    public void addCredits(List<CreditObj> credits) {
        this.credits.addAll(credits);
        notifyDataSetChanged();
    }

    protected interface ListItemClickListener {
        void onCreditItemClicked(String packageId, int points, boolean isCustom);
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder((CreditListItemBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.credit_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        holder.binding.setCredit(credits.get(position));
    }

    @Override
    public int getItemCount() {
        return credits != null ? credits.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CreditListItemBinding binding;

        public Holder(@NonNull CreditListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.buyTextView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onCreditItemClicked(binding.getCredit().getId(), binding.getCredit().getPoints(), getAdapterPosition() == credits.size() - 1);
        }
    }
}