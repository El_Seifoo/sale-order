package com.sale_order.app.main.contact;

import android.content.Context;
import android.view.View;

import com.sale_order.app.classes.ContactObj;
import com.sale_order.app.classes.SignupResponse;
import com.sale_order.app.utils.MySingleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactModel {
    private Context context;

    public ContactModel(Context context) {
        this.context = context;
    }

    protected void sendMessage(ContactObj contactObj, final ModelCallback callback) {
        Call<SignupResponse> call = MySingleton.getInstance(context).createService().contactUs(contactObj);
        call.enqueue(new Callback<SignupResponse>() {
            @Override
            public void onResponse(Call<SignupResponse> call, Response<SignupResponse> response) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.handleSendingMessageResponse(response.body().getMessage());
            }

            @Override
            public void onFailure(Call<SignupResponse> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.onFailureHandler(t);
            }
        });
    }

    protected interface ModelCallback {
        void setProgress(int progress);

        void setButtonsClickable(boolean clickable);

        void onFailureHandler(Throwable t);

        void handleSendingMessageResponse(String message);
    }
}
