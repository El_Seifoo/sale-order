package com.sale_order.app.main.prevAuction.prevAuctionDetails;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.sale_order.app.R;
import com.sale_order.app.bidding.BiddingAdapter;
import com.sale_order.app.bidding.BiddingViewModel;
import com.sale_order.app.classes.AuctionObj;
import com.sale_order.app.classes.OrderObj;
import com.sale_order.app.databinding.ActivityPrevAuctionDetailsBinding;
import com.sale_order.app.login.LoginActivity;
import com.sale_order.app.main.MainActivity;
import com.sale_order.app.main.home.SliderAdapter;
import com.sale_order.app.main.shopping.product.webview.PaymentActivity;
import com.sale_order.app.utils.Constants;
import com.sale_order.app.utils.Language;
import com.sale_order.app.utils.MySingleton;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class PrevAuctionDetailsActivity extends AppCompatActivity implements PrevAuctionDetailsViewModel.ViewListener {
    //    protected final static int WEB_VIEW_REQUEST_CODE = 1;
    protected static final int LOGIN_FIRST_REQUEST_CODE = 2;
    private PrevAuctionDetailsViewModel viewModel;
    private ActivityPrevAuctionDetailsBinding dataBinding;
    private BiddingAdapter adapter;

    private ViewPager viewPager;
    private SliderAdapter sliderAdapter;

    Timer t;
    private static final int DELAY_TIME = 6000;
    private int currentPage = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.arabic_key)));
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_prev_auction_details);
        viewModel = ViewModelProviders.of(this).get(PrevAuctionDetailsViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);
//        dataBinding.setPresenter(this);

        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        dataBinding.biddingList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        adapter = new BiddingAdapter();
        dataBinding.biddingList.setAdapter(adapter);

        viewPager = dataBinding.viewPager;
        sliderAdapter = new SliderAdapter();

        viewModel.requestCertainAuction(getIntent().getExtras().getString("AuctionID")).observe(this, new Observer<AuctionObj>() {
            @Override
            public void onChanged(AuctionObj auctionObj) {
//                Log.e("auctionBidding .....", auctionObj.getTimer().toString());
                dataBinding.setAuction(auctionObj);
                adapter.setBidders(auctionObj.getBiddersInAuctions());

                // images slider
                sliderAdapter.setImages(auctionObj.getImages(), new ArrayList<String>());
                viewPager.setAdapter(sliderAdapter);
                if (sliderAdapter.getCount() > 1)
                    startAutoSliding();
            }
        });

//        createDialog();
    }

    private void startAutoSliding() {
        if (t != null)
            t.cancel();
        t = new Timer();
        t.scheduleAtFixedRate(new TimerTask() {

                                  @Override
                                  public void run() {
                                      currentPage = viewPager.getCurrentItem();
                                      if (currentPage != viewPager.getAdapter().getCount() - 1) {
                                          viewPager.setCurrentItem(++currentPage);
                                      } else {
                                          currentPage = 0;
                                          viewPager.setCurrentItem(currentPage);
                                      }
                                  }
                              },
                //Set how long before to start calling the TimerTask (in milliseconds)
                DELAY_TIME,
                //Set the amount of time between each execution (in milliseconds)
                DELAY_TIME);
    }

    /*
        Dialog dialog;

        private void createDialog() {
            dialog = new Dialog(this);

            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialog.setContentView(R.layout.payment_method_dialog_view);
            dialog.getWindow().setLayout((int) (getResources().getDisplayMetrics().widthPixels * 0.70), WindowManager.LayoutParams.WRAP_CONTENT);

            TextView pay = dialog.findViewById(R.id.pay_button);
            pay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    viewModel.requestPayAuction(getIntent().getExtras().getString("AuctionID"), dataBinding.getAuction().getCurrentUser());
                    dialog.dismiss();
                }
            });

            RadioGroup paymentGroup = dialog.findViewById(R.id.radio_group);
            paymentGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    viewModel.setPaymentMethod(checkedId);
                }
            });

            paymentGroup.check(viewModel.getPaymentMethod().get());


            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    viewModel.setDialogStatus(false);
                }
            });

            dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    viewModel.setDialogStatus(true);
                }
            });
            viewModel.checkDialogStatus();
        }

        @Override
        public void showDialog() {
        dialog.show();
    }

        @Override
        public void onPayButtonClicked() {
    //        showDialog();
            viewModel.requestPayAuction(getIntent().getExtras().getString("AuctionID"), dataBinding.getAuction().getCurrentUser());
        }


        @Override
        public void startWebView(OrderObj response) {
            Intent intent = new Intent(this, PaymentActivity.class);
            intent.putExtra("order", response);
            intent.putExtra("isPoints", false);
            startActivityForResult(intent, WEB_VIEW_REQUEST_CODE);
        }

        @Override
        public void paymentDone() {
            showToastMessage(getString(R.string.your_purchase_was_successful));
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    */

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void loginFirst() {
        Intent loginIntent = new Intent(this, LoginActivity.class);
        loginIntent.putExtra("PrevAuctionDetailActivity", "");
        startActivityForResult(loginIntent, LOGIN_FIRST_REQUEST_CODE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        viewModel.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (t != null)
            t.cancel();
    }
}
