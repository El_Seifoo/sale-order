package com.sale_order.app.main.home;

import android.app.Application;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.sale_order.app.R;
import com.sale_order.app.classes.AuctionObj;
import com.sale_order.app.classes.SliderObj;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class HomeViewModel extends AndroidViewModel implements HomeModel.ModelCallback {
    private Application application;
    private HomeModel model;
    private ViewListener viewListener;
    private ObservableField<Integer> progress;
    private ObservableField<Integer> emptyListTextView;
    private ObservableField<String> errorMessage;
    private ObservableField<Integer> errorView;

    public HomeViewModel(@NonNull Application application) {
        super(application);
        this.application = application;
        model = new HomeModel(application);
        progress = new ObservableField<>(View.GONE);
        emptyListTextView = new ObservableField<>(View.GONE);
        errorView = new ObservableField<>(View.GONE);
        errorMessage = new ObservableField<>("");
    }

    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    private int skip;
    private int take;
    private String language;

    protected MutableLiveData<List<AuctionObj>> requestAuctions(int skip, int take, String language) {
        this.skip = skip;
        this.take = take;
        this.language = language;
        setProgress(View.VISIBLE);
        return model.fetchAuctionsList(skip, take, language, this);
    }

    public void onRetryClickListener(View view) {
        setProgress(View.VISIBLE);
        setErrorView(View.GONE);
        model.fetchAuctionsList(skip, take, language, this);
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    public ObservableField<Integer> getEmptyListTextView() {
        return emptyListTextView;
    }

    public ObservableField<String> getErrorMessage() {
        return errorMessage;
    }

    protected void setErrorMessage(String message) {
        errorMessage.set(message);
    }

    public ObservableField<Integer> getErrorView() {
        return errorView;
    }

    protected void setErrorView(int errorViewStatus) {
        this.errorView.set(errorViewStatus);
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }

    @Override
    public void setEmptyListTextView(int empty) {
        this.emptyListTextView.set(empty);
    }

    @Override
    public void onFailureHandler(Throwable t) {
        setErrorView(View.VISIBLE);
        if (t instanceof IOException)
            setErrorMessage(application.getString(R.string.no_internet_connection));
        else
            setErrorMessage(application.getString(R.string.error_fetching_data));
    }

    @Override
    public void handleSlider(SliderObj slider) {
        List<String> images = new ArrayList<>();
        List<String> videos = new ArrayList<>();
        images.add(slider.getImage());
        images.add(slider.getInstaImage());
        videos.add(slider.getImageVideo());
        videos.add(slider.getInstaVideo());
        viewListener.setSlider(images, videos);
    }

    protected interface ViewListener {

        void setSlider(List<String> images, List<String> videos);
    }
}
