package com.sale_order.app.main.shopping.product;

public interface Presenter {
    void onPlusTextClicked();

    void onMinusTextClicked();

//    void onNextButtonClicked();
//
//    void onPrevButtonClicked();

    void onPayNowButtonClicked();
}
