package com.sale_order.app.main.prevAuction;

import android.content.Context;
import android.view.View;

import androidx.lifecycle.MutableLiveData;

import com.sale_order.app.R;
import com.sale_order.app.classes.AuctionObj;
import com.sale_order.app.classes.AuctionsResponse;
import com.sale_order.app.utils.MySingleton;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PrevAuctionModel {
    private Context context;

    public PrevAuctionModel(Context context) {
        this.context = context;
    }

    private MutableLiveData<List<AuctionObj>> auctionsListMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<List<AuctionObj>> fetchPrevAuctionsList(final int skip, int take, String language, final ModelCallback callback) {
        Map<String, Object> map = new HashMap<>();
        map.put("skip", skip);
        map.put("take", take);
        if (language.equals(context.getString(R.string.english_key)))
            map.put("Ln", language);

        Call<AuctionsResponse> call = MySingleton.getInstance(context).createService().prevAuctions(map);
        call.enqueue(new Callback<AuctionsResponse>() {
            @Override
            public void onResponse(Call<AuctionsResponse> call, Response<AuctionsResponse> response) {
                callback.setProgress(View.GONE);
                List<AuctionObj> list = response.body().getAuctions();
                if (skip == 0 && list == null) {
                    callback.setEmptyListTextView(View.VISIBLE);
                    return;
                }
                if (skip == 0 && list.isEmpty()) {
                    callback.setEmptyListTextView(View.VISIBLE);
                    return;
                }
                callback.setEmptyListTextView(View.GONE);
                auctionsListMutableLiveData.setValue(list);
            }

            @Override
            public void onFailure(Call<AuctionsResponse> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.onFailureHandler(t);
            }
        });

        return auctionsListMutableLiveData;
    }

    protected interface ModelCallback {
        void setProgress(int progress);

        void setEmptyListTextView(int empty);

        void onFailureHandler(Throwable t);
    }
}
