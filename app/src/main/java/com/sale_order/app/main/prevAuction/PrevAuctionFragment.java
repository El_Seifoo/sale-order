package com.sale_order.app.main.prevAuction;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;

import com.sale_order.app.R;
import com.sale_order.app.classes.AuctionObj;
import com.sale_order.app.databinding.FragmentPrevAuctionBinding;
import com.sale_order.app.main.prevAuction.prevAuctionDetails.PrevAuctionDetailsActivity;
import com.sale_order.app.utils.Constants;
import com.sale_order.app.utils.Language;
import com.sale_order.app.utils.MySingleton;

import java.util.List;

public class PrevAuctionFragment extends Fragment implements PrevAuctionAdapter.onPrevAuctionItemClicked {
    private PrevAuctionViewModel viewModel;
    private FragmentPrevAuctionBinding dataBinding;
    private PrevAuctionAdapter adapter;

    public static PrevAuctionFragment newInstance() {
        return new PrevAuctionFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Language.setLanguage(getContext(), MySingleton.getInstance(getContext()).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.arabic_key)));
        dataBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_prev_auction, container, false);
        View view = dataBinding.getRoot();
        viewModel = ViewModelProviders.of(this).get(PrevAuctionViewModel.class);
        dataBinding.setViewModel(viewModel);

        dataBinding.prevAuctionList.setLayoutManager(new GridLayoutManager(getContext(), 2));
        adapter = new PrevAuctionAdapter(this);
        dataBinding.prevAuctionList.setAdapter(adapter);

        viewModel.requestPrevAuctions(0, 100,
                MySingleton.getInstance(getContext()).getStringFromSharedPref(Constants.APP_LANGUAGE, getString(R.string.english_key))).observe(this, new Observer<List<AuctionObj>>() {
            @Override
            public void onChanged(List<AuctionObj> auctionObjs) {
                adapter.setAuctions(auctionObjs);
            }
        });

        return view;
    }

    @Override
    public void onItemClicked(String id) {
        Intent auctionIntent = new Intent(getContext(), PrevAuctionDetailsActivity.class);
        auctionIntent.putExtra("AuctionID", id);
        startActivity(auctionIntent);
    }
}
