package com.sale_order.app.main.shopping.product;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.sale_order.app.R;
import com.sale_order.app.classes.OrderObj;
import com.sale_order.app.classes.ShoppingObj;
import com.sale_order.app.databinding.ActivityProductBinding;
import com.sale_order.app.login.LoginActivity;
import com.sale_order.app.main.MainActivity;
import com.sale_order.app.main.shopping.product.webview.PaymentActivity;
import com.sale_order.app.utils.Constants;
import com.sale_order.app.utils.Language;
import com.sale_order.app.utils.MySingleton;

import java.util.ArrayList;
import java.util.List;

public class ProductActivity extends AppCompatActivity implements Presenter, ProductViewModel.ViewListener {
    protected static final int LOGIN_FIRST_REQUEST_CODE = 2;
    private ActivityProductBinding dataBinding;
    private ProductViewModel viewModel;
    private ViewPager viewPager;
    private SliderAdapter sliderAdapter;
    private String productId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.arabic_key)));
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_product);
        viewModel = ViewModelProviders.of(this).get(ProductViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);

        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = dataBinding.viewPager;

        sliderAdapter = new SliderAdapter();
        viewPager.setAdapter(sliderAdapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int currentPosition) {
//                position = currentPosition;
                viewModel.setViewPagerCurrentPosition(currentPosition);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        productId = getIntent().getExtras().getString("productId");
        viewModel.requestProduct(productId).observe(this, new Observer<ShoppingObj>() {
            @Override
            public void onChanged(ShoppingObj shoppingObj) {
                dataBinding.setProduct(shoppingObj);
                viewModel.setProductQuantity(shoppingObj.getQuantity());
                sliderAdapter.setImages(shoppingObj.getImages());
                viewPager.setCurrentItem(viewModel.getViewPagerCurrentPosition().get());
            }
        });

    }

    @Override
    public void onPlusTextClicked() {
        viewModel.setQuantityField(1);
    }

    @Override
    public void onMinusTextClicked() {
        viewModel.setQuantityField(-1);
    }

    /*
        int position;
        @Override
        public void onNextButtonClicked() {
            position = viewPager.getCurrentItem();
            if (position != viewPager.getAdapter().getCount() - 1) {
                viewPager.setCurrentItem(++position);
                viewModel.setViewPagerCurrentPosition(position);
            }

        }

        @Override
        public void onPrevButtonClicked() {
            position = viewPager.getCurrentItem();
            if (position != 0) {
                viewPager.setCurrentItem(--position);
                viewModel.setViewPagerCurrentPosition(position);
            }
        }

     */


    @Override
    public void onPayNowButtonClicked() {
        viewModel.requestMakeOrder(getIntent().getExtras().getString("productId"));
    }


    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void BuyingDone() {
        showToastMessage(getString(R.string.your_purchase_was_successful));
        if (viewModel.getProductQuantity().get().intValue() - viewModel.getQuantityField().get().intValue() > 0) {
            Log.e("Done", viewModel.getProductQuantity().get().intValue() - viewModel.getQuantityField().get().intValue() + "");
            viewModel.setProductQuantity(viewModel.getProductQuantity().get().intValue() - viewModel.getQuantityField().get().intValue());
            viewModel.setQuantityField(0);// reset the quantity picked by user
        } else {
            viewModel.setProductQuantity(0);
            viewModel.setQuantityField(0);
        }
//        Intent intent = new Intent(this, MainActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        startActivity(intent);
    }


    @Override
    public void loginFirst() {
        Intent loginIntent = new Intent(this, LoginActivity.class);
        loginIntent.putExtra("ProductActivity", "");
        startActivityForResult(loginIntent, LOGIN_FIRST_REQUEST_CODE);
    }

    @Override
    public void emptyProduct() {
        showToastMessage(getApplication().getString(R.string.sorry_product_sold_out));
        setActResult();
        finish();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        viewModel.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (viewModel.getProductQuantity().get().intValue() == 0) {
                setActResult();
            }
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (viewModel.getProductQuantity().get().intValue() == 0) {
            setActResult();
        }
        super.onBackPressed();
    }

    private void setActResult() {
        Intent intent = getIntent();
        intent.putExtra("position", getIntent().getExtras().getInt("position"));
        setResult(Activity.RESULT_OK, intent);
    }
}
