package com.sale_order.app.main.shopping.product.webview;

import android.content.Context;
import android.view.View;

import com.sale_order.app.utils.Constants;
import com.sale_order.app.utils.MySingleton;
import com.sale_order.app.utils.URLs;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentModel {
    private Context context;

    public PaymentModel(Context context) {
        this.context = context;
    }

    protected void disclosePayment(String paymentId, String orderNumber, boolean isPoints, final ModelCallback callback) {
        Call<Void> call = MySingleton.getInstance(context).createService().discloseAuction(
                isPoints ? URLs.DISCLOSE_POINTS : URLs.DISCLOSE_AUCTIONS,
                "Bearer " + MySingleton.getInstance(context).getStringFromSharedPref(Constants.USER_TOKEN, ""),
                paymentId, orderNumber);


        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                callback.setProgress(View.GONE);
                callback.handleResponse(response.code());

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.onFailure(t);
            }
        });

    }


    protected interface ModelCallback {
        void setProgress(int progress);

        void onFailure(Throwable t);

        void handleResponse(int code);
    }
}
