package com.sale_order.app.main;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.LayoutDirection;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.sale_order.app.R;
import com.sale_order.app.main.credit.CreditViewModel;
import com.sale_order.app.main.home.HomeFragment;
import com.sale_order.app.notifications.NotificationsActivity;
import com.sale_order.app.utils.Constants;
import com.sale_order.app.utils.Language;
import com.sale_order.app.utils.MySingleton;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity implements MainViewModel.ViewListener, BottomNavigationView.OnNavigationItemSelectedListener, HomeFragment.ParentListener {

    private NavigationView navigationView;
    private View navHeader;
    private DrawerLayout drawer;
    private BottomNavigationView bottomNavigation;
    private Toolbar toolbar;

    // tag for attached fragments
    protected static final String TAG_HOME = "home";//0
    protected static final String TAG_SHOPPING = "shopping";//1
    protected static final String TAG_CREDIT = "credit";//2
    protected static final String TAG_PREVIOUS_AUCTION = "preAuction";//3
    protected static final String TAG_CONTACT_US = "contact";//4

    // index to know the selected nav nav_menu item
    public int naveItemIndex = 0;

    // tag for the current attached fragment
    private static String CURRENT_TAG = TAG_HOME;

    // flag to make decision when user press back button
    private boolean shouldLoadHomeFragOnBackPress = false;

    private Handler mHandler;
    private Runnable runnable;

    private MainViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.arabic_key)));
        setContentView(R.layout.activity_main);

        viewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        viewModel.setViewListener(this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        bottomNavigation = findViewById(R.id.bottom_navigation);
        bottomNavigation.setOnNavigationItemSelectedListener(this);

        mHandler = new Handler();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navHeader = navigationView.getHeaderView(0);


        // load navigation drawer header view
        viewModel.loadHeader(navigationView, navHeader);
        // initialize navigation drawer
        setUpNavigation();

        viewModel.checkSavedInstanceState(savedInstanceState);
    }

    private ActionBarDrawerToggle actionBarDrawerToggle;

    private void setUpNavigation() {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                return viewModel.handleOnNavigationItemSelected(menuItem);

            }
        });

        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                FrameLayout frame = (FrameLayout) findViewById(R.id.frame_container);
                if (MySingleton.getInstance(MainActivity.this).getStringFromSharedPref(Constants.APP_LANGUAGE, getString(R.string.arabic_key)).equals(getString(R.string.arabic_key)))
                    frame.setX(-slideOffset * drawerView.getWidth());
                else frame.setX(slideOffset * drawerView.getWidth());
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.addDrawerListener(actionBarDrawerToggle);


        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }


    @Override
    public void loadHomeFragment() {
        getSupportActionBar().setTitle(getResources().getStringArray(R.array.nav_item_activity_titles)[naveItemIndex]);

        // close navigation drawer if user selected the current navigation item
        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            closeDrawer();
            return;
        }

        runnable = new Runnable() {
            @Override
            public void run() {
                Fragment fragment = viewModel.getSelectedFragment(naveItemIndex);
                if (fragment != null) {
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.setCustomAnimations(android.R.anim.fade_in,
                            android.R.anim.fade_out);
                    transaction.replace(R.id.frame, fragment);
                    transaction.commitAllowingStateLoss();
                }
            }
        };

        if (runnable != null)
            mHandler.post(runnable);

        // close navigation drawer
        closeDrawer();

        // refresh toolbar menu
        invalidateOptionsMenu();
    }

    @Override
    public boolean navigateToActivity(Class destination) {
        startActivity(new Intent(this, destination));
        closeDrawer();
        return true;
    }

    @Override
    public boolean handleLogoutAction(Class destination) {
        MySingleton.getInstance(this).logout();
        finish();
        startActivity(new Intent(this, destination));
        closeDrawer();
        return true;
    }

    @Override
    public void closeDrawer() {
        drawer.closeDrawers();
    }

    @Override
    public void handleNavFragments(int naveItemIndex, String currentTag, boolean shouldLoadHomeFragOnBackPress) {
        this.naveItemIndex = naveItemIndex;
        CURRENT_TAG = currentTag;
        this.shouldLoadHomeFragOnBackPress = shouldLoadHomeFragOnBackPress;
        if (shouldLoadHomeFragOnBackPress) {
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            actionBarDrawerToggle.setDrawerIndicatorEnabled(false);
        } else {
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
        }
    }

    @Override
    public boolean navigateToActivity1(Class destination) {
        startActivityForResult(new Intent(this, destination), 100);
        closeDrawer();
        return true;
    }


    @Override
    public void selectHome() {
        bottomNavigation.setSelectedItemId(R.id.bottom_nav_home);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBackPressed() {
        if (!viewModel.onBackBtnPressed(drawer.isDrawerOpen(GravityCompat.START), shouldLoadHomeFragOnBackPress))
            super.onBackPressed();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        return viewModel.handleOnNavigationItemSelected(menuItem);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == 100) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);

    }


    @Override
    protected void onRestart() {
        super.onRestart();
        viewModel.loadHeader(navigationView, navHeader);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (naveItemIndex == 0)
            getMenuInflater().inflate(R.menu.option_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.option_notification) {
            if (MySingleton.getInstance(this).isLoggedIn())
                return navigateToActivity(NotificationsActivity.class);

            showToastMessage(getString(R.string.you_should_login_first));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void navToPrevAuction() {
        bottomNavigation.setSelectedItemId(R.id.bottom_nav_prev_auction);
    }
}
