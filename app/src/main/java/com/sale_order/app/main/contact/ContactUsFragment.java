package com.sale_order.app.main.contact;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.sale_order.app.R;
import com.sale_order.app.classes.ContactObj;
import com.sale_order.app.databinding.FragmentContactUsBinding;
import com.sale_order.app.utils.Constants;
import com.sale_order.app.utils.Language;
import com.sale_order.app.utils.MySingleton;

public class ContactUsFragment extends Fragment implements ContactViewModel.ViewListener, Presenter {
    private ContactViewModel viewModel;
    private FragmentContactUsBinding dataBinding;

    public static ContactUsFragment newInstance() {
        ContactUsFragment fragment = new ContactUsFragment();

//        Bundle args = new Bundle();
//        fragment.setArguments(args);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Language.setLanguage(getContext(), MySingleton.getInstance(getContext()).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.arabic_key)));
        dataBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_contact_us, container, false);
        View view = dataBinding.getRoot();
        viewModel = ViewModelProviders.of(this).get(ContactViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);

        observeEditTexts();

        viewModel.getContactObjMutableLiveData().observe(this, new Observer<ContactObj>() {
            @Override
            public void onChanged(ContactObj contactObj) {
                dataBinding.contactMessageEditText.setText(contactObj.getMessage());
                dataBinding.contactEmailEditText.setText(contactObj.getEmail());
                dataBinding.contactPhoneEditText.setText(contactObj.getPhone());
                dataBinding.contactUsernameEditText.setText(contactObj.getUserName());
            }
        });

        return view;
    }

    private void observeEditTexts() {
        dataBinding.contactMessageEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                viewModel.setContactObjMutableLiveDataMessage(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        dataBinding.contactEmailEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                viewModel.setContactObjMutableLiveDataEmail(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        dataBinding.contactPhoneEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                viewModel.setContactObjMutableLiveDataPhone(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        dataBinding.contactUsernameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                viewModel.setContactObjMutableLiveDataUsername(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onSendButtonClicked(MutableLiveData<ContactObj> contact) {
        viewModel.requestContactUs(contact.getValue());
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }
}
