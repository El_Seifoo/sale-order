package com.sale_order.app.main.home;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.sale_order.app.R;
import com.sale_order.app.bidding.BiddingActivity;
import com.sale_order.app.classes.AuctionObj;
import com.sale_order.app.databinding.FragmentHomeBinding;
import com.sale_order.app.utils.Constants;
import com.sale_order.app.utils.Language;
import com.sale_order.app.utils.MySingleton;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class HomeFragment extends Fragment implements HomeAdapter.OnCurrentAuctionItemClicked, HomeViewModel.ViewListener {
    private HomeViewModel viewModel;
    private FragmentHomeBinding dataBinding;
    private HomeAdapter adapter;
    private ParentListener listener;

    Timer t = new Timer();
    private static final int DELAY_TIME = 7000;
    private int currentPage = 0;

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Language.setLanguage(getContext(), MySingleton.getInstance(getContext()).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.arabic_key)));
        dataBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_home, container, false);
        final View view = dataBinding.getRoot();
        viewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2);
        adapter = new HomeAdapter(this);
        gridLayoutManager.setSpanSizeLookup(new SpanSizeLookup(adapter));
        dataBinding.homeList.setLayoutManager(gridLayoutManager);
        dataBinding.homeList.setNestedScrollingEnabled(true);

        dataBinding.homeList.setAdapter(adapter);

        listener = (ParentListener) getActivity();

        viewModel.requestAuctions(0, 100,
                MySingleton.getInstance(getContext()).getStringFromSharedPref(Constants.APP_LANGUAGE, getString(R.string.english_key))).observe(this, new Observer<List<AuctionObj>>() {
            @Override
            public void onChanged(List<AuctionObj> auctionObjs) {
                adapter.setAuctions(auctionObjs);
            }
        });
        return view;
    }

    private final int AUCTION_DETAILS_REQUEST = 1;

    @Override
    public void onItemClickListener(String id) {
        Intent auctionIntent = new Intent(getContext(), BiddingActivity.class);
        auctionIntent.putExtra("AuctionID", id);
        startActivityForResult(auctionIntent, AUCTION_DETAILS_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == AUCTION_DETAILS_REQUEST && resultCode == Activity.RESULT_OK) {
            listener.navToPrevAuction();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        adapter.cancelCounter();
    }

    @Override
    public void setSlider(List<String> images, List<String> videos) {
        adapter.setSlider(images, videos);

    }

    private class SpanSizeLookup extends GridLayoutManager.SpanSizeLookup {
        private HomeAdapter adapter;

        public SpanSizeLookup(HomeAdapter adapter) {
            this.adapter = adapter;
        }

        @Override
        public int getSpanSize(int position) {
            return adapter.getItemViewType(position);
        }
    }

    public interface ParentListener {
        void navToPrevAuction();
    }
}
