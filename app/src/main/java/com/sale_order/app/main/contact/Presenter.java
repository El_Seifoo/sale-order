package com.sale_order.app.main.contact;

import androidx.lifecycle.MutableLiveData;

import com.sale_order.app.classes.ContactObj;

public interface Presenter {
    void onSendButtonClicked(MutableLiveData<ContactObj> contact);
}
