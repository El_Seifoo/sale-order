package com.sale_order.app.main.contact;

import android.app.Application;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.sale_order.app.R;
import com.sale_order.app.classes.ContactObj;
import com.sale_order.app.classes.UserInfoObj;
import com.sale_order.app.utils.MySingleton;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ContactViewModel extends AndroidViewModel implements ContactModel.ModelCallback {
    private Application application;
    private ContactModel model;
    private ViewListener viewListener;
    private ObservableField<Integer> progress;
    private ObservableField<Boolean> buttonsClickable;
    private MutableLiveData<ContactObj> contactObjMutableLiveData;

    public ContactViewModel(@NonNull Application application) {
        super(application);
        this.application = application;
        model = new ContactModel(application);
        progress = new ObservableField<>(View.GONE);
        buttonsClickable = new ObservableField<>(true);
    }

    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public MutableLiveData<ContactObj> getContactObjMutableLiveData() {
        if (contactObjMutableLiveData == null) {
            contactObjMutableLiveData = new MutableLiveData<>();
            setContactObjMutableLiveData();
        }
        return contactObjMutableLiveData;
    }

    public void setContactObjMutableLiveData() {
        if (MySingleton.getInstance(application).isLoggedIn()) {
            UserInfoObj user = MySingleton.getInstance(application).userData();
            contactObjMutableLiveData.setValue(new ContactObj("",
                    user.getPhoneNumber(),
                    user.getUserName(),
                    user.getEmail()));
        } else {
            contactObjMutableLiveData.setValue(new ContactObj("",
                    "",
                    "",
                    ""));
        }
    }

    public void setContactObjMutableLiveDataMessage(String message) {
        getContactObjMutableLiveData().getValue().setMessage(message);
    }

    public void setContactObjMutableLiveDataUsername(String username) {
        getContactObjMutableLiveData().getValue().setUserName(username);
    }

    public void setContactObjMutableLiveDataEmail(String email) {
        getContactObjMutableLiveData().getValue().setEmail(email);
    }

    public void setContactObjMutableLiveDataPhone(String phone) {
        getContactObjMutableLiveData().getValue().setPhone(phone);
    }


    public ObservableField<Boolean> getButtonsClickable() {
        return buttonsClickable;
    }

    @Override
    public void setButtonsClickable(boolean clickable) {
        this.buttonsClickable.set(clickable);
    }

    @Override
    public void onFailureHandler(Throwable t) {
        viewListener.showToastMessage(t instanceof IOException ? application.getString(R.string.no_internet_connection) :
                application.getString(R.string.something_went_wrong));
    }

    @Override
    public void handleSendingMessageResponse(String message) {
        if (message == null) {
            viewListener.showToastMessage(application.getString(R.string.something_went_wrong));
            return;
        }
        if (message.equals(application.getString(R.string.contact_us_response_message_sent))) {
            viewListener.showToastMessage(application.getString(R.string.thanks_our_team_will_check_ur_message));
            if (MySingleton.getInstance(application).isLoggedIn()) {
                setContactObjMutableLiveDataMessage("");
            } else {
                setContactObjMutableLiveDataEmail("");
                setContactObjMutableLiveDataUsername("");
                setContactObjMutableLiveDataPhone("");
                setContactObjMutableLiveDataMessage("");
            }
            return;
        }
        viewListener.showToastMessage(application.getString(R.string.something_went_wrong));
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }


    public void requestContactUs(ContactObj contact) {
        if (contact == null) {
            viewListener.showToastMessage(application.getString(R.string.all_fields_can_not_be_blank));
            return;
        }
        if (contact.getMessage().isEmpty()) {
            viewListener.showToastMessage(application.getString(R.string.message_can_not_be_blank));
            return;
        }

        if (contact.getEmail().isEmpty()) {
            viewListener.showToastMessage(application.getString(R.string.email_can_not_be_blank));
            return;
        }
        if (!checkMailValidation(contact.getEmail())) {
            viewListener.showToastMessage(application.getString(R.string.mail_not_valid));
            return;
        }
        if (contact.getPhone().isEmpty()) {
            viewListener.showToastMessage(application.getString(R.string.phone_can_not_be_blank));
            return;
        }
        if (contact.getUserName().isEmpty()) {
            viewListener.showToastMessage(application.getString(R.string.username_can_not_be_blank));
            return;
        }

        setProgress(View.VISIBLE);
        setButtonsClickable(false);
        model.sendMessage(contact, this);
    }

    private boolean checkMailValidation(String email) {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";
        CharSequence inputStr = email;
        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if (matcher.matches())
            return true;
        else
            return false;
    }

    protected interface ViewListener {

        void showToastMessage(String string);
    }
}
