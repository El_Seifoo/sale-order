package com.sale_order.app.main.shopping.product;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.sale_order.app.R;
import com.sale_order.app.classes.ShoppingObj;

import java.util.ArrayList;
import java.util.List;

public class SliderAdapter extends PagerAdapter {
    private LayoutInflater layoutInflater;
    private List<ShoppingObj.ImageObj> images;


    public void setImages(List<ShoppingObj.ImageObj> images) {
        if (images == null) {
            this.images = new ArrayList<>();
            this.images.add(new ShoppingObj.ImageObj("", ""));
            notifyDataSetChanged();
            return;
        }

        if (images.isEmpty()) {
            this.images = new ArrayList<>();
            this.images.add(new ShoppingObj.ImageObj("", ""));
            notifyDataSetChanged();
            return;
        }

        this.images = images;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) container.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.slider_item, container, false);

        ImageView imageViewPreview = (ImageView) view.findViewById(R.id.image_item);


        Glide.with(container.getContext())
                .load(images.get(position).getImageUrl())
                .thumbnail(0.5f)
                .error(R.mipmap.logo)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageViewPreview);

        container.addView(view);

        return view;
    }

    @Override
    public int getCount() {
        return images != null ? images.size() : 0;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((View) object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
