package com.sale_order.app.main.prevAuction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.sale_order.app.R;
import com.sale_order.app.classes.AuctionObj;
import com.sale_order.app.databinding.PrevAuctionListItemBinding;

import java.util.List;

public class PrevAuctionAdapter extends RecyclerView.Adapter<PrevAuctionAdapter.Holder> {
    private List<AuctionObj> auctions;
    private final onPrevAuctionItemClicked listener;

    public PrevAuctionAdapter(onPrevAuctionItemClicked listener) {
        this.listener = listener;
    }

    public void setAuctions(List<AuctionObj> auctions) {
        this.auctions = auctions;
        notifyDataSetChanged();
    }

    public void addAuctions(List<AuctionObj> auctions) {
        this.auctions.addAll(auctions);
        notifyDataSetChanged();
    }

    protected interface onPrevAuctionItemClicked {
        void onItemClicked(String id);
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder((PrevAuctionListItemBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.prev_auction_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        holder.binding.setAuction(auctions.get(position));
    }

    @Override
    public int getItemCount() {
        return auctions != null ? auctions.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private PrevAuctionListItemBinding binding;

        public Holder(@NonNull PrevAuctionListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.getRoot().setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onItemClicked(binding.getAuction().getId());
        }
    }
}