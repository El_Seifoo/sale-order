package com.sale_order.app.main.shopping;

import android.app.Application;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.sale_order.app.R;
import com.sale_order.app.classes.ShoppingObj;

import java.io.IOException;
import java.util.List;

public class ShoppingViewModel extends AndroidViewModel implements ShoppingModel.ModelCallback {
    private Application application;
    private ShoppingModel model;
    private ObservableField<Integer> progress;
    private ObservableField<Integer> emptyListTextView;
    private ObservableField<String> errorMessage;
    private ObservableField<Integer> errorView;

    public ShoppingViewModel(@NonNull Application application) {
        super(application);
        this.application = application;
        model = new ShoppingModel(application);
        progress = new ObservableField<>(View.GONE);
        emptyListTextView = new ObservableField<>(View.GONE);
        errorView = new ObservableField<>(View.GONE);
        errorMessage = new ObservableField<>("");
    }

    private int skip;
    private int take;
    private String language;

    protected MutableLiveData<List<ShoppingObj>> requestShoppingList(int skip, int take, String language) {
        this.skip = skip;
        this.take = take;
        this.language = language;
        setProgress(View.VISIBLE);
        return model.fetchShoppingList(skip, take, language, this);
    }

    public void onRetryClickListener(View view) {
        setProgress(View.VISIBLE);
        setErrorView(View.GONE);
        model.fetchShoppingList(skip, take, language, this);
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    public ObservableField<Integer> getEmptyListTextView() {
        return emptyListTextView;
    }

    public ObservableField<String> getErrorMessage() {
        return errorMessage;
    }

    protected void setErrorMessage(String message) {
        errorMessage.set(message);
    }

    public ObservableField<Integer> getErrorView() {
        return errorView;
    }

    protected void setErrorView(int errorViewStatus) {
        this.errorView.set(errorViewStatus);
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }

    @Override
    public void setEmptyListTextView(int empty) {
        this.emptyListTextView.set(empty);
    }

    @Override
    public void onFailureHandler(Throwable t) {
        setErrorView(View.VISIBLE);
        Log.e("error", t.toString());
        if (t instanceof IOException)
            setErrorMessage(application.getString(R.string.no_internet_connection));
        else
            setErrorMessage(application.getString(R.string.error_fetching_data));
    }

}
