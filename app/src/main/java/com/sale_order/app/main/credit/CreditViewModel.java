package com.sale_order.app.main.credit;

import android.app.Application;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.sale_order.app.R;
import com.sale_order.app.classes.CreditObj;
import com.sale_order.app.classes.OrderObj;
import com.sale_order.app.classes.OrderResponse;
import com.sale_order.app.classes.PaymentResponse;
import com.sale_order.app.main.MainActivity;
import com.sale_order.app.utils.MySingleton;

import java.io.IOException;
import java.util.List;

import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class CreditViewModel extends AndroidViewModel implements CreditModel.ModelCallback {
    private Application application;
    private CreditModel model;
    private ViewListener viewListener;
    private ObservableField<Integer> progress;
    private ObservableField<Integer> emptyListTextView;
    private ObservableField<String> errorMessage;
    private ObservableField<Integer> errorView;
//    private ObservableField<Boolean> dialogStatus;
//    private ObservableField<Integer> paymentMethod;
//    private ObservableField<Integer> points;

    public CreditViewModel(@NonNull Application application) {
        super(application);
        this.application = application;
        model = new CreditModel(application);
        progress = new ObservableField<>(View.GONE);
        emptyListTextView = new ObservableField<>(View.GONE);
        errorView = new ObservableField<>(View.GONE);
        errorMessage = new ObservableField<>("");
//        dialogStatus = new ObservableField<>(false);
//        paymentMethod = new ObservableField<>(R.id.k_net_payment);
//        points = new ObservableField<>(0);
    }


    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public void onRetryClickListener(View view) {
        setProgress(View.VISIBLE);
        setErrorView(View.GONE);
        model.fetchCredits(skip, take, language, this);
    }

    private int skip;
    private int take;
    private String language;

    protected MutableLiveData<List<CreditObj>> requestAllCredits(int skip, int take, String language) {
        this.skip = skip;
        this.take = take;
        this.language = language;
        setProgress(View.VISIBLE);
        return model.fetchCredits(skip, take, language, this);
    }

    /*
        public ObservableField<Boolean> getDialogStatus() {
            return dialogStatus;
        }

        protected void checkDialogStatus() {
            if (getDialogStatus().get())
                viewListener.showDialog();
        }

        public void setDialogStatus(boolean dialogStatus) {
            this.dialogStatus.set(dialogStatus);
        }

        public ObservableField<Integer> getPoints() {
            return points;
        }
              public void setPoints(int points) {
            this.points.set(points);
        }

        public ObservableField<Integer> getPaymentMethod() {
            return paymentMethod;
        }

        public void setPaymentMethod(int paymentMethod) {
            this.paymentMethod.set(paymentMethod);
        }

        @Override
        public void handleCheckingPaymentResponse(PaymentResponse paymentResponse) {
            if (paymentResponse.getMessage() != null) {
                if (paymentResponse.getMessage().equals(application.getString(R.string.check_payment_response_status_failed_1)))
                    viewListener.showToastMessage(application.getString(R.string.failed_to_buy));
                else
                    viewListener.showToastMessage(application.getString(R.string.payment_done_check_your_points));
                return;
            }
            if (paymentResponse.getOrderPayment().getStatus().equals(application.getString(R.string.check_payment_response_status_failed))) {
                viewListener.showToastMessage(application.getString(R.string.failed_to_buy));
                return;
            }

            viewListener.showToastMessage(application.getString(R.string.payment_done_check_your_points));
        }

        protected void requestCheckPayment(String orderId) {
            setProgress(View.VISIBLE);
            model.checkPaymentStatus(orderId, this);
        }
    */

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    public ObservableField<Integer> getEmptyListTextView() {
        return emptyListTextView;
    }

    public ObservableField<String> getErrorMessage() {
        return errorMessage;
    }

    protected void setErrorMessage(String message) {
        errorMessage.set(message);
    }

    public ObservableField<Integer> getErrorView() {
        return errorView;
    }

    protected void setErrorView(int errorViewStatus) {
        this.errorView.set(errorViewStatus);
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }

    @Override
    public void setEmptyListTextView(int empty) {
        this.emptyListTextView.set(empty);
    }

    @Override
    public void onFailureHandler(Throwable t, int index) {
        if (index == 0) {
            setErrorView(View.VISIBLE);
            Log.e("error", t.toString());
            if (t instanceof IOException)
                setErrorMessage(application.getString(R.string.no_internet_connection));
            else
                setErrorMessage(application.getString(R.string.error_fetching_data));
        } else
            viewListener.showToastMessage(t instanceof IOException
                    ? application.getString(R.string.no_internet_connection)
                    : application.getString(R.string.error_fetching_data));

    }

    @Override
    public void handleBuyPointsResponse(Response<OrderResponse> response) {
        if (response == null){
            viewListener.showToastMessage(application.getString(R.string.something_went_wrong));
            return;
        }
        OrderResponse orderResponse = response.body();
        OrderObj order = orderResponse.getOrder();
        String message = orderResponse.getMessage();
        if (order != null) {
            if (message.equals(getApplication().getString(R.string.start_buying_points_response_success)))
                viewListener.startWebView(order);
            else viewListener.showToastMessage(order.getErrorMessage());
        } else {
            if (message != null)
                viewListener.showToastMessage(getApplication().getString(R.string.something_went_wrong));
            else if (message.isEmpty())
                viewListener.showToastMessage(getApplication().getString(R.string.something_went_wrong));
            else viewListener.showToastMessage(message);
        }
    }


    @Override
    public void loginFirst() {
        MySingleton.getInstance(getApplication()).logout();
        viewListener.loginFirst();
    }


    public void requestBuyPoints(String packageId) {
        if (!MySingleton.getInstance(getApplication()).isLoggedIn()) {
            this.loginFirst();
            return;
        }
        setProgress(View.VISIBLE);
//        setButtonsClickable(false);
        model.buyPoints(packageId, this);
    }



    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CreditFragment.WEB_VIEW_REQUEST_CODE) {
            if (!data.getExtras().getBoolean("done"))
                viewListener.showToastMessage(getApplication().getString(R.string.failed_to_buy));
        }

    }

    protected interface ViewListener {
//        void showDialog();

        void showToastMessage(String message);

        void startWebView(OrderObj response);

        void loginFirst();
    }


}
