package com.sale_order.app.main.home;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.sale_order.app.R;
import com.sale_order.app.classes.ShoppingObj;
import com.sale_order.app.classes.SliderObj;

import java.util.ArrayList;
import java.util.List;

public class SliderAdapter extends PagerAdapter {
    private LayoutInflater layoutInflater;
    private List<String> images;
    private List<String> videos;


    public void setImages(List<String> images, List<String> videos) {
        if (images == null) {
            this.images = new ArrayList<>();
            this.videos = new ArrayList<>();
            this.images.add("");
            this.videos.add("");
            notifyDataSetChanged();
            return;
        }

        if (images.isEmpty()) {
            this.images = new ArrayList<>();
            this.videos = new ArrayList<>();
            this.images.add("");
            this.videos.add("");
            notifyDataSetChanged();
            return;
        }

        this.images = images;
        this.videos = videos;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull final ViewGroup container, final int position) {
        layoutInflater = (LayoutInflater) container.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(videos.isEmpty() ? R.layout.slider_item : R.layout.main_slider_image_item, container, false);

        ImageView imageViewPreview = (ImageView) view.findViewById(R.id.image_item);


        Glide.with(container.getContext())
                .load(images.get(position))
                .thumbnail(0.5f)
                .error(R.mipmap.logo)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageViewPreview);

        if (!videos.isEmpty())
            if (!videos.get(position).isEmpty()) {
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(videos.get(position)));
                        container.getContext().startActivity(intent);
                    }
                });
            }
        container.addView(view);

        return view;
    }

    @Override
    public int getCount() {
        return images != null ? images.size() : 0;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((View) object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
