package com.sale_order.app.main.credit.custom;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.sale_order.app.R;
import com.sale_order.app.classes.OrderObj;
import com.sale_order.app.databinding.ActivityCustomPointsBinding;
import com.sale_order.app.login.LoginActivity;
import com.sale_order.app.main.MainActivity;
import com.sale_order.app.main.shopping.product.webview.PaymentActivity;

public class CustomPointsActivity extends AppCompatActivity implements CustomPointsViewModel.ViewListener, Presenter {
    protected static final int LOGIN_FIRST_REQUEST_CODE = 1;
    protected static final int WEB_VIEW_REQUEST_CODE = 2;
    private CustomPointsViewModel viewModel;
    private ActivityCustomPointsBinding dataBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_custom_points);
        viewModel = ViewModelProviders.of(this).get(CustomPointsViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);

        getSupportActionBar().setTitle(getString(R.string.points));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//        createDialog();
    }

    /*
        Dialog dialog;

        private void createDialog() {
            dialog = new Dialog(this);

            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialog.setContentView(R.layout.payment_method_dialog_view);
            dialog.getWindow().setLayout((int) (getResources().getDisplayMetrics().widthPixels * 0.70), WindowManager.LayoutParams.WRAP_CONTENT);

            TextView pay = dialog.findViewById(R.id.pay_button);
            pay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    viewModel.requestBuyPoints();
                    dialog.dismiss();
                }
            });

            RadioGroup paymentGroup = dialog.findViewById(R.id.radio_group);
            paymentGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    viewModel.setPaymentMethod(checkedId);
                }
            });

            paymentGroup.check(viewModel.getPaymentMethod().get());


            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    viewModel.setDialogStatus(false);
                }
            });

            dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    viewModel.setDialogStatus(true);
                }
            });
            viewModel.checkDialogStatus();
        }

        @Override
        public void showDialog() {
            dialog.show();
        }
    */

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void startWebView(OrderObj response) {
        Intent intent = new Intent(this, PaymentActivity.class);
        intent.putExtra("order", response);
        intent.putExtra("isPoints",false);
        startActivityForResult(intent, WEB_VIEW_REQUEST_CODE);
    }

    @Override
    public void loginFirst() {
        Intent loginIntent = new Intent(this, LoginActivity.class);
        loginIntent.putExtra("CustomPointsActivity", "");
        startActivityForResult(loginIntent, LOGIN_FIRST_REQUEST_CODE);
    }

    @Override
    public void paymentDone() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onNextButtonClicked() {
        viewModel.checkPoints();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        viewModel.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
