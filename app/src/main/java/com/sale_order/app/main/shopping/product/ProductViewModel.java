package com.sale_order.app.main.shopping.product;

import android.app.Application;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.sale_order.app.R;
import com.sale_order.app.classes.OrderObj;
import com.sale_order.app.classes.ShoppingObj;
import com.sale_order.app.classes.UserInfoObj;
import com.sale_order.app.utils.MySingleton;

import java.io.IOException;

import static android.app.Activity.RESULT_OK;


public class ProductViewModel extends AndroidViewModel implements ProductModel.ModelCallback {
    private Application application;
    private ProductModel model;
    private ViewListener viewListener;
    private ObservableField<Integer> progress;
    private ObservableField<String> errorMessage;
    private ObservableField<Integer> errorView;
    private ObservableField<Boolean> buttonsClickable;
    private ObservableField<Integer> quantityField;
    private ObservableField<Integer> viewPagerCurrentPosition;
    private ObservableField<Integer> productQuantity;

    public ProductViewModel(@NonNull Application application) {
        super(application);
        this.application = application;
        model = new ProductModel(application);
        progress = new ObservableField<>(View.GONE);
        errorView = new ObservableField<>(View.GONE);
        errorMessage = new ObservableField<>("");
        buttonsClickable = new ObservableField<>(false);
        quantityField = new ObservableField<>(1);
        viewPagerCurrentPosition = new ObservableField<>(0);
        productQuantity = new ObservableField<>(0);
    }

    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    private String productId;

    protected MutableLiveData<ShoppingObj> requestProduct(String productId) {
        this.productId = productId;
        setProgress(View.VISIBLE);
        setButtonsClickable(false);
        return model.fetchProduct(productId, this);
    }

    public void onRetryClickListener(View view) {
        setProgress(View.VISIBLE);
        setErrorView(View.GONE);
        setButtonsClickable(false);
        model.fetchProduct(productId, this);
    }


    public void requestMakeOrder(String productId) {
        if (getProductQuantity().get().intValue() == 0) {
            viewListener.showToastMessage(getApplication().getString(R.string.sorry_product_sold_out));
            return;
        }
        if (!MySingleton.getInstance(getApplication()).isLoggedIn()) {
            this.loginFirst();
            return;
        }
        setProgress(View.VISIBLE);
        setButtonsClickable(false);
        model.makeOrder(productId, getQuantityField().get(), this);
    }


    public ObservableField<Integer> getViewPagerCurrentPosition() {
        return viewPagerCurrentPosition;
    }

    public void setViewPagerCurrentPosition(int viewPagerCurrentPosition) {
        this.viewPagerCurrentPosition.set(viewPagerCurrentPosition);
    }

    public ObservableField<Integer> getQuantityField() {
        return quantityField;
    }

    public void setQuantityField(int quantity) {
        if (getProductQuantity().get().intValue() == 0) {
            viewListener.showToastMessage(getApplication().getString(R.string.sorry_product_sold_out));
            this.quantityField.set(1);// reset the quantity picked by user
            return;
        }

        if (quantity == -1 && quantityField.get() > 1)
            this.quantityField.set(quantityField.get() + quantity);
        else if (quantity == 1 && quantityField.get() < productQuantity.get())
            this.quantityField.set(quantityField.get() + quantity);
        else if (quantity == 0) {// zero means reset the quantity picked by user
            this.quantityField.set(1);
        }
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    public ObservableField<String> getErrorMessage() {
        return errorMessage;
    }

    protected void setErrorMessage(String message) {
        errorMessage.set(message);
    }

    public ObservableField<Integer> getErrorView() {
        return errorView;
    }

    protected void setErrorView(int errorViewStatus) {
        this.errorView.set(errorViewStatus);
    }

    public ObservableField<Boolean> getButtonsClickable() {
        return buttonsClickable;
    }

    public ObservableField<Integer> getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(int productQuantity) {
        this.productQuantity.set(productQuantity);
    }

    @Override
    public void setButtonsClickable(boolean clickable) {
        this.buttonsClickable.set(clickable);
    }

    @Override
    public void onFailureHandler(Throwable t, int index) {
        if (index == 0) {
            setErrorView(View.VISIBLE);
            Log.e("error", t.toString());
            if (t instanceof IOException)
                setErrorMessage(application.getString(R.string.no_internet_connection));
            else
                setErrorMessage(application.getString(R.string.error_fetching_data));
        } else viewListener.showToastMessage(t instanceof IOException
                ? application.getString(R.string.no_internet_connection)
                : application.getString(R.string.error_fetching_data));

    }

    @Override
    public void handleMakeOrderResponse(String message) {
        if (message.equals(getApplication().getString(R.string.shopping_response_success))) {
            viewListener.BuyingDone();
            return;
        }
        viewListener.showToastMessage(message.equals(getApplication().getString(R.string.shopping_response_points_not_enough))
                ? getApplication().getString(R.string.points_not_enough_shopping)
                : message.equals(getApplication().getString(R.string.shopping_response_quantity_not_available)) ?
                getApplication().getString(R.string.required_quantity_not_available)
                : getApplication().getString(R.string.something_went_wrong));
    }


    @Override
    public void loginFirst() {
        MySingleton.getInstance(getApplication()).logout();
        viewListener.loginFirst();
    }

    @Override
    public void handleEmptyProduct() {
        viewListener.emptyProduct();
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == ProductActivity.LOGIN_FIRST_REQUEST_CODE && resultCode == RESULT_OK) {
//            model.addCurrentUser(MySingleton.getInstance(getApplication()).userData());
//        }
    }

    protected interface ViewListener {
        void showToastMessage(String message);

        void BuyingDone();

        void loginFirst();

        void emptyProduct();
    }

}
