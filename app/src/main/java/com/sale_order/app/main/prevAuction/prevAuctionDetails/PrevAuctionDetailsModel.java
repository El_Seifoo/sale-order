package com.sale_order.app.main.prevAuction.prevAuctionDetails;

import android.content.Context;
import android.view.View;

import androidx.lifecycle.MutableLiveData;

import com.sale_order.app.classes.AuctionObj;
import com.sale_order.app.classes.AuctionResponse;
import com.sale_order.app.classes.OrderObj;
import com.sale_order.app.classes.OrderResponse;
import com.sale_order.app.classes.PaymentResponse;
import com.sale_order.app.classes.UserInfoObj;
import com.sale_order.app.utils.Constants;
import com.sale_order.app.utils.MySingleton;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PrevAuctionDetailsModel {
    private Context context;

    public PrevAuctionDetailsModel(Context context) {
        this.context = context;
    }

    private MutableLiveData<AuctionObj> auctionObjMutableLiveData = new MutableLiveData<>();

    protected void addCurrentUser(UserInfoObj user) {
        auctionObjMutableLiveData.getValue().setCurrentUser(user);
    }

    protected MutableLiveData<AuctionObj> fetchCertainAuction(String auctionId, final ModelCallback callback) {
        Call<AuctionResponse> call = MySingleton.getInstance(context).createService().certainAuction(
                MySingleton.getInstance(context).getStringFromSharedPref(Constants.USER_TOKEN, ""), auctionId);
        call.enqueue(new Callback<AuctionResponse>() {
            @Override
            public void onResponse(Call<AuctionResponse> call, Response<AuctionResponse> response) {
                callback.setProgress(View.GONE);
                callback.setButtonClickable(true);

                AuctionObj auctionObj = response.body().getAuctionObj();
                if (auctionObj.getBiddersInAuctions() == null)
                    callback.setEmptyListTextView(View.VISIBLE);
                else if (auctionObj.getBiddersInAuctions().isEmpty())
                    callback.setEmptyListTextView(View.VISIBLE);
                else callback.setEmptyListTextView(View.GONE);
                callback.setHighestBidderPrice(0.0);
                if (!auctionObj.getBiddersInAuctions().isEmpty()) {
                    int size = auctionObj.getBiddersInAuctions().size();
                    callback.setHighestBidderPrice(auctionObj.getBiddersInAuctions().get(0).getPrice());
                }
                auctionObjMutableLiveData.setValue(auctionObj);
            }

            @Override
            public void onFailure(Call<AuctionResponse> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.setButtonClickable(true);
                callback.onFailureHandler(t, 0);
            }
        });

        return auctionObjMutableLiveData;
    }

    /*
        public void payAuction(String productId, final ModelCallback callback) {
            Call<OrderResponse> call = MySingleton.getInstance(context).createService().payAuction(
                    "Bearer " + MySingleton.getInstance(context).getStringFromSharedPref(Constants.USER_TOKEN, ""),
                    productId, false);

            call.enqueue(new Callback<OrderResponse>() {
                @Override
                public void onResponse(Call<OrderResponse> call, Response<OrderResponse> response) {
                    callback.setProgress(View.GONE);
                    callback.setButtonClickable(true);
                    if (response.code() == 401) {
                        callback.loginFirst();
                        return;
                    }
                    if (response.code() == 200) {
                        callback.handlePayAuctionResponse(response);
                        return;
                    }

                    callback.handlePayAuctionResponse(null);
                }

                @Override
                public void onFailure(Call<OrderResponse> call, Throwable t) {
                    callback.setProgress(View.GONE);
                    callback.setButtonClickable(true);
                    callback.onFailureHandler(t, 1);
                }
            });
        }

        public void checkPaymentStatus(String orderId, final ModelCallback callback) {
        Call<PaymentResponse> call = MySingleton.getInstance(context).createService().checkAuctionPayment(
                "Bearer " + MySingleton.getInstance(context).getStringFromSharedPref(Constants.USER_TOKEN, ""), orderId);
        call.enqueue(new Callback<PaymentResponse>() {
            @Override
            public void onResponse(Call<PaymentResponse> call, Response<PaymentResponse> response) {
                callback.setProgress(View.GONE);
                callback.setButtonClickable(true);
                callback.handleCheckingPaymentResponse(response.body());
            }

            @Override
            public void onFailure(Call<PaymentResponse> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.setButtonClickable(true);
                callback.onFailureHandler(t, 1);
            }
        });
    }
    */

    protected interface ModelCallback {
        void setProgress(int progress);

        void setEmptyListTextView(int empty);

        void setButtonClickable(boolean clickable);

        void onFailureHandler(Throwable t, int index);

//        void handlePayAuctionResponse(Response<OrderResponse> response);

//        void handleCheckingPaymentResponse(PaymentResponse response);

        void loginFirst();

        void setHighestBidderPrice(double price);
    }
}
