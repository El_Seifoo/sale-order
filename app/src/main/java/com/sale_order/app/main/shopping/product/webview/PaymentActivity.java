package com.sale_order.app.main.shopping.product.webview;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.sale_order.app.R;
import com.sale_order.app.classes.OrderObj;
import com.sale_order.app.databinding.ActivityPaymentBinding;
import com.sale_order.app.utils.Constants;
import com.sale_order.app.utils.Language;
import com.sale_order.app.utils.MySingleton;

public class PaymentActivity extends AppCompatActivity implements PaymentViewModel.ViewListener {
    private WebView webView;
    private ActivityPaymentBinding dataBinding;
    private PaymentViewModel viewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.arabic_key)));
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_payment);
        viewModel = ViewModelProviders.of(this).get(PaymentViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);

        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final OrderObj order = (OrderObj) getIntent().getSerializableExtra("order");
        final boolean isPoints = getIntent().getExtras().getBoolean("isPoints");

        webView = dataBinding.webView;
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {

                if (url.contains(order.getRedirectUrl())) {
                    showToastMessage(getString(R.string.please_w8_until_this_close_automatically));
                    Uri uri = Uri.parse(url);
                    String paymentId = uri.getQueryParameter("paymentId");
                    if (paymentId != null) {
                        viewModel.disclose(paymentId, order.getOrderNumber(), isPoints);
                    }
                }
                super.onPageFinished(view, url);
            }
        });
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(order.getUrl());


    }


    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
            return;
        }
        Intent returnIntent = new Intent();
        returnIntent.putExtra("done", viewModel.getPaymentStatus());
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Intent returnIntent = new Intent();
            returnIntent.putExtra("done", viewModel.getPaymentStatus());
            setResult(RESULT_OK, returnIntent);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void paymentDone(String string) {
        showToastMessage(string);
        Intent returnIntent = new Intent();
        returnIntent.putExtra("done", true);
        setResult(RESULT_OK, returnIntent);
        finish();
    }
}
