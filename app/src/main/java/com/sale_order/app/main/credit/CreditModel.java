package com.sale_order.app.main.credit;

import android.content.Context;
import android.view.View;

import androidx.lifecycle.MutableLiveData;

import com.sale_order.app.R;
import com.sale_order.app.classes.CreditObj;
import com.sale_order.app.classes.CreditResponse;
import com.sale_order.app.classes.OrderObj;
import com.sale_order.app.classes.OrderResponse;
import com.sale_order.app.classes.PaymentResponse;
import com.sale_order.app.utils.Constants;
import com.sale_order.app.utils.MySingleton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreditModel {
    private Context context;

    public CreditModel(Context context) {
        this.context = context;
    }

    private MutableLiveData<List<CreditObj>> creditMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<List<CreditObj>> fetchCredits(final int skip, int take, String language, final ModelCallback callback) {
        Map<String, Object> map = new HashMap<>();
        map.put("skip", skip);
        map.put("take", take);
        if (language.equals(context.getString(R.string.english_key)))
            map.put("Ln", language);

        Call<CreditResponse> call = MySingleton.getInstance(context).createService().credits(map);

        call.enqueue(new Callback<CreditResponse>() {
            @Override
            public void onResponse(Call<CreditResponse> call, Response<CreditResponse> response) {
                callback.setProgress(View.GONE);

                if (response.body() != null) {
                    List<CreditObj> credits = response.body().getCredits();
                    if (skip == 0 && credits == null) {
                        callback.setEmptyListTextView(View.VISIBLE);
                        return;
                    }
                    if (skip == 0 && credits.isEmpty()) {
                        callback.setEmptyListTextView(View.VISIBLE);
                        return;
                    }
                    callback.setEmptyListTextView(View.GONE);
                    creditMutableLiveData.setValue(credits);
                }
            }

            @Override
            public void onFailure(Call<CreditResponse> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.onFailureHandler(t, 0);
            }
        });

        return creditMutableLiveData;
    }

    public void buyPoints(String packageId, final ModelCallback callback) {
        Call<OrderResponse> call = MySingleton.getInstance(context).createService().buyPointsByPackage(
                "Bearer " + MySingleton.getInstance(context).getStringFromSharedPref(Constants.USER_TOKEN, ""),
                packageId);

        call.enqueue(new Callback<OrderResponse>() {
            @Override
            public void onResponse(Call<OrderResponse> call, Response<OrderResponse> response) {
                callback.setProgress(View.GONE);
//                callback.setButtonsClickable(true);
                if (response.code() == 401) {
                    callback.loginFirst();
                    return;
                }
                if (response.code() == 200) {
                    callback.handleBuyPointsResponse(response);
                    return;
                }

                callback.handleBuyPointsResponse(null);
            }

            @Override
            public void onFailure(Call<OrderResponse> call, Throwable t) {
                callback.setProgress(View.GONE);
//                callback.setButtonsClickable(true);
                callback.onFailureHandler(t, 1);
            }
        });
    }

    /*
        protected void checkPaymentStatus(String orderId, final ModelCallback callback) {
        Call<PaymentResponse> call = MySingleton.getInstance(context).createService().checkPayment(
                MySingleton.getInstance(context).getStringFromSharedPref(Constants.USER_TOKEN, ""),
                orderId);
        call.enqueue(new Callback<PaymentResponse>() {
            @Override
            public void onResponse(Call<PaymentResponse> call, Response<PaymentResponse> response) {
                callback.setProgress(View.GONE);
                callback.handleCheckingPaymentResponse(response.body());
            }

            @Override
            public void onFailure(Call<PaymentResponse> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.onFailureHandler(t, 1);

            }
        });
    }
    */

    protected interface ModelCallback {
        void setProgress(int progress);

        void setEmptyListTextView(int empty);

        void onFailureHandler(Throwable t, int index);

        void handleBuyPointsResponse(Response<OrderResponse> response);

//        void handleCheckingPaymentResponse(PaymentResponse body);

        void loginFirst();
    }
}
