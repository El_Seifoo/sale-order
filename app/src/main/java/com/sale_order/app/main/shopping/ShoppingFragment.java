package com.sale_order.app.main.shopping;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sale_order.app.R;
import com.sale_order.app.classes.ShoppingObj;
import com.sale_order.app.databinding.FragmentShoppingBinding;
import com.sale_order.app.main.shopping.product.ProductActivity;
import com.sale_order.app.utils.Constants;
import com.sale_order.app.utils.Language;
import com.sale_order.app.utils.MySingleton;

import java.util.List;

public class ShoppingFragment extends Fragment implements ShoppingAdapter.OnProductClicked {
    private static final int VIEW_PRODUCT_REQUEST = 1;
    private ShoppingViewModel viewModel;
    private FragmentShoppingBinding dataBinding;
    private ShoppingAdapter adapter;

    public static ShoppingFragment newInstance() {
        return new ShoppingFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Language.setLanguage(getContext(), MySingleton.getInstance(getContext()).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.arabic_key)));
        dataBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_shopping, container, false);
        View view = dataBinding.getRoot();
        viewModel = ViewModelProviders.of(this).get(ShoppingViewModel.class);
        dataBinding.setViewModel(viewModel);

        dataBinding.shoppingList.setLayoutManager(new GridLayoutManager(getContext(), 2));
        adapter = new ShoppingAdapter(this);
        dataBinding.shoppingList.setAdapter(adapter);


        viewModel.requestShoppingList(0, 100,
                MySingleton.getInstance(getContext()).getStringFromSharedPref(Constants.APP_LANGUAGE, getString(R.string.english_key))).observe(this, new Observer<List<ShoppingObj>>() {
            @Override
            public void onChanged(List<ShoppingObj> shoppingObjs) {
                adapter.setShoppingList(shoppingObjs);
            }
        });
        return view;
    }

    @Override
    public void onProductClickListener(String productId, int position) {
        Intent productIntent = new Intent(getContext(), ProductActivity.class);
        productIntent.putExtra("productId", productId);
        productIntent.putExtra("position", position);
        startActivityForResult(productIntent, VIEW_PRODUCT_REQUEST);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == VIEW_PRODUCT_REQUEST && resultCode == Activity.RESULT_OK && data != null) {
            if (adapter.getItemCount() == 1)
                viewModel.setEmptyListTextView(View.VISIBLE);
            adapter.removeItemByPosition(getContext(), Integer.valueOf(data.getExtras().getInt("position")));
        }
    }
}
