package com.sale_order.app.main.home;

import android.content.Context;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.sale_order.app.R;
import com.sale_order.app.classes.AuctionObj;
import com.sale_order.app.classes.TimerObj;
import com.sale_order.app.databinding.CurrentAuctionListItemBinding;
import com.sale_order.app.utils.Constants;
import com.sale_order.app.utils.MySingleton;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class HomeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final int TYPE_HEADER = 2;
    public static final int TYPE_NORMAL = 1;
    private List<AuctionObj> auctions;
    private List<String> images = new ArrayList<>(), videos = new ArrayList<>();
    private final OnCurrentAuctionItemClicked listener;
    private CountDownTimer countDownTimer;


    public HomeAdapter(OnCurrentAuctionItemClicked listener) {
        this.listener = listener;
    }

    public void setSlider(List<String> images, List<String> videos) {
        if (images == null) {
            this.images = new ArrayList<>();
            this.videos = new ArrayList<>();
            this.images.add("");
            this.videos.add("");
            notifyDataSetChanged();
        }

        if (images.isEmpty()) {
            this.images = new ArrayList<>();
            this.videos = new ArrayList<>();
            this.images.add("");
            this.videos.add("");
            notifyDataSetChanged();
            return;

        }

        this.images = images;
        this.videos = videos;
        notifyDataSetChanged();
        sliderAdapter = new SliderAdapter();
        sliderAdapter.setImages(images, videos);

    }

    protected interface OnCurrentAuctionItemClicked {
        void onItemClickListener(String auctionId);
    }

    long maxTime = 0;

    public void setAuctions(List<AuctionObj> auctions) {
        TimerObj timer;
        for (int i = 0; i < auctions.size(); i++) {
            timer = auctions.get(i).getTimer();
            if (timerConverter(timer.getDays(), timer.getHours(), timer.getMinutes(), timer.getSeconds()) > maxTime) {
                maxTime = timerConverter(timer.getDays(), timer.getHours(), timer.getMinutes(), timer.getSeconds()) + (86400000l);
            }
        }
        startCounter(maxTime);
        this.auctions = auctions;
        notifyDataSetChanged();
    }

    public void addAuctions(List<AuctionObj> auctions) {
        this.auctions.addAll(auctions);
        notifyDataSetChanged();
    }

    int counter = 0;

    private void startCounter(long millis) {
        countDownTimer = new CountDownTimer(millis, 1000) {
            public void onTick(long millisUntilFinished) {

                counter++;
                if (counter == 15) {
                    counter = 0;
                    if (images != null) {
                        if (HeaderHolder.viewPager != null) {
                            currentPage = HeaderHolder.viewPager.getCurrentItem();
                            if (currentPage != sliderAdapter.getCount() - 1) {
                                HeaderHolder.viewPager.setCurrentItem(++currentPage);
                            } else {
                                currentPage = 0;
                                HeaderHolder.viewPager.setCurrentItem(currentPage);
                            }
                        }
                    }
                }

                long timeLong;
                TimerObj timerObj;
                for (int i = 0; i < auctions.size(); i++) {
                    timerObj = auctions.get(i).getTimer();
                    timeLong = timerConverter(timerObj.getDays(), timerObj.getHours(), timerObj.getMinutes(), timerObj.getSeconds()) - 1000;
                    int second = (int) (timeLong / 1000) % 60;
                    int minute = (int) (timeLong / (1000 * 60)) % 60;
                    int hour = (int) (timeLong / (1000 * 60 * 60)) % 24;
                    int days = (int) (timeLong / (1000 * 60 * 60 * 24));

                    auctions.get(i).setTimer(new TimerObj(days, hour, minute, second, auctions.get(i).getTimer().getStartIn(), auctions.get(i).getTimer().getEndIn()));

                }

            }

            public void onFinish() {
            }

        };
        countDownTimer.start();
    }

    private long timerConverter(int days, int hours, int minutes, int seconds) {
        long seconds1 = days * (60 * 60 * 24);
        long seconds2 = (hours) * (60 * 60);
        long seconds3 = (minutes) * (60);
        return ((seconds1 + seconds2 + seconds3 + seconds) * 1000);
    }

    public void cancelCounter() {
        if (countDownTimer != null)
            countDownTimer.cancel();

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER)
            return new HeaderHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.main_slider_item, parent, false));
        else
            return new Holder((CurrentAuctionListItemBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                    R.layout.current_auction_list_item, parent, false));

    }

    private SliderAdapter sliderAdapter;
    private int currentPage = 0;

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof HeaderHolder)
            ((HeaderHolder) holder).viewPager.setAdapter(sliderAdapter);
        else
            ((Holder) holder).binding.setAuction(auctions.get(position - 1));

    }


    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return TYPE_HEADER;
        else
            return TYPE_NORMAL;
    }


    @Override
    public int getItemCount() {
        if (auctions == null && images.isEmpty()) return 0;
        if (auctions == null && !images.isEmpty()) return 1;
        if (auctions != null && !images.isEmpty()) return auctions.size() + 1;
        return auctions.size();
    }


    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private CurrentAuctionListItemBinding binding;

        public Holder(@NonNull CurrentAuctionListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.getRoot().setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onItemClickListener(binding.getAuction().getId());
        }

    }

    public static class HeaderHolder extends RecyclerView.ViewHolder {
        public static ViewPager viewPager;

        public HeaderHolder(@NonNull View itemView) {
            super(itemView);
            viewPager = itemView.findViewById(R.id.view_pager);
        }

    }


}