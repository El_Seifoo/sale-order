package com.sale_order.app.settings;

public interface Presenter {
    void onLanguageButtonClicked(boolean english);
}
