package com.sale_order.app.settings;

import android.app.Application;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;

import com.sale_order.app.R;
import com.sale_order.app.utils.Constants;
import com.sale_order.app.utils.Language;
import com.sale_order.app.utils.MySingleton;

public class SettingsViewModel extends AndroidViewModel {
    private ViewListener viewListener;
    private ObservableField<Drawable> arabicButton, englishButton;
    private ObservableField<Boolean> notificationSettings;

    public SettingsViewModel(@NonNull Application application) {
        super(application);
        arabicButton = new ObservableField<>(!MySingleton.getInstance(application).getStringFromSharedPref(Constants.APP_LANGUAGE,
                application.getString(R.string.arabic_key)).equals(application.getString(R.string.english_key)) ?
                application.getResources().getDrawable(R.drawable.lang_active_button_bg) : application.getResources().getDrawable(R.drawable.lang_in_active_button_bg));
        englishButton = new ObservableField<>(MySingleton.getInstance(application).getStringFromSharedPref(Constants.APP_LANGUAGE,
                application.getString(R.string.arabic_key)).equals(application.getString(R.string.english_key)) ?
                application.getResources().getDrawable(R.drawable.lang_active_button_bg) : application.getResources().getDrawable(R.drawable.lang_in_active_button_bg));
        notificationSettings = new ObservableField<>(MySingleton.getInstance(application).getBooleanFromSharedPref(Constants.NOTIFICATION, true));
    }

    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public void requestChangeLang(boolean english) {
        englishButton.set(english ? getApplication().getResources().getDrawable(R.drawable.lang_active_button_bg) : getApplication().getResources().getDrawable(R.drawable.lang_in_active_button_bg));
        arabicButton.set(!english ? getApplication().getResources().getDrawable(R.drawable.lang_active_button_bg) : getApplication().getResources().getDrawable(R.drawable.lang_in_active_button_bg));
        MySingleton.getInstance(getApplication()).saveStringToSharedPref(Constants.APP_LANGUAGE,
                english ? getApplication().getString(R.string.english_key) : getApplication().getString(R.string.arabic_key));
        Language.setLanguage(getApplication(), english ? getApplication().getString(R.string.english_key) : getApplication().getString(R.string.arabic_key));
        viewListener.recreateAct();
    }

    public ObservableField<Drawable> getArabicButton() {
        return arabicButton;
    }

    public ObservableField<Drawable> getEnglishButton() {
        return englishButton;
    }

    public void setNotificationSettings(boolean notify) {
        MySingleton.getInstance(getApplication()).saveBooleanToSharedPref(Constants.NOTIFICATION, notify);
        notificationSettings.set(notify);
    }

    public ObservableField<Boolean> getNotificationSettings() {
        return notificationSettings;
    }

    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        setNotificationSettings(isChecked);
    }

    protected interface ViewListener {
        void recreateAct();
    }
}
