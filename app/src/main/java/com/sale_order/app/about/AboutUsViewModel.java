package com.sale_order.app.about;

import android.app.Application;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.sale_order.app.R;
import com.sale_order.app.utils.Constants;
import com.sale_order.app.utils.MySingleton;

import java.io.IOException;

public class AboutUsViewModel extends AndroidViewModel implements AboutUsModel.ModelCallback {
    private Application application;
    private AboutUsModel model;
    private ViewListener viewListener;
    private ObservableField<Integer> progress;
    private ObservableField<String> errorMessage;
    private ObservableField<Integer> errorView;

    public AboutUsViewModel(@NonNull Application application) {
        super(application);
        this.application = application;
        model = new AboutUsModel(application);
        progress = new ObservableField<>(View.GONE);
        errorView = new ObservableField<>(View.GONE);
        errorMessage = new ObservableField<>("");
    }

    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    protected MutableLiveData<String> requestTermsConditions(String language) {
        setProgress(View.VISIBLE);
        return model.fetchAboutUs(language, this);
    }

    public void onRetryClickListener(View view) {
        setProgress(View.VISIBLE);
        setErrorView(View.GONE);
        model.fetchAboutUs(MySingleton.getInstance(view.getContext()).getStringFromSharedPref(Constants.APP_LANGUAGE, view.getContext().getString(R.string.english_key)), this);
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    public ObservableField<String> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String message) {
        errorMessage.set(message);
    }

    public ObservableField<Integer> getErrorView() {
        return errorView;
    }

    public void setErrorView(int errorViewStatus) {
        this.errorView.set(errorViewStatus);
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }

    @Override
    public void onFailureHandler(Throwable t) {
        // error fetching Terms & conditions ...
        setErrorView(View.VISIBLE);
        Log.e("error", t.toString());
        if (t instanceof IOException)
            setErrorMessage(application.getString(R.string.no_internet_connection));
        else
            setErrorMessage(application.getString(R.string.error_fetching_data));

    }


    protected interface ViewListener {

    }
}
