package com.sale_order.app.about;

import android.content.Context;
import android.view.View;

import androidx.lifecycle.MutableLiveData;

import com.sale_order.app.R;
import com.sale_order.app.classes.TermsConditionsResponse;
import com.sale_order.app.utils.MySingleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AboutUsModel {
  private Context context;

  public AboutUsModel(Context context) {
    this.context = context;
  }


  private MutableLiveData<String> stringMutableLiveData = new MutableLiveData<>();

  protected MutableLiveData<String> fetchAboutUs(String lang, final ModelCallback callback) {
    Call<TermsConditionsResponse> call = MySingleton.getInstance(context).createService()
            .aboutUs(lang.equals(context.getString(R.string.arabic_key)) ? null : lang);

    call.enqueue(new Callback<TermsConditionsResponse>() {
      @Override
      public void onResponse(Call<TermsConditionsResponse> call, Response<TermsConditionsResponse> response) {
        callback.setProgress(View.GONE);
        if (response.body() != null) {
          stringMutableLiveData.setValue(response.body().getData());
        }
      }

      @Override
      public void onFailure(Call<TermsConditionsResponse> call, Throwable t) {
        callback.setProgress(View.GONE);
        callback.onFailureHandler(t);
      }
    });
    return stringMutableLiveData;
  }

  protected interface ModelCallback {
    void setProgress(int progress);

    void onFailureHandler(Throwable t);
  }
}
