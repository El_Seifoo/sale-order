package com.sale_order.app.about;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.sale_order.app.R;
import com.sale_order.app.databinding.ActivityAboutUsBinding;
import com.sale_order.app.utils.Constants;
import com.sale_order.app.utils.Language;
import com.sale_order.app.utils.MySingleton;

public class AboutUsActivity extends AppCompatActivity implements AboutUsViewModel.ViewListener {
    private AboutUsViewModel viewModel;
    private ActivityAboutUsBinding dataBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.arabic_key)));

        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_about_us);
        viewModel = ViewModelProviders.of(this).get(AboutUsViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);

        getSupportActionBar().setTitle(getString(R.string.about_us));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewModel.requestTermsConditions(MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE, getString(R.string.english_key))).observe(this, new Observer<String>() {
            @Override
            public void onChanged(String data) {
                dataBinding.aboutUsTextView.setText(data);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
